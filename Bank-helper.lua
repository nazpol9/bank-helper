script_name('Bank-Helper')
script_author('Cosmo')
script_version('8.2')

-- colors
local mc, sc, wc = '{006AC2}', '{006D86}', '{FFFFFF}'
local mcx = 0x006AC2

require "moonloader"
local dlstatus = require "moonloader".download_status
local vkeys = require 'vkeys'
local sampev = require 'lib.samp.events'
local inicfg = require 'inicfg'
local imgui = require 'imgui'

local res_addons, addons = pcall(require, "imgui_addons")
if not res_addons then
	no_error_msg = true
	downloadUrlToFile('https://gitlab.com/uploads/-/system/personal_snippet/1978930/4de4d446323a40abfa7689794e60adb8/imgui_addons.lua', getWorkingDirectory()..'/lib/imgui_addons.lua', function (id, status, p1, p2)
		if status == dlstatus.STATUSEX_ENDDOWNLOAD then
			lua_thread.create(function()
				sampfuncsLog(mc..'Bank-Helper:'..wc..' ������� ����������� ���������� imgui_addons.lua')
				no_error_msg = true
				wait(5000)
				thisScript():reload()
			end)
		end
	end)
else
	imgui.ToggleButton = require("imgui_addons").ToggleButton
	imgui.Spinner = require("imgui_addons").Spinner
end

local bNotf, notify = pcall(import, "imgui_notf.lua")
if not bNotf then
	no_error_msg = true
	downloadUrlToFile('https://gitlab.com/uploads/-/system/personal_snippet/1978930/88a837a7022708b33b6eed6296888117/imgui_notf.lua', getWorkingDirectory()..'/imgui_notf.lua', function (id, status, p1, p2)
		if status == dlstatus.STATUSEX_ENDDOWNLOAD then
			lua_thread.create(function()
				sampfuncsLog(mc..'Bank-Helper:'..wc..' ������� ����������� ���������� imgui_notf.lua')
				no_error_msg = true
				wait(5000)
				thisScript():reload()
			end)
		end
	end)
end

local res_fa, fa = pcall(require, "fAwesome5")
if not res_fa then
	no_error_msg = true
	downloadUrlToFile('https://gitlab.com/uploads/-/system/personal_snippet/1978930/cd9d80b7e1298e3ab23b7681fb09acd0/fAwesome5.lua', getWorkingDirectory()..'/lib/fAwesome5.lua', function (id, status, p1, p2)
		if status == dlstatus.STATUSEX_ENDDOWNLOAD then
			lua_thread.create(function()
				sampfuncsLog(mc..'Bank-Helper:'..wc..' ������� ����������� ���������� fAwesome5.lua')
				no_error_msg = true
				wait(5000)
				thisScript():reload()
			end)
		end
	end)
end

local res_lfs, lfs = pcall(require, "lfs")
if not res_lfs then
	no_error_msg = true
	downloadUrlToFile('https://gitlab.com/uploads/-/system/personal_snippet/1978930/8227d0f46a58b02909a9e75b726af843/lfs.dll', getWorkingDirectory()..'/lib/lfs.dll', function (id, status, p1, p2)
		if status == dlstatus.STATUSEX_ENDDOWNLOAD then
			lua_thread.create(function()
				sampfuncsLog(mc..'Bank-Helper:'..wc..' ������� ����������� ���������� lfs.dll')
				no_error_msg = true
				wait(5000)
				thisScript():reload()
			end)
		end
	end)
end

local encoding = require 'encoding'
local u8 = encoding.UTF8
encoding.default = 'CP1251'
local no_error_msg = false
local tag = mc..'[ Bank ] '..wc
local id_kassa = 0
local getMB = false
local distBetweenKassa = 0
local getMembersCount = '**'
local membersList = {}
local msgError = {}
local status_button_gov = false
local getmembers = {
	[1] = 0,
	[2] = 0,
	[3] = 0,
	[4] = 0,
	[5] = 0,
	[6] = 0,
	[7] = 0,
	[8] = 0,
	[9] = 0
}

local blg = inicfg.load({
	ylw = {
		y = ''
	},
	red = {
		r = ''
	}
}, "Bank_BlackList")

local ylw = imgui.ImBuffer(blg.ylw.y:gsub('~', '\n'), 65536)
local red = imgui.ImBuffer(blg.red.r:gsub('~', '\n'), 65536)
local configuration = inicfg.load({
	main_settings = 
	{
		rank = 1,
		sex = 1,
		encode_ustav = true,
		autoF8 = true,
		upWithRp = true,
		LectDelay = 5,
		rpbat = false,
		rpbat_true = "/me {sex:����|�����} ������� � ����� ���� � ������ ����",
		rpbat_false = "/me {sex:�������|��������} ������� �� ����" , 
		colorRchat = 759410733,
		colorDchat = 771725619,
		style = 3,
		forma_post = '/r �����������: {my_name} | ����: [����] | ���������: ����������',
		forma_lect = '/r ����������! ���� ��������� � ����������, ����� ������! �� ������� - �������!',
		forma_tr = '/r ����������! ������ ����� ����������! ���� �� �����! �� ������� - �������!',
		accent_status = false,
		accent = '[����������� ������]',
		glass_light = 1.0,
		glass_light_child = 1.0,
		infoupdate = false,
		loginupdate = true,
		members_state = false,
		members_posX = 100,
		members_posY = 300,
		members_afk = 300,
		members_delay = 10,
		members_fontName = 'Arial',
		members_fontSize = 10,
		members_fontFlag = 5,
		members_fontOffset = 16,
		KipX = select(1, getScreenResolution()) - 250,
		KipY = select(2, getScreenResolution()) / 2, 
		ki_stat = true,
		timeF9 = true
	},
	timer =
	{
	    day = os.date("%a"),
	    online = tonumber(0),
	    kassa_dep = tonumber(0),
	    kassa_card = tonumber(0),
	    kassa_credit = tonumber(0),
	    kassa_recard = tonumber(0)
	},
	Chat = {
		msg_expel = true,
		msg_shtrafs = true,
		msg_incazna = true,
		msg_invite = true,
		msg_uval = true
	},
	nameRank = {
		'��������', 
		'������� ��������', 
		'��������� ������', 
		'��.��������� �����', 
		'���.������ ����������', 
		'���.������ ����������', 
		'��������', 
		'���.���������', 
		'�������� �����', 
		'������� ��������'
	},
	Binds_Name = {},
	Binds_Action = {},
	Binds_Deleay = {}
}, "Bank_Config")

local bank 				= imgui.ImBool(false)
local int_bank 			= imgui.ImBool(false)
local uval				= imgui.ImBool(false)
local premium 			= imgui.ImBool(false)
local kassaInfo			= imgui.ImBool(false)
local kassa_stat 		= imgui.ImBool(false)
local kassa_name 		= imgui.ImBuffer(256)
local kassa_time 		= imgui.ImBuffer(256)
local kip 				= imgui.ImBool(false)
local type_window 		= imgui.ImInt(0)
local TypeAction 		= imgui.ImInt(1)
local question 			= imgui.ImInt(1)
local giverank 			= imgui.ImInt(1)
local text_binder 		= imgui.ImBuffer(65536) 
local binder_name 		= imgui.ImBuffer(40) 
local binder_delay 		= imgui.ImInt(2500)
local play_current_bind = imgui.ImBool(false)
local ustav_window 		= imgui.ImBool(false)
local search_ustav 		= imgui.ImBuffer(256)
local go_credit 		= imgui.ImBool(false)
local credit_sum 		= imgui.ImInt(5000)
local uninvite_id 		= imgui.ImBuffer(256)
local uninvite_reason 	= imgui.ImBuffer(256)
local bl_reason			= imgui.ImBuffer(256)
local st_members		= imgui.ImBool(false)
local premsum			= imgui.ImInt(100000)
local prem_bank			= imgui.ImInt(1)
local bank_cazna		= imgui.ImInt(0)
local mGov = imgui.ImInt(30)
local hGov = imgui.ImInt(15)
local gosScreen = imgui.ImBool(true)
local gosDep = imgui.ImBool(true)
local delayGov = imgui.ImInt(2500)
local govstr1 = imgui.ImBuffer(u8'[����] ��������� ������ �����!', 256)
local govstr2 = imgui.ImBuffer(u8'[����] �� ������� ����� ������� ��������� �� ������� ������ ������������ �����.', 256)
local govstr3 = imgui.ImBuffer(u8'[����] � ��� �� �������� ������� ��������, ������, ��������� ����. ����������� �� �������.', 256)
local prem_rank = {
	[1] = imgui.ImBool(false),
	[2] = imgui.ImBool(false),
	[3] = imgui.ImBool(false),
	[4] = imgui.ImBool(false),
	[5] = imgui.ImBool(false),
	[6] = imgui.ImBool(false),
	[7] = imgui.ImBool(false),
	[8] = imgui.ImBool(false),
	[9] = imgui.ImBool(false)
}
local infoupdate		= imgui.ImBool(configuration.main_settings.infoupdate)
local loginupdate		= imgui.ImBool(configuration.main_settings.loginupdate)
local members_state 	= imgui.ImBool(configuration.main_settings.members_state)
local members_posX 		= imgui.ImInt(configuration.main_settings.members_posX)
local members_posY 		= imgui.ImInt(configuration.main_settings.members_posY)
local members_afk 		= imgui.ImInt(configuration.main_settings.members_afk)
local members_clr1 		= imgui.ImFloat4(imgui.ImColor(configuration.main_settings.members_clr1):GetFloat4())
local members_clr2 		= imgui.ImFloat4(imgui.ImColor(configuration.main_settings.members_clr2):GetFloat4())
local members_delay 	= imgui.ImInt(configuration.main_settings.members_delay)
local members_fontName 	= imgui.ImBuffer(configuration.main_settings.members_fontName, 256)
local members_fontSize 	= imgui.ImInt(configuration.main_settings.members_fontSize)
local members_fontFlag 	= imgui.ImInt(configuration.main_settings.members_fontFlag)
local members_fontOffset= imgui.ImInt(configuration.main_settings.members_fontOffset)
local ki_stat			= imgui.ImBool(configuration.main_settings.ki_stat)
local rank 				= imgui.ImInt(configuration.main_settings.rank)
local sex 				= imgui.ImInt(configuration.main_settings.sex)
local autoF8 			= imgui.ImBool(configuration.main_settings.autoF8)
local upWithRp 			= imgui.ImBool(configuration.main_settings.upWithRp)
local LectDelay 		= imgui.ImInt(configuration.main_settings.LectDelay)
local msg_expel 		= imgui.ImBool(configuration.Chat.msg_expel)
local msg_shtrafs 		= imgui.ImBool(configuration.Chat.msg_shtrafs)
local msg_incazna 		= imgui.ImBool(configuration.Chat.msg_incazna)
local msg_invite 		= imgui.ImBool(configuration.Chat.msg_invite)
local msg_uval 			= imgui.ImBool(configuration.Chat.msg_uval)
local rpbat 			= imgui.ImBool(configuration.main_settings.rpbat)
local rpbat_true 		= imgui.ImBuffer(u8(configuration.main_settings.rpbat_true), 256) 
local rpbat_false 		= imgui.ImBuffer(u8(configuration.main_settings.rpbat_false), 256)
local colorRchat 		= imgui.ImFloat4(imgui.ImColor(configuration.main_settings.colorRchat):GetFloat4())
local colorDchat 		= imgui.ImFloat4(imgui.ImColor(configuration.main_settings.colorDchat):GetFloat4())
local style 			= imgui.ImInt(configuration.main_settings.style)
local forma_post 		= imgui.ImBuffer(u8(configuration.main_settings.forma_post), 256)
local forma_lect 		= imgui.ImBuffer(u8(configuration.main_settings.forma_lect), 256)
local forma_tr 			= imgui.ImBuffer(u8(configuration.main_settings.forma_tr), 256)
local accent_status 	= imgui.ImBool(configuration.main_settings.accent_status)
local accent 			= imgui.ImBuffer(u8(configuration.main_settings.accent), 256)
local glass_light 		= imgui.ImFloat(configuration.main_settings.glass_light)
local glass_light_child = imgui.ImFloat(configuration.main_settings.glass_light_child)
local timeF9 			= imgui.ImBool(configuration.main_settings.timeF9)

local script_changelog = [[������ 9.0 by Brian Randall] test
[������ 8.0 - 8.2 (�������):
 - ������ �� ������ � ����������� �� ����� ������ ����������� ����� ����������, � �����������, ����� ��������� ����� ����
 - ������ ������ �� ����� ��������, ���� �� �� �������� � �����
 - ���������� ������� ��������� ����� ��������
 - ���������� ������� �������� ����������� ��� /premium
 - �������� ������ ������ ������ �� /members, ��� �� ��� ���������� ��� ��� ������, ������ �������� ���� /members, ������ ��� �� ������� �� ���
 - ������� ������� ��������� ������, ������ ��� ������ ������� �������, � ��� �� ����� ������ ��������� � ��� � ���� ��������
 - �������� ���� /time �� ������� F9, ��������� ����� � ����������
 - �������� ������� ��������: ������������ ����� ������ 300.000$ (��������), ������ ����� �� ������ ������ ������� ���������� ������� ������������, ������������� �������� �, ���� ������� ���� ������������.txt � ����� /moonloader/BHelper
 - ���� ��������� ��������� �� ������� � ����, � ��� �� ��������� �� ������������ ����� ��������� � ������� sampfuncs, ��� �� �� ������ ������ ���
 - [New] ���� ���� � 1-4 ������, ����� ��� ������� ���� � ����� �����, �������� ��������� "���� � ����� �������� � 5-�� �����"
 - [New] ���������� ������ ���������
 - [New] ������� ��������������� ���� /premium, ������ ��� ���� ��������������� ������� ���������� ������ �� ������ ����������
 - [New] �������� � ������ ������ ������������ ��������� ����� ������������ ���
 - [New] ��� @id ������ ���������� ������ ��� ������, ��� ��� �������. ��������: "@343, ��� �� ����������?" - "Jeffy, ��� �� ����������?"
 - [New] ���������� �������� ����� �������, ������ ���������� � ����� ����, ����� �������� � ��������, ��� ���� �����

������ 7.4 - 7.9:
 - ��������� ������ ����������, ����� ������ �� ����� (��������� ����� � /bank - ��������� - ����� ���������)
 - �������� �������������� ������ ����� �������� /kip (����������� ����� ����������)
 - ����� 3dText, ����� ��������� �����, ������ �� ������ �������� �����. ������ �� ������� � ������ ���������� (����� ����)
 - ������ ��� �������� � /bankmenu (��� + Q) ������������ �� ������. �������� ������ ������ ���� �� ����� ������ ������ "������ ������", � ������ �� ��������
 - ��������� ���� � /premium ��� ��������� ����-������
 - ��������� ������� /unblacklist [id]
 - ������ ��� {screen} �������� ��� �����. ������ �������������: "����������, ������ � ������� ��� ���������� {screen}" (������ ����������� ������������� ����� ��������, � {screen} �������)
 - ������������� ��������� �����, ������ �� ������ ����� ��� ������� �������� ����� ��������
 - ��������� ����-��������� �������������� ��������� (fAwesome, lfs), � ������ �� ���������
 - ������ �����

������ 7.3:
 - ��������� ���, ����� ������ ���� ������ ������ ")" "))" "(" "((" "xD" ":D" �������� "� ��������". ������ �� �������� �� ��
 - ��� ������� � ������������ ������ ���� �����, � ������� ���������� ��� ���������, ����������, ������ ��������� � ������. ����� ������� ��� ���������� ������� ��������
 - ���������� ���� � �������� /invite
 - ����� ��������� ����� members'a

������ 7.2:
 - �������� ����� /members, �� �������� � 5-��� ����� � ������� /bank -> ��. ������
 - ���������� ����������� �������
 - ��������� ����� �������� �������� ����� ���������� ��� �������� ����� ������������ � �������. ��������: /bank -> ��������� -> ��������� ������
 - ���������� ���� � �������, �� ���, �������� ��������� ��� �� "����������" ������� ����������. �� ����� "������", �� ����� ����� ����������, ������� ����� ��������� �������� � ���������� 10-15 ������, ��� �� �� ��������. 

������ 6.0:
 - ��������� ����������� � ���� ������, ������ ��������� ��������� ����� ��������� � ���
 - ��� 9-10 ������ ��������� ���� ������������ GOV �����. ����� ��� ����� �� ������� "������� ������"
 - � ���� �� ������ ��������� ���� ������ ������, ������ ��� ���� ������ �� ������� /premium
 - �������� ��� ���������� ��� ������ � �����������. ������ � ���� ����� �������� /ro � /rbo. (������ ��� ��� ��� � ����-��������, ��������� ����� ������ ���� ���������� ����� /r � /rb)

������ 5.0:
 - ���� ������ ������ ����������� (9+), ��� ��������� ������ �������� - /premium
 - ���������� ��������� ���������
 - ���������� ��������� ����
 - ������� ��������� ��� �������: /blacklist, /giverank
 - � ���������� ������� ����� ���������� ������� "����-�����". ��� ������� ������������� ������� � /time ��� ���� ���������, ����������, �� � �.�. 
 - ��� �� � ���������� ����� ������� ASI-������ "Screenshot" �� MISTER_GONWIK'a. �� ��������� ������ ��������� ��� ��������� (�������� ���� �� ������������ :D)
 - �������� ����� ��� ��� ������� - {screen}. ����� ������������ � ����/��������. ��������: /giverank 228 9 {screen}, ������� �������� �������, � ����� ��� ��������� � � /time

������ 4.0:
 - ������ �������� �� �������� ��������� ��� ������� ������������ ��� + G (������� �� �����), ����� ������ ��� �������� �����������, � �������� ���������� ��� �������������
 - ������ ��� ���� � ���������� ������� ������������� �������������.

������ 3.0:
 - ���� ��� ���� ���� 8-���, �� ��� ��������� ��������� �������������, ��� ��� ���������� �� ����� ������������ ��, ��� �� ������ ����� � ���������, � �����������, ��� �� ������� ������������, ��� �� �� ��� ������ (������� � �����)
 - �������� ����� � ���������� "� �������" - ��������� �������������� ����������, ����� �� �������� � ����
 - ����� ��������� ��� �������� ������� (5+ ����) � ���� /bank
 - �������� �� ������������� (5+ ����) � ��������� ��. ������ (/bank). ������ � ����� �����������: "������ ��" � "������� ��", � �������� ��� ������� ��� ������� � ������������, � ��� �� ��� ���, ���� ��������� �������������. ��� ������ �������� �� ׸����� ������ ��� �� ���� ��� �������, ���� �������� ������ ������ �� �������� � ���� �����
 - ��������� ������� /invite (9+ ����), ��������� ��������� ��� � �������������
 - ������ ����� ������ ������

������ 2.0: 
 - �������� ����� ���� ��������� � "���������� ���������/�������"
 - ���� ���� ����� � ������� ���������� ����������� �������� "{SSSSSS}"
 - ��������� ��������� ��������� ������, ���� �� �� �������� ������� ����� �� �������
 - ��������� ������� /uninvite, ������ ��������� ��������� ��� � �������������
 - ������ �����

������ 1.0 - 1.9:
 - �������� ����-���� �������
 - ���������� ���� �������� ������� ��� ������ � ����� (/bankmenu)
 - ���������� ������ ����� ���� �����������/������������
 - ���������� �������
 - ���������� ��������� �� ������� 
 - ���������� ��������� �� ������� /fwarn, /unfwarn
 - ������ ������������

������ 1.0: 
 - ����� �������, � ������ ��� ��������� ����-�����, ������� �� ������ Noa Shelby, Bruno Quinzsy, Markus Quinzsy]]
local changelog = imgui.ImBuffer(u8(script_changelog), 65536)

function main()
    if not isSampLoaded() or not isSampfuncsLoaded() then return end
    while not isSampAvailable() do wait(0) end

    if not doesFileExist('moonloader/config/Bank_Config.ini') then
        if inicfg.save(configuration, 'Bank_Config.ini') then 
        	sampfuncsLog(mc..'Bank-Helper:'..wc..' ������ ���� Bank_Config.ini')
    	end
    end

    if configuration.main_settings.loginupdate then
    	autoupdate("https://gitlab.com/snippets/2002160/raw", '['..string.upper(thisScript().name)..']: ', "https://gitlab.com/snippets/2002160/raw?inline=false")
    	checkdata()
    end

    if not doesFileExist('moonloader/config/Bank_BlackList.ini') then 
    	if inicfg.save(blg, 'Bank_BlackList.ini') then 
    		sampfuncsLog(mc..'Bank-Helper:'..wc..' ������ ���� Bank_BlackList.ini')
    	end
    end

    if configuration.timer.day ~= os.date("%a") then
        configuration.timer.day = os.date("%a")
        configuration.timer.online = tonumber(0)
        configuration.timer.kassa_dep = tonumber(0)
		configuration.timer.kassa_card = tonumber(0)
		configuration.timer.kassa_cedit = tonumber(0)
		configuration.timer.kassa_recard = tonumber(0)
        inicfg.save(configuration, 'Bank_Config.ini')
    end   

    if doesFileExist('moonloader/BHelper/main.png') then
    	mainPng = imgui.CreateTextureFromFile('moonloader/BHelper/main.png')
    end

    if doesFileExist('moonloader/BHelper/binds.png') then
    	bindsPng = imgui.CreateTextureFromFile('moonloader/BHelper/binds.png')
    end

    if res_addons and bNotf and res_fa and res_lfs then
	    if bNotf then
	    	notify.addNotify(mc.."Bank-Helper", "������� ����: "..mc.."/bank\n���� ��������������: "..mc.."��� + Q", 2, 2, 5)
	    	notify.addNotify(mc.."Bank-Helper", "������� ������: "..mc..thisScript().version.."\n������ ��������!", 2, 2, 5)
		else
		    sampAddChatMessage(tag..'��������� ��������! ������: '..mc..thisScript().version, mcx)
			sampAddChatMessage(tag..'��������� '..mc..'��� + Q'..wc..' �� ������ ��� �� ������� ���� ��������������', mcx)
			sampAddChatMessage(tag..'�������� ���� - '..mc..'/bank', mcx)
		end
	end

	if configuration.main_settings.infoupdate then
		infoupdate.v = true
		configuration.main_settings.infoupdate = false
        inicfg.save(configuration, 'Bank_Config.ini')
	end

	sampRegisterChatCommand('bank', function()
		bank.v = not bank.v
	end)

	-- >> DEVELOPER BLOCK <<
	sampRegisterChatCommand('devmode', function()
		if sampGetPlayerNickname(select(2, sampGetPlayerIdByCharHandle(playerPed))) == 'Jeffy_Cosmo' then
			dev0x = 0xFFF000
			devtag = '{FFF000}[ Bank | DEV ] '..wc
			sampAddChatMessage(devtag..'�� ����� � ����� ������������!', dev0x)
			sampAddChatMessage(devtag..'��������� �������:', dev0x)
			sampAddChatMessage(devtag..'/setrankdev [rank]', dev0x)
			devmode = true
		end
	end)
	sampRegisterChatCommand('setrankdev', function(rank)
		if devmode then
			configuration.main_settings.rank = tonumber(rank)
			sampAddChatMessage(devtag..'���� '..rank..' �����', dev0x)
		end
	end)
	-- >> DEVELOPER BLOCK END <<

	sampRegisterChatCommand('kip', function()
		if kassa_stat.v and not kip.v then 
			kip.v = true 
		else
			sampAddChatMessage(tag..'�������� �� ���� ��� �������� ������ � ����������', mcx)
		end
		if kip.v then
			showCursor(true, true)
			checkCursor = true
			sampAddChatMessage(tag..'������� '..mc..'ENTER'..wc..' ���-�� ��������� ������� ������', mcx)
			lua_thread.create(function()
				while checkCursor do
					local cX, cY = getCursorPos()
					configuration.main_settings.KipX = cX
					configuration.main_settings.KipY = cY
					if isKeyDown(13) then
						kip.v = false
						checkCursor = false
						showCursor(false, false)
						if inicfg.save(configuration, 'Bank_Config.ini') then 
							sampAddChatMessage(tag..'������� ���������!', mcx)
						end
					end
					wait(0)
				end
			end)
		end
	end)

	sampRegisterChatCommand('ustav', function()
		ustav_window.v = not ustav_window.v
	end)

	sampRegisterChatCommand('ro', function(text)
		if configuration.main_settings.rank > 8 or sampGetPlayerNickname(select(2, sampGetPlayerIdByCharHandle(playerPed))) == 'Jeffy_Cosmo' then
			if #text > 0 then
				sampSendChat('/r [����������] '..text)
			else
				sampAddChatMessage(tag..'��������� /ro [text]', mcx)
			end
		else
			sampAddChatMessage(tag..'�������� ������ � 9 �����', mcx)
		end
	end)
	sampRegisterChatCommand('rbo', function(text)
		if configuration.main_settings.rank > 8 or sampGetPlayerNickname(select(2, sampGetPlayerIdByCharHandle(playerPed))) == 'Jeffy_Cosmo' then
			if #text > 0 then
				sampSendChat('/rb [����������] '..text)
			else
				sampAddChatMessage(tag..'��������� /rbo [text]', mcx)
			end
		else
			sampAddChatMessage(tag..'�������� ������ � 9 �����', mcx)
		end
	end)

	sampRegisterChatCommand('premium', function(prem_arg)
		if configuration.main_settings.rank > 8 then
			if #prem_arg == 0 then
				for i = 1, 9 do
					getmembers[i] = 0
				end
				st_members.v = true
				sampSendChat('/members')
				premium.v = true
			else
				local prem_kazna, prem_sum, prem_rank_num = prem_arg:match('(%d+) (%d+) (%d+)')
				local er_msg = tag..'��������� /premium [ 1 (� ����� ) | 2 (� ���) ] [ 100k - 20kk ] [ ���� ]'
				if tonumber(prem_kazna) and tonumber(prem_sum) and tonumber(prem_rank_num) then 
					if tonumber(prem_kazna) > 0 and tonumber(prem_kazna) < 3 then
						if tonumber(prem_sum) > 100000 and tonumber(prem_sum) < 20000000 then 
							if tonumber(prem_rank_num) > 0 and tonumber(prem_rank_num) < 10 then 
								sampSendChat('premium '..tostring(prem_kazna)..' '..tonumber(prem_sum)..' '..tonumber(prem_rank_num))
								lua_thread.create(function()
									if configuration.main_settings.autoF8 then
										wait(1000)
										sampSendChat('/time')
										wait(500)
										setVirtualKeyDown(VK_F8, true)
										wait(0)
										setVirtualKeyDown(VK_F8, false)
									end
								end)
							else sampAddChatMessage(er_msg, mcx) end
						else sampAddChatMessage(er_msg, mcx) end
					else sampAddChatMessage(er_msg, mcx) end
				else sampAddChatMessage(er_msg, mcx) end
			end
		else
			sampAddChatMessage(tag..'������� �������� ������ � 9-��� �����', mcx)
		end
	end)
	sampRegisterChatCommand('/uninvite', function(uninv_id)
		if #uninv_id == 0 then uninv_id = '' end
		sampSendChat('/uninvite '..uninv_id)
	end)
	sampRegisterChatCommand('uninvite', function(uninv_id)
		if configuration.main_settings.rank > 8 then
			local uninv_id, uninv_reason = uninv_id:match('(%d+) (.+)')
			if tonumber(uninv_id) and uninv_reason then
				if sampIsPlayerConnected(tonumber(uninv_id)) then
					lua_thread.create(function()
		                sampSendChat("/me {sex:������|�������} ������� � {sex:������|�������} ���� ������")
		                wait(2500)
		                sampSendChat("/me {sex:�������|�������} � ������ ����������� � {sex:�����|�����} ��� "..sampGetPlayerNickname(tonumber(uninv_id)))
		                wait(2500)
		                sampSendChat("/me {sex:������|�������} ���������� � {sex:�����|������} ���������")
		                wait(500)
						sampSendChat('/uninvite '..tostring(uninv_id)..' '..tostring(uninv_reason))
						if configuration.main_settings.autoF8 then
							wait(1000)
							sampSendChat('/time')
							wait(500)
							setVirtualKeyDown(VK_F8, true)
							wait(0)
							setVirtualKeyDown(VK_F8, false)
						end
		            end)
		        else
		        	sampAddChatMessage(tag..'������ ������ ��� � ����!', mcx)
		        end
			else
				sampAddChatMessage(tag..'���������: /uninvite [id] [�������]', mcx)
			end
		else
			sampAddChatMessage(tag..'������� �������� ������ � 9-��� �����', mcx)
		end
	end)
	sampRegisterChatCommand('/giverank', function(grank_arg)
		if #grank_arg == 0 then grank_arg = '' end
		sampSendChat('/giverank '..grank_arg)
	end)
	sampRegisterChatCommand('giverank', function(grank_arg)
		if configuration.main_settings.rank > 8 then
			local grank_id, grank_rank = grank_arg:match('(%d+) (%d+)')
			if not doesDirectoryExist(getWorkingDirectory()..'/BHelper/�����������') then 
    			if createDirectory(getWorkingDirectory()..'/BHelper/�����������') then 
    				sampfuncsLog(mc..'Bank-Helper:'..wc..' ������� ����������� /BHelper/�����������')
    			end
    		end
    		log_giverank = io.open(getWorkingDirectory()..'/BHelper/�����������/���������.txt', "a")
			log_giverank:write(sampGetPlayerNickname(grank_id).." | "..tostring(grank_rank - 1).." -> "..tostring(grank_rank).." | "..os.date("%d.%m.%Y").." | "..os.date("%H:%M:%S", os.time()).." | "..sampGetPlayerNickname(select(2, sampGetPlayerIdByCharHandle(playerPed))).."\n")
			log_giverank:close()
			if tonumber(grank_id) and tonumber(grank_rank) then
				if sampIsPlayerConnected(tonumber(grank_id)) then
					if getDistBetweenPlayers(grank_id) < 5 then
						lua_thread.create(function()
			                sampSendChat('/me {sex:������|�������} �� ������� ���')
							wait(2500)
							sampSendChat('/me {sex:�������|��������} ��� � {sex:�����|�����} � ������ "����������"')
							wait(2500)
							sampSendChat('/me {sex:������|�������} ���������� '..sampGetPlayerNickname(grank_id))
							wait(2500)
							sampSendChat('/me {sex:�������|��������} ��������� ���������� �� �'..configuration.nameRank[tonumber(grank_rank)]..'�')
							wait(2500)
							sampSendChat('/giverank '..tostring(grank_id)..' '..tostring(grank_rank))
							if configuration.main_settings.autoF8 then
								wait(1000)
								sampSendChat('/time')
								wait(500)
								setVirtualKeyDown(VK_F8, true)
								wait(0)
								setVirtualKeyDown(VK_F8, false)
							end
			            end)
			        else
			        	sampAddChatMessage(tag..'�� ������ �� ����� ����������!', mcx)
			        end
		        else
		        	sampAddChatMessage(tag..'������ ������ ��� � ����!', mcx)
		        end
			else
				sampAddChatMessage(tag..'���������: /giverank [id] [����]', mcx)
			end
		else
			sampAddChatMessage(tag..'������� �������� ������ � 9-��� �����', mcx)
		end
	end)
	sampRegisterChatCommand('/invite', function(inv_id)
		if #inv_id == 0 then inv_id = '' end
		sampSendChat('/invite '..inv_id)
	end)
	sampRegisterChatCommand('invite', function(inv_id)
		if configuration.main_settings.rank > 8 then
			if tonumber(inv_id) then
				if sampIsPlayerConnected(tonumber(inv_id)) then
					if not blg.red.r:find(sampGetPlayerNickname(tonumber(inv_id))) then 
						if not blg.ylw.y:find(sampGetPlayerNickname(tonumber(inv_id))) then 
							lua_thread.create(function()
				                sampSendChat("/me {sex:������|�������} ������� � {sex:������|�������} ���� ������")
				                wait(2500)
				                sampSendChat("/me {sex:�������|�������} � ������ ����������� � {sex:����|������} ���� ������ ���������� "..sampGetPlayerNickname(tonumber(inv_id)))
				                wait(2500)
				                sampSendChat("/me {sex:�������|��������} ���������� ����� �� ��������")
				                wait(500)
								sampSendChat('/invite '..tostring(inv_id))
								if configuration.main_settings.autoF8 then
									wait(1000)
									sampSendChat('/time')
									wait(500)
									setVirtualKeyDown(VK_F8, true)
									wait(0)
									setVirtualKeyDown(VK_F8, false)
								end
				            end)
			            else
			            	sampAddChatMessage(tag..'�� �� ������ ������� ����� ������ �� �������!', mcx)
			            	sampAddChatMessage(tag..'����� '..mc..sampGetPlayerNickname(tonumber(inv_id))..wc..' ��������� � {FF9D00}�� ����� �������{FFFFFF} �������������!', mcx)
			            end
			        else
			        	sampAddChatMessage(tag..'�� �� ������ ������� ����� ������ �� �������!', mcx)
			        	sampAddChatMessage(tag..'����� '..mc..sampGetPlayerNickname(tonumber(inv_id))..wc..' ��������� � {FF0000}�� ������� �������{FFFFFF} �������������!', mcx)
			        end
		       	else
		        	sampAddChatMessage(tag..'������ ������ ��� � ����!', mcx)
		        end
			else
				sampAddChatMessage(tag..'���������: /invite [id]', mcx)
			end
		else
			sampAddChatMessage(tag..'������� �������� ������ � 9-��� �����', mcx)
		end
	end)
	sampRegisterChatCommand('/fwarn', function(fwarn_arg)
		if #fwarn_arg == 0 then fwarn_arg = '' end
		sampSendChat('/fwarn '..fwarn_arg)
	end)
	sampRegisterChatCommand('fwarn', function(fwarn_arg)
		if configuration.main_settings.rank > 8 then
			local fwarn_id, fwarn_reason = fwarn_arg:match('(%d+) (.+)')
			if tonumber(fwarn_id) and fwarn_reason then
				if sampIsPlayerConnected(tonumber(fwarn_id)) then
					lua_thread.create(function()
		                sampSendChat("/me {sex:������|�������} �� �������� ������ �����������")
		                wait(2500)
		                sampSendChat("/me {sex:������|�������} ���������� "..sampGetPlayerNickname(tonumber(fwarn_id)))
		                wait(2500)
		                sampSendChat("/me � ���� {sex:������|�������} ����� ������� �������")
		                wait(500)
						sampSendChat('/fwarn '..tostring(fwarn_id)..' '..tostring(fwarn_reason))
						if configuration.main_settings.autoF8 then
							wait(1000)
							sampSendChat('/time')
							wait(500)
							setVirtualKeyDown(VK_F8, true)
							wait(0)
							setVirtualKeyDown(VK_F8, false)
						end
		            end)
		        else
		        	sampAddChatMessage(tag..'������ ������ ��� � ����!', mcx)
		        end
			else
				sampAddChatMessage(tag..'���������: /fwarn [id] [�������]', mcx)
			end
		else
			sampAddChatMessage(tag..'������� �������� ������ � 9-��� �����', mcx)
		end
	end)
	sampRegisterChatCommand('/blacklist', function(bl_arg)
		if #bl_arg == 0 then bl_arg = '' end
		sampSendChat('/blacklist '..bl_arg)
	end)
	sampRegisterChatCommand('blacklist', function(bl_arg)
		if configuration.main_settings.rank > 8 then
			local bl_id, bl_reason = bl_arg:match('(%d+) (.+)')
			if tonumber(bl_id) and bl_reason then
				if sampIsPlayerConnected(tonumber(bl_id)) then
					lua_thread.create(function()
		                sampSendChat("/me {sex:������|�������} �� �������� ������ �׸���� ������")
		                wait(2500)
		                sampSendChat("/me � ���� {sex:������|�������} ����� ����������")
		                wait(2500)
		                sampSendChat("/me {sex:����|������} ���������� "..sampGetPlayerNickname(tonumber(bl_id)).." � ������ ������ �����")
		                wait(500)
						sampSendChat('/blacklist '..tostring(bl_id)..' '..tostring(bl_reason))
						if configuration.main_settings.autoF8 then
							wait(1000)
							sampSendChat('/time')
							wait(500)
							setVirtualKeyDown(VK_F8, true)
							wait(0)
							setVirtualKeyDown(VK_F8, false)
						end
		            end)
		        else
		        	sampAddChatMessage(tag..'������ ������ ��� � ����!', mcx)
		        end
			else
				sampAddChatMessage(tag..'���������: /blacklist [id] [�������]', mcx)
			end
		else
			sampAddChatMessage(tag..'������� �������� ������ � 9-��� �����', mcx)
		end
	end)
	sampRegisterChatCommand('/unblacklist', function(unbl_arg)
		if #unbl_arg == 0 then unbl_arg = '' end
		sampSendChat('/blacklist '..unbl_arg)
	end)
	sampRegisterChatCommand('unblacklist', function(unbl_arg)
		if configuration.main_settings.rank > 8 then
			local unbl_id = unbl_arg:match('(%d+)')
			if tonumber(unbl_id) then
				if sampIsPlayerConnected(tonumber(unbl_id)) then
					lua_thread.create(function()
		                sampSendChat("/me {sex:������|�������} �� �������� ������ �׸���� ������")
		                wait(2500)
		                sampSendChat("/me � ���� {sex:������|�������} ����� �����������")
		                wait(2500)
		                sampSendChat("/me {sex:��������|���������} ���������� "..sampGetPlayerNickname(tonumber(unbl_id)).." �� ������� ������ �����")
		                wait(500)
						sampSendChat('/unblacklist '..tostring(unbl_id))
						if configuration.main_settings.autoF8 then
							wait(1000)
							sampSendChat('/time')
							wait(500)
							setVirtualKeyDown(VK_F8, true)
							wait(0)
							setVirtualKeyDown(VK_F8, false)
						end
		            end)
		        else
		        	sampAddChatMessage(tag..'������ ������ ��� � ����!', mcx)
		        end
			else
				sampAddChatMessage(tag..'���������: /unblacklist [id]', mcx)
			end
		else
			sampAddChatMessage(tag..'������� �������� ������ � 9-��� �����', mcx)
		end
	end)
	sampRegisterChatCommand('/unfwarn', function(unfwarn_id)
		if #unfwarn_id == 0 then unfwarn_id = '' end
		sampSendChat('/unfwarn '..unfwarn_id)
	end)
	sampRegisterChatCommand('unfwarn', function(unfwarn_id)
		if configuration.main_settings.rank > 7 then
			if tonumber(unfwarn_id) then
				if sampIsPlayerConnected(tonumber(unfwarn_id)) then
					lua_thread.create(function()
		                sampSendChat("/me {sex:������|�������} �� �������� ������ �����������")
		                wait(2500)
		                sampSendChat("/me {sex:������|�������} ���������� "..sampGetPlayerNickname(tonumber(unfwarn_id)))
		                wait(2500)
		                sampSendChat("/me � ���� {sex:������|�������} ����� ������ �������")
		                wait(500)
						sampSendChat('/unfwarn '..tostring(unfwarn_id)..' '..tostring(funwarn_reason))
						if configuration.main_settings.autoF8 then
							wait(1000)
							sampSendChat('/time')
							wait(500)
							setVirtualKeyDown(VK_F8, true)
							wait(0)
							setVirtualKeyDown(VK_F8, false)
						end
		            end)
		        else
		        	sampAddChatMessage(tag..'������ ������ ��� � ����!', mcx)
		        end
			else
				sampAddChatMessage(tag..'���������: /unfwarn [id]', mcx)
			end
		else
			sampAddChatMessage(tag..'������� �������� ������ � 8-��� �����', mcx)
		end
	end)
	sampRegisterChatCommand('uval', function(id_player)
		if configuration.main_settings.rank > 8 then
			if #id_player == 0 then 
				sampAddChatMessage(tag..sc..'������ ������������:'..wc..' /uval [id] [�������] [�� ������� (0 ���� ���)]', mcx)
				uninvite_id.v, uninvite_reason.v, bl_reason.v = '', '', ''
				name_uval, uval_res, chs_res = '', '', ''
				uval.v = true
			else
				local id_u, reason_u, bl_u = id_player:match('(%d+) (.+) (.+)')
				if tonumber(id_u) and reason_u and bl_u then
					if bl_u == '0' then 
						bl_reason.v = ''
					else
						bl_reason.v = u8(bl_u)
					end
					uninvite_id.v = tostring(id_u)
					uninvite_reason.v = u8(reason_u)
					uval.v = true
				else
					sampAddChatMessage('[������] '..wc..'/uval [id] [�������] [�� ������� (0 ���� ���)]', mcx)
				end
			end
		else
			sampAddChatMessage(tag..'������� �������� ������ � 9-��� �����', mcx)
		end
	end)

	lua_thread.create(checkOrg)
	lua_thread.create(timer)
	lua_thread.create(members)
	fontChecker = renderCreateFont(configuration.main_settings.members_fontName, configuration.main_settings.members_fontSize, configuration.main_settings.members_fontFlag)
 
	while true do

		if configuration.main_settings.style == 2 then
		    mc, sc, wc = '{BD6DF3}', '{F85CFF}', '{FFFFFF}'
			mcx = 0xBD6DF3
			tag = mc..'[ Bank ] '..wc
		else
			mc, sc, wc = '{006AC2}', '{006D86}', '{FFFFFF}'
			mcx = 0x006AC2
			tag = mc..'[ Bank ] '..wc
		end

		if configuration.main_settings.timeF9 then 
			if isKeyJustPressed(VK_F9) then 
				sampSendChat('/time')
			end
		end

		if configuration.main_settings.rpbat then 
			frpbat()
		end

		if isKeyJustPressed(81) then
	        local validtar, pedtar = getCharPlayerIsTargeting(playerHandle)
	        if validtar and doesCharExist(pedtar) then
	            local result, id = sampGetPlayerIdByCharHandle(pedtar)
	            if result then
	            	local _, selfid = sampGetPlayerIdByCharHandle(playerPed) 
	            	if getCharActiveInterior(playerPed) == 22 then
		            	if sampGetPlayerColor(selfid) == 2150206647 then
			            	sampAddChatMessage(tag..'��������� '..mc..'Esc'..wc..', ��� �� ������� ����', mcx)
			            	actionId = id
			                int_bank.v = true
			            else
			            	sampAddChatMessage(tag..'�� �� � ������� �����!', mcx)
			            end
			        else
				    	sampAddChatMessage(tag..'�� ������ ��������� � ������ �����!', mcx)
				    end
	            end
	        end
    	end

    	if isKeyJustPressed(71) then
	        local validtar, pedtar = getCharPlayerIsTargeting(playerHandle)
	        if validtar and doesCharExist(pedtar) then
	            local result, id = sampGetPlayerIdByCharHandle(pedtar)
	            if result then
	            	local _, selfid = sampGetPlayerIdByCharHandle(playerPed)
	            	if sampGetPlayerColor(selfid) == 2150206647 then
		                expelId = id
		                go_expel()
		            else
		            	sampAddChatMessage(tag..'�� �� � ������� �����!', mcx)
		            end
	            end
	        end
    	end

    	if isKeyJustPressed(85) then
	        local validtar, pedtar = getCharPlayerIsTargeting(playerHandle)
	        if validtar and doesCharExist(pedtar) then
	            local result, id = sampGetPlayerIdByCharHandle(pedtar)
	            if result then
	            	if configuration.main_settings.rank > 8 then
		            	local _, selfid = sampGetPlayerIdByCharHandle(playerPed)
		                uninvite_id.v = tostring(id)
		                uninvite_reason.v = u8'�.�.�.'
		                bl_reason.v = u8'�.�.�.'
		                uval.v = true
		            else
		            	sampAddChatMessage(tag..'������� �������� ������ � 9-��� �����', mcx)
		            end
	            end
	        end
    	end

    	if isKeyJustPressed(82) then
	        local validtar, pedtar = getCharPlayerIsTargeting(playerHandle)
	        if validtar and doesCharExist(pedtar) then
	            local result, id = sampGetPlayerIdByCharHandle(pedtar) 
	            if result then
	                actionId = id
	                sampAddChatMessage(tag..'����� '..mc..sampGetPlayerNickname(actionId)..wc..' ������ � �������� �������� '..mc..'{select_id/name}', mcx)
	            end
	        end
    	end

    	if get_kassa then
	    	for i = 0, 2048 do
				if sampIs3dTextDefined(i) then
					local text, color, posX, posY, posZ, distance, ignoreWalls, playerId, vehicleId = sampGet3dTextInfoById(i)
					if text:find("����� %d") and getDistanceBetween3dText(i) < 3 then
				 		kassa_number = text:match('����� (%d)')
				 		id_kassa = tonumber(i)
				   	end
				end
			end
		end

    	if testCheat('BB') and not bank.v and not int_bank.v and not uval.v and not sampIsDialogActive() then
    		type_window.v = 1
	    	bank.v = true
	    end

	    if sampGetChatInputText() == '!����' then
            sampSetChatInputText(configuration.main_settings.forma_post)
            sampSetChatInputCursor(0, 0)
        end
        if sampGetChatInputText() == '!������' then
            sampSetChatInputText(configuration.main_settings.forma_lect)
            sampSetChatInputCursor(0, 0)
        end
        if sampGetChatInputText() == '!�����' then
            sampSetChatInputText(configuration.main_settings.forma_tr)
            sampSetChatInputCursor(0, 0)
        end

        if status_button_gov then 
        	if tonumber(os.date("%S", os.time())) == 00 and antiflud then
	        	if tonumber(os.date("%H", os.time())) == hGov.v and tonumber(os.date("%M", os.time())) == mGov.v - 1 then
        			antiflud = false
	        		if bNotf then
				    	notify.addNotify(mc.."Bank-Helper", "����� "..mc.."1"..wc.." ������ GOV �����\n"..sc.."����������� � ����!", 2, 2, 10)
					else
						sampAddChatMessage(tag..'����� '..mc..'1'..wc..' ������ GOV �����, '..sc..'����������� � ����!', mcx)
					end
	        	end
	        end
        	if tonumber(os.date("%H", os.time())) == hGov.v and tonumber(os.date("%M", os.time())) == mGov.v then 
        		status_button_gov = false
        		goGov()
        	end
        end

        if configuration.main_settings.members_state and sampGetPlayerColor(select(2, sampGetPlayerIdByCharHandle(playerPed))) == 2150206647 then

            local X = members_posX.v -- ������� �� X
            local Y = members_posY.v -- ������� �� Y

            renderFontDrawText(fontChecker, '������ � ����������� ['..tostring(getMembersCount)..']:', X, Y - 20, 0xFF009D86)
            if #membersList > 0 then
	            for key, value in pairs(membersList) do
	                renderFontDrawText(fontChecker, value, X, Y, 0xFFFFFFFF)
	                Y = Y + configuration.main_settings.members_fontOffset
	                if key > tonumber(getMembersCount) then break end
	            end
	        else
	        	renderFontDrawText(fontChecker, '�������� ��������...\n�������� ��� ���������� ����', X, Y, 0xFFFFFFFF)
	        end
        end

	   	if bank.v or int_bank.v or ustav_window.v or uval.v or infoupdate.v or premium.v then 
	        imgui.ShowCursor = true
	        imgui.Process = true
	    elseif kassa_stat.v then
	        imgui.ShowCursor = false
	        imgui.Process = true
		    if not sampIsCursorActive() and not kip.v then
		    	showCursor(false)
		    end
	    else
	        imgui.Process = false
	    end
	wait(0)
	end
end

function sampev.onCreate3DText(id, color, position, distance, testLOS, attachedPlayerId, attachedVehicleId, text)
	if text:find('�����') and text:find(sampGetPlayerNickname(select(2, sampGetPlayerIdByCharHandle(playerPed)))) then
		kassaX, kassaY, kassaZ = position.x, position.y, position.z
		kassa_stat.v = true
		kassa_name.v = text:match('����: (.+)\n���������')
		kassa_time.v = tostring(text:match('�� �����: {FFA441}(%d+){FFFFFF} �����'))
		return false
	end
	if kassa_name.v ~= '' and text:find(kassa_name.v) and text:find('��������') then
		kassa_name.v = ''
		kassa_stat.v = false
	end
end

function sampev.onPlaySound(sound, pos)
	if getMB and sound == 1052 then 
		return false
	end
end

function checkOrg()
	while not sampIsLocalPlayerSpawned() do wait(1000) end
	if sampIsLocalPlayerSpawned() then
		getRankInStats = true
		sampSendChat('/stats')
	end
end

function goGov()
	lua_thread.create(function()
		if gosDep.v then 
			sampSendChat('/d [����]-[����] ������� ���.�����, ������� �� ����������')
			wait(delayGov.v)
		end
	    sampSendChat('/gov '..u8:decode(govstr1.v))
	    wait(delayGov.v)
	    sampSendChat('/gov '..u8:decode(govstr2.v))
	    wait(delayGov.v)
	    sampSendChat('/gov '..u8:decode(govstr3.v))
	    if gosDep.v then 
			sampSendChat('/d [����]-[����] ���������� ���.�����')
		end
		if gosScreen.v then
			wait(500)
			sampSendChat('{screen}')
		end
	end)
end

function sampev.onServerMessage(clr, msg) -- ��� ����
	local other, get_rank = string.match(msg, '(.+) ������� �� (%d+) �����')
	if other and rank ~= nil then
		if tonumber(get_rank) > 0 and tonumber(get_rank) < 10 then
			sampAddChatMessage(tag..'�����������! '..other..' ������� ��� �� '..mc..get_rank..wc..' �����!', mcx)
			sampAddChatMessage(tag..'������ ��� ����: '..mc..configuration.nameRank[tonumber(get_rank)], mcx)
			sampAddChatMessage(tag..'�� �������� ������� �������� � /time', mcx)
			configuration.main_settings.rank = tonumber(get_rank)
			inicfg.save(configuration, 'Bank_Config.ini')
		end
	end

	if msg:find(sampGetPlayerNickname(select(2, sampGetPlayerIdByCharHandle(playerPed)))..' ������������� � ����������� ������') then
		if bNotf then
			notify.addNotify(mc..'Bank-Helper', '�� ��������� ������� ����!\n������ ���������!', 2, 2, 5)
		else
			sampAddChatMessage(tag..'�� ��������� ���� ������� ����!', mcx)
			sampAddChatMessage(tag..'������ ���������!', mcx)
		end
		return false
	end

	if msg:find(sampGetPlayerNickname(select(2, sampGetPlayerIdByCharHandle(playerPed)))..' ������������� � ������� ������') then
		if bNotf then
			notify.addNotify(mc..'Bank-Helper', '�� ������ ������� ����!\n������������ ������!', 2, 2, 5)
			if configuration.main_settings.members_state then
				getMB = true
			    sampSendChat('/members')
			    membersList = {}
			end
		else
			sampAddChatMessage(tag..'�� ������ ���� ������� ����!', mcx)
			sampAddChatMessage(tag..'������������ ������!', mcx)
		end
		return false
	end

	local expel_bank, expel_name, expel_reason = string.match(msg, '%[i%] (.+){FFFFFF} ������ (.+) �� �����! �������: (.+)')
	if expel_bank and expel_name and expel_reason and configuration.Chat.msg_expel then 
		sampAddChatMessage(tag..'��������� '..mc..expel_bank..wc..' ������(�) �� ����� '..mc..expel_name, mcx)
		sampAddChatMessage(tag..'�������� �������: '..mc..expel_reason, mcx)
		return false
	end

	local shtraf_procent, shtraf_name = string.match(msg, '����������� �������� (.+) �� ������ ������ ������� (.+)%.')
	if shtraf_procent and shtraf_name and configuration.Chat.msg_shtrafs then 
		sampAddChatMessage(tag..'� ����� ����� ������ '..mc..shtraf_procent, mcx)
		sampAddChatMessage(tag..'�������: ������ ������� ������� '..mc..shtraf_name, mcx)
		return false
	end
	if msg:find('��������� ������ ���� ������������ ����� �������� �����') and configuration.Chat.msg_shtrafs then
		return false
	end

	local incazna_name, incazna_sum = string.match(msg, '{FFFFFF}(.+) {73B461}�������� ���� ����������� �� {FFFFFF}(.+)')
	if incazna_name and incazna_sum and configuration.Chat.msg_incazna then 
		sampAddChatMessage(tag..'��������� '..mc..incazna_name..wc..' ��������(�) ����� ����� �� '..mc..incazna_sum, mcx)
		return false
	end

	local invite_name, invite_bank = string.match(msg, '������������ ������ ����� ����� ����������� (.+), �������� ���������: (.+)%.')
	if invite_name and invite_bank and configuration.Chat.msg_invite then 
		sampAddChatMessage(tag..'� ����������� �������(�): '..mc..invite_name..wc..'. ��� ������(�): '..mc..invite_bank, mcx)
		return false
	end

	local uval_bank, uval_member, uval_reason = string.match(msg, '{FFFFFF}(.+) ������ (.+) �� �����������. �������: (.+)')
	if uval_bank and uval_member and uval_reason and configuration.Chat.msg_uval then 
		sampAddChatMessage(tag..'��������� '..mc..uval_member..wc..' ��� ������ � �������� '..mc..uval_reason, mcx)
		sampAddChatMessage(tag..'���������: '..mc..uval_bank, mcx)
		return false
	end

	local credit_accept_name = string.match(msg, '%[����������%] {FFFFFF}�� ������� ������ ������ ������ {73B461}(.+)')
	if credit_accept_name then
		if bNotf then
    		notify.addNotify(mc.."Bank-Helper", "������ ������ �����������\n������ ��������!", 2, 2, 8)
    	else
    		sampAddChatMessage(tag..'����� '..credit_accept_name..' ������ ���� ����������� �� �������', mcx)
    	end
		return false
	end

	if msg:find('%[R%]') and not msg:find('%[����������%]') and clr == 766526463 then
		if msg:find('Jeffy_Cosmo%[%d+%]:') then
			local r, g, b, a = imgui.ImColor(configuration.main_settings.colorRchat):GetRGBA()
			local getDevId = msg:match('Jeffy_Cosmo%[(%d+)%]:')
			local devNick_msg = msg:gsub('Jeffy_Cosmo%[%d+%]:', string.format('{00FFB5}Jeffy_Cosmo['..tostring(getDevId)..']:{%06X}', join_rgb(r, g, b)))
			sampAddChatMessage(devNick_msg, join_rgb(r, g, b))
			return false
		else
			local r, g, b, a = imgui.ImColor(configuration.main_settings.colorRchat):GetRGBA()
			sampAddChatMessage(msg, join_rgb(r, g, b))
			return false
		end
	end

	if msg:find('%[D%]') and clr == 865730559 then
		local r, g, b, a = imgui.ImColor(configuration.main_settings.colorDchat):GetRGBA()
		sampAddChatMessage(msg, join_rgb(r, g, b))
		return false
	end

	if msg:find('%[������%] {FFFFFF}��� ������ ��������� �� ��������� �������!') then
		if bNotf then
    		notify.addNotify(mc.."Bank-Helper", "������ ��������� �� ������\n����������� ����� ������", 2, 2, 8)
    	else
    		sampAddChatMessage(tag..'������ ��������� �� ��������� �������', mcx)
    	end
		credit_send = false
		return false
	end
	if msg:find('%[������%] {FFFFFF}� ����� �������� ��� ���� ������������� � �����!') and credit_send then
		if bNotf then
    		notify.addNotify(mc.."Bank-Helper", "� ����� ������ ���\n���� ����������� �� �������\n������ ����� ������", 2, 2, 8)
    	else
    		sampAddChatMessage(tag..'� �������� ��� ���� ����������� �� �������! ������ ����� ������', mcx)
    	end
		sampSendChat('� ���������, ����������, ��� �� ��� ��� �������� ���� ������, �������� ������� ���')
		credit_send = false
		return false
	end

	if msg:find('%[������%] {FFFFFF}�� ������ �� ������!') and {dep_minus or dep_plus or card_create or card_recreate or dep_check or credit_send} then 
		dep_minus = false
		dep_plus = false
		card_create = false
		card_recreate = false
		dep_check = false
		credit_send = false
		if bNotf then
    		notify.addNotify(mc.."Bank-Helper", "�������� ��������!\n����� ������ �� ���", 2, 2, 5)
    	else
    		sampAddChatMessage(tag..'�������� ��������! ����� ������ �� ���', mcx)
    	end
		return false
	end

	if msg:find('%[������%] {FFFFFF}� ����� �������� ��� ���� ���������� �����!') and {dep_minus or dep_plus or card_create or card_recreate or dep_check or credit_send} then 
		card_create = false
		if bNotf then
    		notify.addNotify(mc.."Bank-Helper", "� ����� ������\n��� ���� ���������� �����", 2, 2, 5)
    	else
    		sampAddChatMessage(tag..'� ����� ������ ��� ���� ���������� �����!', mcx)
    	end	
		return false
	end
	

	if msg:find('%[������%] {FFFFFF}� ����� �������� ������������ �������!') and card_create then
		if bNotf then
    		notify.addNotify(mc.."Bank-Helper", "� ����� ������\n������������ �������\n��� ������ ���� ������", 2, 2, 8)
    	else
    		sampAddChatMessage(tag..'� ������ ������������ ������� ��� ������ ���� ������', mcx)
    	end	
		sampSendChat('������ ������ ����� 3.000$, � ���, ��� � ���� ����� ����� ���, ��������� � ������ ���')
		card_create = false
		return false
	end

	if msg:find('%[����������%]') and (msg:find('�������� �����') or msg:find('������� ��������') or msg:find('Jeffy_Cosmo')) then
		if msg:find('%[%d+%]:%(%(') then
			local remake = msg:gsub('%(%( %[����������%]', '((')
			local remake = remake:gsub('%[R%]', '[����������]')
			local remake = remake:gsub('%[%d+%]:%(%(', ': ((')
			sampAddChatMessage(remake, 0xFFAE00)
		else
			local remake = msg:gsub(': %[����������%]', ':')
			local remake = remake:gsub('%[R%]', '[����������]')
			local remake = remake:gsub('%[%d+%]', '')
			sampAddChatMessage(remake, 0xFFAE00)
		end
		return false
	end

	if msg:find('%[����������%] {FFFFFF}�� �������� ����!') then 
		kassa_stat.v = false
		if bNotf then
			notify.addNotify(mc..'Bank-Helper', '�� �������� ����!', 2, 2, 5)
		else
			sampAddChatMessage(tag..'�� �������� ����!', -1)
		end
		return false
	end

	if msg:find('���� � ����� �������� � 5%-�� �����!') then 
		return false
	end
end

function sampev.onShowDialog(dialogId, style, title, button1, button2, text) -- ��� ��������
 	--// DEPOSITE [-]
	if dialogId == 713 and dep_minus then
		sampSendDialogResponse(713, 1, 6, nil)
		return false
	end
	if dialogId == 0 and dep_minus then 
		if text:find('����� {73B461}%d{FFFFFF} �����') then
			local hourDep = text:match('����� {73B461}(%d){FFFFFF} �����')
			local schet_dep = {"���", "����", "����", "����", "�����"}
			if schet_dep[tonumber(hourDep)] then
				if bNotf then
		    		notify.addNotify(mc.."Bank-Helper", "���� ����� �� �����\n� ������ ������ ������������\n���������� ���� ("..tostring(hourDep).."�.)", 2, 2, 8)
		    	else
		    		sampAddChatMessage(tag..'���� ����� ������ ����� ������ � �������� ������ ����� '..mc..tostring(hourDep)..wc..' '..schet_dep[tonumber(hourDep)], mcx)
		    	end	
				sampSendChat('��������, �� ����� � �������� �� ������� ������ ����� '..tostring(hourDep)..' '..schet_dep[tonumber(hourDep)])
				dep_minus = false
				return false
			else sampAddChatMessage(tag..'������', mcx) end
		else
			dep_minus = false
		end
	end
	--// DEPOSITE [+]
	if dialogId == 713 and dep_plus then
		sampSendDialogResponse(713, 1, 5, nil)
		dep_plus = false
		return false
	end
	--// DEPOSITE [check]
	if dialogId == 713 and dep_check then
		sampSendDialogResponse(713, 1, 7, nil)
		return false
	end
	if dialogId == 0 and dep_check then
		if text:find('����� {73B461}%d{FFFFFF} ����!') then
			local time = text:match('����� {73B461}(%d){FFFFFF} ����!')
			local schet_dep = {"����� 1 ���", "����� 2 ����", "����� 3 ����", "����� 4 ����", "����� 5 �����"}
			sampSendChat('C���� ������ � �������� �� ������� '..schet_dep[tonumber(time)])
			dep_check = false
			return false
		else
			sampSendChat('C���� ������ � �������� �� ������ ��� ����� ������')
			dep_check = false
			return false
		end
	end
	--// CARD [Create]
	if dialogId == 713 and card_create then
		sampSendDialogResponse(713, 1, 3, nil)
		return false
	end
	if dialogId == 0 and card_create then
		card_create = false
	end
	--// CARD [ReCreate]
	if dialogId == 713 and card_recreate then
		sampSendDialogResponse(713, 1, 4, nil)
		card_recreate = false
		return false
	end
	--// CREDIT
	if dialogId == 713 and credit_send then
		sampSendDialogResponse(713, 1, 0, nil)
		return false
	end
	if dialogId == 227 and credit_send then
		sampSendDialogResponse(227, 1, -1, tostring(credit_sum.v))
		return false
	end
	if dialogId == 228 and credit_send then
		sampSendDialogResponse(228, 1, -1, nil)
		credit_send = false
		if bNotf then
    		notify.addNotify(mc.."Bank-Helper", "�� ���������� �����\n������ �� �����: "..mc..tostring(credit_sum.v)..'$', 2, 2, 5)
    	else
    		sampAddChatMessage(tag..'�� ���������� ����� ������ �� ����� '..mc..tostring(credit_sum.v)..'$'..wc..' ������ '..mc..sampGetPlayerNickname(actionId), mcx)
    	end
    	if kassa_stat.v then configuration.timer.kassa_credit = configuration.timer.kassa_credit + 1 end 
		return false
	end
	
	if dialogId == 32 and report_false then
		if bNotf then
    		notify.addNotify(mc.."Bank-Helper", "������ ������� ���������!\n�������� ������", 2, 2, 5)
    	else
    		sampAddChatMessage(tag..'������ ������� ���������! �������� ������', mcx)
    	end
        report_false = false 
        return false
    end

    if dialogId == 235 and getRankInStats then
    	if not text:find('����������� ����') then
	    	lua_thread.create(function()
	    		wait(10000)
	    		sampAddChatMessage(tag..'�� �� � ����������� '..mc..'����������� ����', mcx)
	    		sampAddChatMessage(tag..'������ �������� ������ � ���� �����������', mcx)
	    		sampAddChatMessage(tag..'���� �� ��������, ��� ��� ������ - �������� ������������ '..mc..'vk.com/opasanya', mcx)
	    		sampAddChatMessage(tag..'������ ��������..', mcx)
	    		no_error_msg = true
	    		sampfuncsLog(mc..'Bank-Helper:'..wc..' ������ ��������. �� �� � ����������� "����������� ����"')
	    		thisScript():unload()
    		end) 
    		return false
    	end

    	for DialogLine in text:gmatch('[^\r\n]+') do
    		local nameRankStats, getStatsRank = DialogLine:match('���������: {B83434}(.+)%p(%d+)%p')
    		if tonumber(getStatsRank) then
    			if nameRankStats:find('������� ��������') then getStatsRank = 10 end
    			if tonumber(getStatsRank) ~= configuration.main_settings.rank then
	    			configuration.main_settings.rank = tonumber(getStatsRank)
	    			if inicfg.save(configuration, 'Bank_Config.ini') then 
	    				if updateHand then
	    					sampAddChatMessage(tag..'���� ������� �� '..tostring(nameRankStats)..'('..tostring(getStatsRank)..')', mcx)
	    				end
	    			end
	    		else
	    			if updateHand then
	    				sampAddChatMessage(tag..'��� ���� ������������� ����� � /stats!', mcx)
	    				sampAddChatMessage(tag..'���� ��� �� ��� - ���������� � ������������', mcx)
	    				updateHand = false
	    			end
	    		end
    		end
    	end
    	getRankInStats = false
    	return false
    end
    
    if dialogId == 2015 and st_members.v then -- ��� /premium
    	for DialogLine in text:gmatch('[^\r\n]+') do
    		if DialogLine:find('.+%p%d+%p\t.+%p9%p') and not DialogLine:find('{FF3B3B}') then getmembers[9] = getmembers[9] + 1 end
    		if DialogLine:find('.+%p%d+%p\t.+%p8%p') and not DialogLine:find('{FF3B3B}') then getmembers[8] = getmembers[8] + 1 end
    		if DialogLine:find('.+%p%d+%p\t.+%p7%p') and not DialogLine:find('{FF3B3B}') then getmembers[7] = getmembers[7] + 1 end
    		if DialogLine:find('.+%p%d+%p\t.+%p6%p') and not DialogLine:find('{FF3B3B}') then getmembers[6] = getmembers[6] + 1 end
    		if DialogLine:find('.+%p%d+%p\t.+%p5%p') and not DialogLine:find('{FF3B3B}') then getmembers[5] = getmembers[5] + 1 end
    		if DialogLine:find('.+%p%d+%p\t.+%p4%p') and not DialogLine:find('{FF3B3B}') then getmembers[4] = getmembers[4] + 1 end
    		if DialogLine:find('.+%p%d+%p\t.+%p3%p') and not DialogLine:find('{FF3B3B}') then getmembers[3] = getmembers[3] + 1 end
    		if DialogLine:find('.+%p%d+%p\t.+%p2%p') and not DialogLine:find('{FF3B3B}') then getmembers[2] = getmembers[2] + 1 end
    		if DialogLine:find('.+%p%d+%p\t.+%p1%p') and not DialogLine:find('{FF3B3B}') then getmembers[1] = getmembers[1] + 1 end
    	end
    	if text:find('��������� ��������') then
    		sampSendDialogResponse(2015, 1, 15, _)
    		if text:find('��������� ��������') then
	    		sampSendDialogResponse(2015, 1, 15, _)
	    		if text:find('��������� ��������') then
		    		sampSendDialogResponse(2015, 1, 15, _)
		    		if text:find('��������� ��������') then
			    		sampSendDialogResponse(2015, 1, 15, _)
			    		if text:find('��������� ��������') then
				    		sampSendDialogResponse(2015, 1, 15, _)
				    	else sampSendChat('/lmenu') end
			    	else sampSendChat('/lmenu') end
		    	else sampSendChat('/lmenu') end
	    	else sampSendChat('/lmenu') end
    	else sampSendChat('/lmenu') end
    	return false
    end

    if dialogId == 2015 and not st_members.v and not getMB then 
    	for DialogLine in text:gmatch('[^\r\n]+') do
    		if DialogLine:find('{.+}%a+_%a+%(%d+%)\t.+%(%d+%)\t%d+\t%d+') then
	    		local rName, rNum = DialogLine:match('{.+}%a+_%a+%(%d+%)\t(.+)%((%d+)%)\t%d+\t%d+')
	    		if configuration.nameRank[tonumber(rNum)] ~= tostring(rName) then
	    			sampAddChatMessage(tag..'���� '..mc..configuration.nameRank[tonumber(rNum)]..wc..' ������� �� '..mc..tostring(rName)..wc..'!', mcx)
	    			configuration.nameRank[tonumber(rNum)] = tostring(rName)
	    		end
	    	end
    	end
    end

    if dialogId == 2015 and getMB then -- ��� ������
    	getMembersCount = title:match('%(� ����: (%d+)%)')
    	for DialogLine in text:gmatch('[^\r\n]+') do
    		local getFormaInList, getNameInList, getIdInList, getRankInList, getRankNumberInList, getWarnInList, getAFKInList = DialogLine:match('{(.+)}(%a+_%a+)%((%d+)%)\t(.+)%((%d+)%)\t(%d+)\t(%d+)')
    		
    		if getNameInList and getIdInList and getRankInList and getRankNumberInList and getWarnInList and getAFKInList and getFormaInList then
    			if getFormaInList == 'FF3B3B' then formaState = '{D84848}' else formaState = '{FFFFFF}' end
    			if getFormaInList == 'FFFFFF' and tonumber(getAFKInList) > members_afk.v then getAFKInList = '{FF0000}'..getAFKInList end
    			table.insert(membersList, formaState..'['..getRankNumberInList..'] '..getNameInList..'['..getIdInList..'] >> AFK: '..getAFKInList)
    		end
    	end
    	if text:find('��������� ��������') then
    		sampSendDialogResponse(2015, 1, 15, _)
    		if text:find('��������� ��������') then
	    		sampSendDialogResponse(2015, 1, 15, _)
	    		if text:find('��������� ��������') then
		    		sampSendDialogResponse(2015, 1, 15, _)
		    		if text:find('��������� ��������') then
			    		sampSendDialogResponse(2015, 1, 15, _)
			    		if text:find('��������� ��������') then
				    		sampSendDialogResponse(2015, 1, 15, _)
				    	else getMB = false end
			    	else getMB = false end
		    	else getMB = false end
	    	else getMB = false end
    	else getMB = false end
    	return false
    end

    if dialogId == 1214 and st_members.v then -- ����� ����� (9+)
    	bank_cazna.v = title:match('����: {E1E948}(%d+)')
    	st_members.v = false
    	return false
    end

    if dialogId == 0 and text:find('�� ������� ��������� ����������� �� ����� ������') then
    	if bNotf then
    		notify.addNotify(mc.."Bank-Helper", "����������� "..mc.."������� ������"..wc.."\n������� ����������", 2, 2, 5)
    	else
    		sampAddChatMessage(tag..'����������� ������� ������ ������� ����������', mcx)
    	end
    	if kassa_stat.v then configuration.timer.kassa_recard = configuration.timer.kassa_recard + 1 end
		return false
	end
	if dialogId == 0 and text:find('�� ������� ���� {73B461}������{FFFFFF} �����, ��� ���������� ��������') then
    	if bNotf then
    		notify.addNotify(mc.."Bank-Helper", "����� "..mc.."����������"..wc.." ��������\n������� ������!", 2, 2, 5)
    	else
    		sampAddChatMessage(tag..'����� ���������� �������� ������� ������!', mcx)
    	end
    	if kassa_stat.v then configuration.timer.kassa_dep = configuration.timer.kassa_dep + 1 end
		return false
	end
	if dialogId == 0 and text:find('�� ������� ���� ������ {73B461}�����{FFFFFF}, ��� ��������� ������ ��������') then
    	if bNotf then
    		notify.addNotify(mc.."Bank-Helper", "����� "..mc.."���������"..wc.." ��������\n������� ������!", 2, 2, 5)
    	else
    		sampAddChatMessage(tag..'����� ���������� �������� ������� ������!', mcx)
    	end
    	if kassa_stat.v then configuration.timer.kassa_dep = configuration.timer.kassa_dep + 1 end
		return false
	end
	if dialogId == 0 and text:find('����� ���������� ���������� �����') then
    	if bNotf then
    		notify.addNotify(mc.."Bank-Helper", "����� ��� "..mc.."��������� �����"..wc.."\n������� ������!", 2, 2, 5)
    	else
    		sampAddChatMessage(tag..'����� ��� ��������� ���������� ����� ������� ������!', mcx)
    	end
    	if kassa_stat.v then configuration.timer.kassa_card = configuration.timer.kassa_card + 1 end
		return false
	end
end

local fa_font = nil
local fontsize20 = nil
local fa_glyph_ranges = imgui.ImGlyphRanges({ fa.min_range, fa.max_range })
function imgui.BeforeDrawFrame()
    if fa_font == nil then
        local font_config = imgui.ImFontConfig()
        font_config.MergeMode = true

        fa_font = imgui.GetIO().Fonts:AddFontFromFileTTF('moonloader/resource/fonts/fa-solid-900.ttf', 13.0, font_config, fa_glyph_ranges)
    end
    if fontsize20 == nil then
        fontsize20 = imgui.GetIO().Fonts:AddFontFromFileTTF(getFolderPath(0x14) .. '\\trebucbd.ttf', 35.0, nil, imgui.GetIO().Fonts:GetGlyphRangesCyrillic())
    end
end


function imgui.OnDrawFrame()
	local ex, ey = getScreenResolution()
	if uval.v then
        imgui.SetNextWindowSize(imgui.ImVec2(335, 205), imgui.Cond.FirstUseEver)
		imgui.SetNextWindowPos(imgui.ImVec2(ex / 2, ey / 2), imgui.Cond.FirstUseEver, imgui.ImVec2(0.5, 0.5))
		imgui.Begin(u8'���� ����������', uval, imgui.WindowFlags.NoCollapse + imgui.WindowFlags.NoResize + imgui.WindowFlags.NoScrollbar)

		imgui.CenterTextColoredRGB('������� '..mc..'ID{SSSSSS} ������ � '..mc..'�������{SSSSSS} ����������')
		imgui.NewLine()
		imgui.SetCursorPosY(65)
		imgui.PushItemWidth(120)
		imgui.InputText(u8"##uninvite_id", uninvite_id, imgui.InputTextFlags.CharsDecimal)
		imgui.PopItemWidth()
		if not imgui.IsItemActive() and #uninvite_id.v == 0 then
			imgui.SameLine(15)
			imgui.PushStyleColor(imgui.Col.Text, imgui.ImVec4(0.5, 0.5, 0.5, 1))
			imgui.Text(u8'ID')
			imgui.PopStyleColor()
		end
		imgui.PushItemWidth(120)
		imgui.InputText(u8"##uninvite_reason", uninvite_reason)
		imgui.PopItemWidth()
		if not imgui.IsItemActive() and #uninvite_reason.v == 0 then
			imgui.SameLine(15)
			imgui.PushStyleColor(imgui.Col.Text, imgui.ImVec4(0.5, 0.5, 0.5, 1))
			imgui.Text(u8'�������')
			imgui.PopStyleColor()
		end
		imgui.PushItemWidth(120)
		imgui.InputText(u8"##bl_reason", bl_reason)
		imgui.HintHovered(u8'�������� ������\n���� ��� ��')
		imgui.PopItemWidth()
		if not imgui.IsItemActive() and #bl_reason.v == 0 then
			imgui.SameLine(15)
			imgui.PushStyleColor(imgui.Col.Text, imgui.ImVec4(0.5, 0.5, 0.5, 1))
			imgui.Text(u8'������� ��')
			imgui.PopStyleColor()
		end 
		imgui.SetCursorPos(imgui.ImVec2(140, 65))
		imgui.BeginChild("##UvalInfo", imgui.ImVec2(-1, 68), true, imgui.WindowFlags.NoScrollbar)
		if #uninvite_id.v > 0 then
			if sampIsPlayerConnected(tonumber(uninvite_id.v)) then
				name_uval = '{SSSSSS}'..sampGetPlayerNickname(tonumber(uninvite_id.v))
			else
				name_uval = '{FF0000}��� �� �������!'
			end
		else
			name_uval = '{565656}�������� ID'
		end
		imgui.CenterTextColoredRGB(mc..'���: '..name_uval)
		if #uninvite_reason.v == 0 then
			uval_res = '{565656}�� �������' 
		else 
			if #bl_reason.v == 0 then
				uval_res = u8:decode(uninvite_reason.v) 
			else
				uval_res = u8:decode(uninvite_reason.v)..' + ��'
			end
		end
		imgui.CenterTextColoredRGB(mc..'�������: {SSSSSS}'..uval_res)
		if #bl_reason.v == 0 then
			chs_res = '{565656}��� ��' 
		else 
			chs_res = '{SSSSSS}'..u8:decode(bl_reason.v)
		end
		imgui.CenterTextColoredRGB(mc..'������� ��: '..chs_res)

		imgui.EndChild()
		imgui.NewLine()
		if #uninvite_id.v > 0 and #uninvite_reason.v > 0 then
			elsebtn()
			if imgui.Button(u8('������� ')..fa.ICON_FA_MINUS_CIRCLE, imgui.ImVec2(-1, 40)) then
				uval.v = false
				lua_thread.create(function()
	                sampSendChat('/me {sex:������|�������} �� ������� ��� � {sex:�������|��������} ���')
	                wait(2500)
	                sampSendChat('/me {sex:������|�������} ������ �����������')
	                wait(2500)
	                sampSendChat('/me {sex:������|�������} ���� ���������� '..sampGetPlayerNickname(tonumber(uninvite_id.v)))
	                wait(2500)
	                sampSendChat('/me {sex:������|�������} ���������� �� �����������')
	                wait(500)
	                sampSendChat('/uninvite '..uninvite_id.v..' '..tostring(uval_res))
	                if configuration.main_settings.autoF8 then
						wait(1000)
						sampSendChat('/time')
						wait(500)
						setVirtualKeyDown(VK_F8, true)
						wait(0)
						setVirtualKeyDown(VK_F8, false)
					end
	                if #bl_reason.v > 0 then
						wait(2000)
						sampSendChat('/me {sex:��������|���������} ������ �׸���� ������')
						wait(2500)
						sampSendChat('/me {sex:����|������} '..sampGetPlayerNickname(tonumber(uninvite_id.v))..' � ������ ������ �����������')
						wait(500)
						sampSendChat('/blacklist '..uninvite_id.v..' '..tostring(u8:decode(bl_reason.v)))
						if configuration.main_settings.autoF8 then
							wait(1000)
							sampSendChat('/time')
							wait(500)
							setVirtualKeyDown(VK_F8, true)
							wait(0)
							setVirtualKeyDown(VK_F8, false)
						end
					end
					uninvite_id.v, uninvite_reason.v, bl_reason.v = '', '', ''
					name_uval, uval_res, chs_res = '', '', ''
	            end)
			end
			endbtn()
		else
			imgui.PushDisableButton()
			imgui.Button(u8('������� ')..fa.ICON_FA_MINUS_CIRCLE, imgui.ImVec2(-1, 40))
			imgui.PopDisableButton()
		end
		imgui.End()
	end

	if int_bank.v then
		local _, selfid = sampGetPlayerIdByCharHandle(playerPed)
		local boolSkin, id_skin = sampGetPlayerSkin(actionId)
        imgui.SetNextWindowSize(imgui.ImVec2(465, 295), imgui.Cond.FirstUseEver)
		imgui.SetNextWindowPos(imgui.ImVec2(ex / 2, ey - 305), imgui.Cond.FirstUseEver, imgui.ImVec2(0.5, 0))
        imgui.Begin(u8'##IntMenuBank', _, imgui.WindowFlags.NoCollapse + imgui.WindowFlags.NoResize + imgui.WindowFlags.NoTitleBar + imgui.WindowFlags.NoScrollbar)
	        imgui.BeginChild("##ActionIdInfo", imgui.ImVec2(150, 180), true, imgui.WindowFlags.NoScrollbar)
	        imgui.CenterTextColoredRGB(mc..'��� ������')
	        if sampIsPlayerConnected(actionId) then
	        	imgui.CenterTextColoredRGB(sampGetPlayerNickname(actionId)..'['..actionId..']')
	        	if imgui.IsItemClicked() then
		            if setClipboardText(u8:decode((sampGetPlayerNickname(actionId)))) then
		            	sampAddChatMessage(tag..'��� '..mc..sampGetPlayerNickname(actionId)..wc..' ���������� � ������ ������!', mcx)
		            end
		        else
		        	imgui.HintHovered(u8'�����, ���-�� �����������')
		        end
		        if TypeAction.v == 2 then 
		        	if blg.red.r:find(sampGetPlayerNickname(actionId)) then 
						imgui.CenterTextColoredRGB('{FF0000}�� ������� �������!')
					elseif blg.ylw.y:find(sampGetPlayerNickname(actionId)) then 
						imgui.CenterTextColoredRGB('{FF0000}�� Ƹ���� �������!')
					else
						imgui.NewLine()
					end
		        else
		        	imgui.NewLine()
		        end
		        imgui.CenterTextColoredRGB(mc..'��� � �����')
		        if TypeAction.v ~= 2 then 
		        	imgui.CenterTextColoredRGB(tostring(sampGetPlayerScore(actionId))..' ���')
		        else
		        	if sampGetPlayerScore(actionId) > 2 then
		        		imgui.CenterTextColoredRGB(tostring(sampGetPlayerScore(actionId))..' ��� | ��������')
		        	else
		        		imgui.CenterTextColoredRGB(tostring(sampGetPlayerScore(actionId))..' ��� | {FF0000}�� ��������')
		        	end
		        end
		    else
	        	int_bank.v = false
	        	sampAddChatMessage(tag..'����� ����� �� ����!', mcx)
	        end
	        imgui.NewLine()
	        imgui.CenterTextColoredRGB(mc..'��� ����')
		    if boolSkin then 
		        imgui.CenterTextColoredRGB('���� �'..id_skin)
		        if not howdress(id_skin) then
		        	imgui.CenterTextColoredRGB('(���������)')
		        else
					imgui.CenterTextColoredRGB('{FF0000}(�� ���������)')
		        end
		    else
		    	imgui.CenterTextColoredRGB('���� �� ��������')
		    end
	        imgui.EndChild()
	        imgui.SameLine()
	        if TypeAction.v == 2 or TypeAction.v == 3 then
	        	imgui.BeginChild("##Actions", imgui.ImVec2(-1, 275), true, imgui.WindowFlags.NoScrollbar)
	        end
	        	if TypeAction.v == 1 then
			        if not go_credit.v then
			        	imgui.SetCursorPos(imgui.ImVec2(165, 10))
				        if imgui.Button(u8('����������� ')..fa.ICON_FA_CHILD, imgui.ImVec2(-1, 30)) then
				        	int_bank.v = false
							lua_thread.create(function()
			                    sampSendChat("/do �� ����� ������� �"..configuration.nameRank[configuration.main_settings.rank].."�")
			                    wait(1000)
			                    sampSendChat("������������, � "..configuration.nameRank[configuration.main_settings.rank].." - {my_name}")
			                    wait(2500)
			                    sampSendChat("��� ���� ��� ������?")
		                    end)
						end
						imgui.SetCursorPos(imgui.ImVec2(165, 45))
						if configuration.main_settings.rank > 4 then
							if imgui.Button(u8('�������: ����� ')..fa.ICON_FA_MINUS_CIRCLE, imgui.ImVec2(250, 30)) then
								int_bank.v = false
								lua_thread.create(function()
				                    sampSendChat("/me {sex:������|�������} �� �������� ������ ����������")
				                    wait(2500)
				                    sampSendChat("/me {sex:������|�������} ����� ������� � {sex:�����|������} �������������")
				                    wait(2500)
				                    sampSendChat("/me {sex:����|�����} ������������ ����� � {sex:�������|��������} "..sampGetPlayerNickname(actionId))
				                    wait(500)
				                    dep_minus = true
									sampSendChat('/bankmenu '..actionId)
			                    end)
							end
						else
							imgui.PushDisableButton()
							imgui.Button(u8('�������: ����� ')..fa.ICON_FA_LOCK, imgui.ImVec2(250, 30))
							imgui.HintHovered(u8'�������� � 5+ �����')
							imgui.PopDisableButton()
						end
						imgui.SetCursorPos(imgui.ImVec2(165, 80))
						if configuration.main_settings.rank > 4 then
							if imgui.Button(u8('�������: ��������� ')..fa.ICON_FA_PLUS_CIRCLE, imgui.ImVec2(250, 30)) then
								int_bank.v = false
								lua_thread.create(function()
				                    sampSendChat("/me {sex:������|�������} �� �������� ������ ����������")
				                    wait(2500)
				                    sampSendChat("/me {sex:������|�������} ����� ����������� � {sex:�����|������} �������������")
				                    wait(2500)
				                    sampSendChat("/me {sex:����|�����} ������������ ����� � {sex:�������|��������} "..sampGetPlayerNickname(actionId))
				                    wait(500)
				                    dep_plus = true
									sampSendChat('/bankmenu '..actionId)
			                    end)
							end
						else
							imgui.PushDisableButton()
							imgui.Button(u8('�������: ��������� ')..fa.ICON_FA_LOCK, imgui.ImVec2(250, 30))
							imgui.HintHovered(u8'�������� � 5+ �����')
							imgui.PopDisableButton()
						end
						imgui.SetCursorPos(imgui.ImVec2(165, 115))
						if configuration.main_settings.rank > 5 then
							if imgui.Button(u8('������ ������ ')..fa.ICON_FA_CALCULATOR, imgui.ImVec2(-1, 30)) then
								go_credit.v = true
								lua_thread.create(function()
									sampAddChatMessage(tag..'��������� ���������� ���������!', mcx)
				                    sampSendChat("/me {sex:������|�������} �� �������� ������ �������������")
				                    wait(2500)
				                    sampSendChat("/me {sex:������|�������} ����� ����������� ������� � {sex:�����|������} �������������")
				                    wait(2500)
				                    sampSendChat("/me {sex:����|�����} ������������ ����� � {sex:�����|������} ��������� ���")
				                    wait(2500)
				                    sampSendChat("����� ��� �������, ����������")
			                    end)
							end
						else
							imgui.PushDisableButton()
							imgui.Button(u8('������ ������ ')..fa.ICON_FA_LOCK, imgui.ImVec2(-1, 30))
							imgui.HintHovered(u8'�������� � 6+ �����')
							imgui.PopDisableButton()
						end
						imgui.SetCursorPos(imgui.ImVec2(165, 150))
						if configuration.main_settings.rank > 4 then
							if imgui.Button(u8('������ ���� ����� ')..fa.ICON_FA_MONEY_CHECK, imgui.ImVec2(-1, 30)) then
								int_bank.v = false
								lua_thread.create(function()
				                    sampSendChat("/me {sex:������|�������} �� �������� ���� ������")
				                    wait(2500)
				                    sampSendChat("/me ����� �� "..sampGetPlayerNickname(actionId).." ���-�� ������� � ����")
									sampSendChat('/bankmenu '..actionId)
									sampSendDialogResponse(713, 1, 1, nil)
			                    end)
							end
						else
							imgui.PushDisableButton()
							imgui.Button(u8('������ ���� ����� ')..fa.ICON_FA_LOCK, imgui.ImVec2(-1, 30))
							imgui.HintHovered(u8'�������� � 5+ �����')
							imgui.PopDisableButton()
						end
						imgui.SetCursorPos(imgui.ImVec2(165, 185))
						if configuration.main_settings.rank > 4 then
							if imgui.Button(u8('������ ���-�� ����� � ����� ')..fa.ICON_FA_PIGGY_BANK, imgui.ImVec2(-1, 30)) then
								int_bank.v = false
								lua_thread.create(function()
				                    sampSendChat("/me {sex:������|�������} �� �������� ���� ������")
				                    wait(2500)
				                    sampSendChat("/me ����� �� "..sampGetPlayerNickname(actionId).." ���-�� ������� � ����")
									sampSendChat('/bankmenu '..actionId)
									sampSendDialogResponse(713, 1, 2, nil)
			                    end)
							end
						else
							imgui.PushDisableButton()
							imgui.Button(u8('������ ���-�� ����� � ����� ')..fa.ICON_FA_LOCK, imgui.ImVec2(-1, 30))
							imgui.HintHovered(u8'�������� � 5+ �����')
							imgui.PopDisableButton()
						end
						imgui.SetCursorPos(imgui.ImVec2(165, 220))
						if configuration.main_settings.rank > 4 then
							if imgui.Button(u8('������ ����� ')..fa.ICON_FA_CREDIT_CARD, imgui.ImVec2(-1, 30)) then
								int_bank.v = false
								lua_thread.create(function()
				                    sampSendChat("/me {sex:������|�������} �� �������� ������ ����������� ������")
				                    wait(2500)
				                    sampSendChat("/me {sex:������|�������} ����� �������� ����� � {sex:�����|������} �������������")
				                    wait(2500)
				                    sampSendChat("/me {sex:����|�����} ������������ ����� � {sex:��������|���������} ������ ������")
				                    wait(3000)
				                    sampSendChat("/todo ��������� ���� ����� � ����� ����!*������ �� "..sampGetPlayerNickname(actionId))
				                    wait(500)
				                    card_create = true
									sampSendChat('/bankmenu '..actionId)
			                    end)
							end
						else
							imgui.PushDisableButton()
							imgui.Button(u8('������ ����� ')..fa.ICON_FA_LOCK, imgui.ImVec2(-1, 30))
							imgui.HintHovered(u8'�������� � 5+ �����')
							imgui.PopDisableButton()
						end
						imgui.SetCursorPos(imgui.ImVec2(165, 255))
						if configuration.main_settings.rank > 4 then
							if imgui.Button(u8('������������ ����� ')..fa.ICON_FA_RECYCLE, imgui.ImVec2(-1, 30)) then
								imgui.OpenPopup(u8("����� ��������##Card"))
							end
						else
							imgui.PushDisableButton()
							imgui.Button(u8('������������ ����� ')..fa.ICON_FA_LOCK, imgui.ImVec2(-1, 30))
							imgui.HintHovered(u8'�������� � 5+ �����')
							imgui.PopDisableButton()
						end
						if imgui.BeginPopupModal(u8("����� ��������##Card"), _, imgui.WindowFlags.NoCollapse + imgui.WindowFlags.NoScrollbar + imgui.WindowFlags.AlwaysAutoResize) then
							imgui.CenterTextColoredRGB(mc..'�������� ��������!\n���� ����� �������� �������, ������\n�������� �� ���� � /report?')
							imgui.NewLine()
							if imgui.Button(u8('�������� � /report'), imgui.ImVec2(150, 30)) then
								imgui.CloseCurrentPopup()
								report_false = true
								sampSendChat('/report')
								sampSendDialogResponse(32, 1, -1, '��������� '..sampGetPlayerNickname(actionId)..'['..actionId..'] �� ����� (����)')
							end
							imgui.SameLine()
							if imgui.Button(u8('����������� �����'), imgui.ImVec2(150, 30)) then
								imgui.CloseCurrentPopup()
								int_bank.v = false
								lua_thread.create(function()
									sampSendChat('�������� ��������, ������ ������ �������� ��� � 30.000$')
									wait(2500)
				                    sampSendChat("/me {sex:������|�������} �� �������� ������ ����������� ������")
				                    wait(2500)
				                    sampSendChat("/me {sex:������|�������} ����� ����������� ���� � {sex:�����|������} �������������")
				                    wait(2500)
				                    sampSendChat("/me {sex:����|�����} ������������ ����� � {sex:��������|���������} ������ ������")
				                    wait(3000)
				                    sampSendChat("/todo ��������� ������� ��� ��� ������� � ����� ����*��������� ����� "..sampGetPlayerNickname(actionId))
				                    wait(500)
				                    card_recreate = true
									sampSendChat('/bankmenu '..actionId)
			                    end)
							end
							elsebtn()
							if imgui.Button(u8('�����'), imgui.ImVec2(-1, 30)) then
								imgui.CloseCurrentPopup()
							end
							endbtn()
							imgui.EndPopup()
						end
						imgui.SetCursorPos(imgui.ImVec2(420, 45))
						if configuration.main_settings.rank > 4 then
							if imgui.Button(fa.ICON_FA_CLOCK, imgui.ImVec2(35, 65)) then
								int_bank.v = false
								lua_thread.create(function()
				                    sampSendChat("/me {sex:������|�������} �� �������� ���� ������")
				                    wait(2500)
				                    sampSendChat("/me ����� �� "..sampGetPlayerNickname(actionId).." ���-�� ������� � ����")
				                    dep_check = true
									sampSendChat('/bankmenu '..actionId)
			                    end)
							end
						else
							imgui.PushDisableButton()
							imgui.Button(fa.ICON_FA_LOCK, imgui.ImVec2(35, 65))
							imgui.HintHovered(u8'�������� � 5+ �����')
							imgui.PopDisableButton()
						end
					else
						imgui.BeginChild("##CreditWindow", imgui.ImVec2(-1, 275), true, imgui.WindowFlags.NoScrollbar)
							imgui.CenterTextColoredRGB(mc..'���������� ������� ��\n'..mc..'������ '..sc..sampGetPlayerNickname(actionId))
							imgui.NewLine()
							imgui.PushItemWidth(170)
							if imgui.InputInt(u8'����� �������', credit_sum) then
								if credit_sum.v < 5000 then credit_sum.v = 5000 end
								if credit_sum.v > 300000 then credit_sum.v = 300000 end
							end
							imgui.PopItemWidth()
							imgui.NewLine()
							imgui.CenterTextColoredRGB(mc..'���������� ������� ������������\n'..mc..'�� ������������� ������� ������������\n'..sc..'������������ � ��� �� ������ ���:')
							imgui.SetCursorPosX((imgui.GetWindowWidth() - 150) / 2)
							if imgui.Button(u8('������� ������������##credit'), imgui.ImVec2(150, 20)) then
								imgui.OpenPopup(u8("������� ������������"))
							end
							system_credit()
							imgui.NewLine()
							elsebtn()
							if imgui.Button(u8('������ ������ �� '..credit_sum.v..'$'), imgui.ImVec2(-1, 30)) then
								go_credit.v = false
								int_bank.v = false
								lua_thread.create(function()
				                    sampSendChat("/me {sex:��������|���������} ��������� �����")
				                    wait(2500)
				                    sampSendChat("/me ������ ������ ������ �� ����������� ������")
				                    wait(2500)
				                    sampSendChat("/me ������� ������� ����� "..sampGetPlayerNickname(actionId))
				                    wait(2500)
				                    sampSendChat("��������� ������� � ������ ������ � ������ ������ "..credit_sum.v..'$ ���')
				                    wait(500)
									sampSendChat('/bankmenu '..actionId)
									credit_send = true
			                    end)
							end
							endbtn()
							if imgui.Button(u8('�������� ��������'), imgui.ImVec2(-1, 30)) then
								go_credit.v = false
								sampSendChat("/me {sex:�������|��������} ����� � �������")
							end
						imgui.EndChild()
					end
				end
				if TypeAction.v == 2 then
					imgui.CenterTextColoredRGB(mc..'������������ ��������:')
					imgui.CenterTextColoredRGB('� 3 ���� ���������� � ����� (3+ ���)')
					imgui.CenterTextColoredRGB('� 35 � ����� �����������������')
					imgui.CenterTextColoredRGB('� ��������� ���������� � ���������')
					imgui.CenterTextColoredRGB('� ��������� ���������������� {868686}(?)')
					if imgui.IsItemHovered() then
                        imgui.BeginTooltip()
                        imgui.PushTextWrapPos(450)
                        imgui.TextUnformatted(u8'����������� �� 5 ��.')
                        imgui.PopTextWrapPos()
                        imgui.EndTooltip()       
                    end
					imgui.CenterTextColoredRGB('� ���������� ���������')
					imgui.NewLine()
					if imgui.Button(u8('�����������'), imgui.ImVec2(-1, 30)) then
						lua_thread.create(function()
		                    sampSendChat("������������, � "..configuration.nameRank[configuration.main_settings.rank].." - {my_name}")
		                    wait(2500)
		                    sampSendChat("�� �� �������������?")
	                    end)
					end
					if imgui.Button(u8('�������'), imgui.ImVec2(88, 30)) then
						lua_thread.create(function()
		                    sampSendChat("������ �����, �������� ��� �������.")
		                    wait(1000)
		                    sampSendChat("/b /showpass "..selfid.." + 1-2 ���������")
	                    end)
					end
					imgui.SameLine()
					if imgui.Button(u8('���. �����'), imgui.ImVec2(88, 30)) then
						lua_thread.create(function()
		                    sampSendChat("������� � �������, ������ ����� ���� ���-�����?")
		                    wait(1000)
		                    sampSendChat("/b /showmc "..selfid.." + 1-2 ���������")
	                    end)
					end
					imgui.SameLine()
					if imgui.Button(u8('��������'), imgui.ImVec2(-1, 30)) then
						lua_thread.create(function()
		                    sampSendChat("�������, �������� ������� ���� ��������. ��������� ��� ��")
		                    wait(1000)
		                    sampSendChat("/b /showlic "..selfid.." + 1-2 ���������")
	                    end)
					end
					if question.v == 1 then 
						if imgui.Button(u8('����-����� [������ �1]'), imgui.ImVec2(-1, 30)) then
							lua_thread.create(function()
			                    sampSendChat("���, ������ ���� ���������� ��� ���..")
			                    wait(2500)
			                    sampSendChat("������ �� ��������� ���� ����������� ����������� �� ������?")
		                    end)
		                    question.v = 2
						end
					end
					if question.v == 2 then 
						if imgui.Button(u8('����-����� [������ �2]'), imgui.ImVec2(-1, 30)) then
			                sampSendChat("������� ����. ����� �������?")
		                    question.v = 3
						end
					end
					if question.v == 3 then 
						if imgui.Button(u8('����-����� [������ �3]'), imgui.ImVec2(-1, 30)) then
							lua_thread.create(function()
				                sampSendChat("������������ �������� ����� ������?")
				                wait(1000)
								sampSendChat("/b ������ ����� = �����.")
				                sampAddChatMessage(tag..'������� ���������! ����� ��������� �����!', mcx)
			                    question.v = 1
			                end)
						end
					end
				    imgui.PushStyleColor(imgui.Col.ButtonHovered, imgui.ImVec4(0.00, 0.40, 0.00, 1.00))
				    imgui.PushStyleColor(imgui.Col.ButtonActive, imgui.ImVec4(0.00, 0.30, 0.00, 1.00))
					if imgui.Button(u8('������� ')..fa.ICON_FA_USER_PLUS, imgui.ImVec2(135, 30)) then
						if not blg.red.r:find(sampGetPlayerNickname(actionId)) then 
							if not blg.ylw.y:find(sampGetPlayerNickname(actionId)) then 
								if configuration.main_settings.rank > 8 then
									question.v = 1
									int_bank.v = false
									lua_thread.create(function()
										int_bank.v = false
					                    sampSendChat("����������! �� ��� ���������!")
					                    wait(2500)
					                    sampSendChat("/me ������ �� ����� ���� �� ����� � �������, ������� ��� "..sampGetPlayerNickname(actionId))
					                    wait(2500)
					                    sampSendChat("/invite "..actionId)
					                    sampSendChat("������������� � ������������ � �������� �����.")
					                    if configuration.main_settings.autoF8 then
											wait(1000)
											sampSendChat('/time')
											wait(500)
											setVirtualKeyDown(VK_F8, true)
											wait(0)
											setVirtualKeyDown(VK_F8, false)
										end
				                    end)
				                else
				                	question.v = 1
				                	get_kassa = true
									int_bank.v = false
									lua_thread.create(function()
										int_bank.v = false
					                    sampSendChat("����������! �� ��� ���������!")
					                    wait(2500)
					                    sampSendChat("������ � ������ ����������, ��� �� �� ��� ������! ���� ����������..")
					                    wait(2500)
					                    if sampIs3dTextDefined(id_kassa) and getDistanceBetween3dText(id_kassa) < 3 then 
								 			sampSendChat('/r ����� ������� �� ����� �'..tostring(kassa_number)..', ����� ������� �������� ���������� �������������')
								 		else
								 			sampSendChat('/r ����� ������� �� ���, ���-�� ������� �������� ���������� �������������')
								 		end
								 		get_kassa = false
				                    end)
				                end
			                else
				            	sampAddChatMessage(tag..'�� �� ������ ������� ����� ������ �� �������!', mcx)
				            	sampAddChatMessage(tag..'����� '..mc..sampGetPlayerNickname(actionId)..wc..' ��������� � {FF9D00}�� ����� �������{FFFFFF} �������������!', mcx)
				            end
			            else
			            	sampAddChatMessage(tag..'�� �� ������ ������� ����� ������ �� �������!', mcx)
			            	sampAddChatMessage(tag..'����� '..mc..sampGetPlayerNickname(actionId)..wc..' ��������� � {FF0000}�� ������� �������{FFFFFF} �������������!', mcx)
			            end
					end
    				imgui.PopStyleColor(2)
					imgui.SameLine()
					imgui.PushStyleColor(imgui.Col.ButtonHovered, imgui.ImVec4(0.40, 0.00, 0.00, 1.00))
    				imgui.PushStyleColor(imgui.Col.ButtonActive, imgui.ImVec4(0.30, 0.00, 0.00, 1.00))
					if imgui.Button(u8('��������� ')..fa.ICON_FA_USER_TIMES, imgui.ImVec2(-1, 30)) then
						imgui.OpenPopup(u8("������� ����������"))
						question.v = 1
					end
					imgui.PopStyleColor(2)
					if imgui.BeginPopupModal(u8("������� ����������"), _, imgui.WindowFlags.NoCollapse + imgui.WindowFlags.NoScrollbar + imgui.WindowFlags.AlwaysAutoResize) then
						imgui.CenterTextColoredRGB(mc..'������� ������� ����������')
						if imgui.Button(u8('�������� � �������� (NicName)'), imgui.ImVec2(250, 30)) then
							imgui.CloseCurrentPopup()
							int_bank.v = false
							lua_thread.create(function()
			                    sampSendChat("� ���������, �� �� ���������, � ��� �������� � ��������")
			                    wait(2500)
			                    sampSendChat("/b ����� �������")
			                    wait(2500)
			                    sampSendChat("��������� ��� ������������� � ��������� ���!")
		                    end)
						end
						if imgui.Button(u8('���� ��� � �����'), imgui.ImVec2(250, 30)) then
							imgui.CloseCurrentPopup()
							int_bank.v = false
							lua_thread.create(function()
			                    sampSendChat("� ���������, �� �� ���������, �� ���������� ���� ��� � �����")
			                    wait(2500)
			                    sampSendChat("/b ����� 3+ ������� ���������")
			                    wait(2500)
			                    sampSendChat("��������� � ������ ���!")
		                    end)
						end
						if imgui.Button(u8('������������'), imgui.ImVec2(250, 30)) then
							imgui.CloseCurrentPopup()
							int_bank.v = false
							lua_thread.create(function()
			                    sampSendChat("� ���������, �� �� ���������, �� ��� �������������")
			                    wait(2500)
			                    sampSendChat("������������ � ��������� ��� ���!")
		                    end)
						end
						if imgui.Button(u8('����� �����������������'), imgui.ImVec2(250, 30)) then
							imgui.CloseCurrentPopup()
							int_bank.v = false
							lua_thread.create(function()
			                    sampSendChat("� ���������, �� �� ���������, �� ����������������")
			                    wait(2500)
			                    sampSendChat("/b 35+ ����������������� - /showpass "..actionId)
			                    wait(2500)
			                    sampSendChat("��������� � ������ ���!")
		                    end)
						end
						if imgui.Button(u8('����������������'), imgui.ImVec2(250, 30)) then
							imgui.CloseCurrentPopup()
							int_bank.v = false
							lua_thread.create(function()
			                    sampSendChat("� ���������, �� �� ���������, �� �������������")
			                    wait(2500)
			                    sampSendChat("/b �������� 3 ���������������� - /showmc "..actionId)
			                    wait(2500)
			                    sampSendChat("���������� � ��������� ��� ���!")
		                    end)
						end
						if imgui.Button(u8('����������� ����������'), imgui.ImVec2(250, 30)) then
							imgui.CloseCurrentPopup()
							int_bank.v = false
							lua_thread.create(function()
			                    sampSendChat("� ���������, �� �� ���������, �� ������ � ��������������� ��������")
			                    wait(2500)
			                    sampSendChat("/b ���������, �������� ���. ����� - /showpass "..actionId)
			                    wait(2500)
			                    sampSendChat("�������� ����� ���-����� � ��������� ��� ���!")
		                    end)
						end
						if imgui.Button(u8('������'), imgui.ImVec2(250, 30)) then
							imgui.CloseCurrentPopup()
							int_bank.v = false
							lua_thread.create(function()
			                    sampSendChat("� ���������, �� �� ���������, �� �������")
			                    wait(2500)
			                    sampSendChat("/b ����������� ��� ���������� � RP ���")
			                    wait(2500)
			                    sampSendChat("��������� � ��������� ��� ���!")
		                    end)
						end
						if imgui.Button(u8('����� ������� �������'), imgui.ImVec2(250, 30)) then
							imgui.CloseCurrentPopup()
							int_bank.v = false
						end
						elsebtn()
						if imgui.Button(u8('�����'), imgui.ImVec2(250, 30)) then
							imgui.CloseCurrentPopup()
						end
						endbtn()
						imgui.EndPopup()
					end
				end
				if TypeAction.v == 3 then
					imgui.CenterTextColoredRGB(mc..'�������� ���� ������� ������')
					imgui.CenterTextColoredRGB(sc..'������� ����� ������� �������������\n'..sc..'�� �������� � ������� "��������"\n'..sc..'�� ������ �������!')
					imgui.NewLine()

		        	imgui.SetCursorPosX(33)
		        	imgui.TextDisabled('1'); imgui.SameLine(60)
		        	imgui.TextDisabled('2'); imgui.SameLine(88)
		        	imgui.TextDisabled('3'); imgui.SameLine(115)
		        	imgui.TextDisabled('4'); imgui.SameLine(142)
		        	imgui.TextDisabled('5'); imgui.SameLine(168)
		        	imgui.TextDisabled('6'); imgui.SameLine(196)
		        	imgui.TextDisabled('7'); imgui.SameLine(223)
		        	imgui.TextDisabled('8'); imgui.SameLine(249)
		        	imgui.TextDisabled('9')

		        	imgui.SetCursorPosX(28)
		        	imgui.RadioButton(u8("##giverank1"), giverank, 1); imgui.HintHovered(u8(configuration.nameRank[1])); imgui.SameLine()
		        	imgui.RadioButton(u8("##giverank2"), giverank, 2); imgui.HintHovered(u8(configuration.nameRank[2])); imgui.SameLine()
		        	imgui.RadioButton(u8("##giverank3"), giverank, 3); imgui.HintHovered(u8(configuration.nameRank[3])); imgui.SameLine()
		        	imgui.RadioButton(u8("##giverank4"), giverank, 4); imgui.HintHovered(u8(configuration.nameRank[4])); imgui.SameLine()
		        	imgui.RadioButton(u8("##giverank5"), giverank, 5); imgui.HintHovered(u8(configuration.nameRank[5])); imgui.SameLine()
		        	imgui.RadioButton(u8("##giverank6"), giverank, 6); imgui.HintHovered(u8(configuration.nameRank[6])); imgui.SameLine()
		        	imgui.RadioButton(u8("##giverank7"), giverank, 7); imgui.HintHovered(u8(configuration.nameRank[7])); imgui.SameLine()
		        	imgui.RadioButton(u8("##giverank8"), giverank, 8); imgui.HintHovered(u8(configuration.nameRank[8])); imgui.SameLine()
		        	imgui.RadioButton(u8("##giverank9"), giverank, 9); imgui.HintHovered(u8(configuration.nameRank[9]));

		        	imgui.NewLine()
		        	imgui.SetCursorPosX(80)
		        	if imgui.Checkbox(u8'� �� ����������', upWithRp) then 
		        		configuration.main_settings.upWithRp = upWithRp.v
		        	end

		        	imgui.NewLine()
		        	elsebtn()
		        	if imgui.Button(u8('�������� ')..fa.ICON_FA_SORT_NUMERIC_UP, imgui.ImVec2(-1, 40)) then
						if upWithRp.v then 
							int_bank.v = false
							lua_thread.create(function()
								sampSendChat('/me {sex:������|�������} �� ������� ���')
								wait(2500)
								sampSendChat('/me {sex:�������|��������} ��� � {sex:�����|�����} � ������ "����������"')
								wait(2500)
								sampSendChat('/me {sex:������|�������} ���������� '..sampGetPlayerNickname(actionId))
								wait(2500)
								sampSendChat('/me {sex:�������|��������} ��������� ���������� �� '..configuration.nameRank[giverank.v])
								wait(2500)
								sampSendChat('/giverank '..' '..actionId..' '..giverank.v)
								sampSendChat('���������� ���!')
								if configuration.main_settings.rank > 8 then
									if not doesDirectoryExist(getWorkingDirectory()..'/BHelper/�����������') then 
						    			if createDirectory(getWorkingDirectory()..'/BHelper/�����������') then 
						    				sampfuncsLog(mc..'Bank-Helper:'..wc..' ������� ����������� /BHelper/�����������')
						    			end
						    		end
						    		log_giverank = io.open(getWorkingDirectory()..'/BHelper/�����������/���������.txt', "a")
									log_giverank:write(sampGetPlayerNickname(actionId).." | "..tostring(giverank.v - 1).." -> "..tostring(giverank.v).." | "..os.date("%d.%m.%Y").." | "..os.date("%H:%M:%S", os.time()).." | "..sampGetPlayerNickname(select(2, sampGetPlayerIdByCharHandle(playerPed))).."\n")
									log_giverank:close()
								end
								if configuration.main_settings.autoF8 then
									wait(1000)
									sampSendChat('/time')
									wait(500)
									setVirtualKeyDown(VK_F8, true)
									wait(0)
									setVirtualKeyDown(VK_F8, false)
								end
							end)
						else
							int_bank.v = false
							lua_thread.create(function()
								sampSendChat('/giverank '..' '..actionId..' '..giverank.v)
								if configuration.main_settings.autoF8 then
									wait(1000)
									sampSendChat('/time')
									wait(500)
									setVirtualKeyDown(VK_F8, true)
									wait(0)
									setVirtualKeyDown(VK_F8, false)
								end
							end)
						end
					end
					endbtn()
					if imgui.Button(u8('�������� ')..fa.ICON_FA_SORT_NUMERIC_DOWN, imgui.ImVec2(-1, -1)) then
						if upWithRp.v then 
							int_bank.v = false
							lua_thread.create(function()
								sampSendChat('/me {sex:������|�������} �� ������� ���')
								wait(2500)
								sampSendChat('/me {sex:�������|��������} ��� � ����� � ������ "����������"')
								wait(2500)
								sampSendChat('/me {sex:������|�������} ���������� '..sampGetPlayerNickname(actionId))
								wait(2500)
								sampSendChat('/me {sex:�������|��������} ��������� ���������� �� '..configuration.nameRank[giverank.v])
								wait(2500)
								sampSendChat('/giverank '..' '..actionId..' '..giverank.v)
								if configuration.main_settings.autoF8 then
									wait(1000)
									sampSendChat('/time')
									wait(500)
									setVirtualKeyDown(VK_F8, true)
									wait(0)
									setVirtualKeyDown(VK_F8, false)
								end
							end)
						else
							int_bank.v = false
							lua_thread.create(function()
								sampSendChat('/giverank '..' '..actionId..' '..giverank.v)
								if configuration.main_settings.autoF8 then
									wait(1000)
									sampSendChat('/time')
									wait(500)
									setVirtualKeyDown(VK_F8, true)
									wait(0)
									setVirtualKeyDown(VK_F8, false)
								end
							end)
						end
					end
				end
			if TypeAction.v == 2 or TypeAction.v == 3 then
	        	imgui.EndChild()
	        end
				imgui.SetCursorPos(imgui.ImVec2(10, 200))
				imgui.RadioButton(u8("���������� ������"), TypeAction, 1)
				imgui.SetCursorPos(imgui.ImVec2(10, 230))
				if configuration.main_settings.rank > 4 then
        			imgui.RadioButton(u8("C�����������e"), TypeAction, 2)
        		else
        			imgui.RadioButton(u8("C�����������e ")..fa.ICON_FA_LOCK, false)
        			imgui.HintHovered(u8'�������� � 5+ �����')
        		end
        		imgui.SetCursorPos(imgui.ImVec2(10, 260))
        		if configuration.main_settings.rank > 8 then
        			imgui.RadioButton(u8("���������"), TypeAction, 3)
        		else
        			imgui.RadioButton(u8("��������� ")..fa.ICON_FA_LOCK, false)
        			imgui.HintHovered(u8'�������� � 9+ �����')
        		end
        imgui.End()
	end

	if bank.v then
		local _, selfid = sampGetPlayerIdByCharHandle(playerPed) 
		if configuration.main_settings.rank > 4 then
       		imgui.SetNextWindowSize(imgui.ImVec2(553, 295))
       	else
       		imgui.SetNextWindowSize(imgui.ImVec2(553, 265))
       	end
		imgui.SetNextWindowPos(imgui.ImVec2(ex / 2, ey / 2), imgui.Cond.FirstUseEver, imgui.ImVec2(0.5, 0.5))
        imgui.Begin(u8'##MainMenu', _, imgui.WindowFlags.NoCollapse + imgui.WindowFlags.NoResize + imgui.WindowFlags.NoTitleBar)
        imgui.BeginChild("##SelfInfo", imgui.ImVec2(180, 125), true, imgui.WindowFlags.NoScrollbar)

        imgui.CenterTextColoredRGB(mc..'���� ���')
        imgui.CenterTextColoredRGB(sampGetPlayerNickname(selfid)..' ['..selfid..']')
        imgui.Separator()
        imgui.CenterTextColoredRGB(mc..'���� ���������')
        imgui.CenterTextColoredRGB(sc..configuration.nameRank[configuration.main_settings.rank]..' ('..configuration.main_settings.rank..')')
        if imgui.IsItemClicked() then
            getRankInStats = true
            updateHand = true
            sampSendChat('/stats')
        else
        	imgui.HintHovered(u8'�����, ���-�� ��������')
        end
        imgui.Separator()
        imgui.CenterTextColoredRGB(mc..'������ �� ���� {565656}(?)')
        imgui.HintHovered(u8'������ � ��� �� �����������!')
        imgui.CenterTextColoredRGB(get_timer(configuration.timer.online))
        imgui.EndChild()
        imgui.SameLine()
        if configuration.main_settings.rank > 4 then
        	imgui.BeginChild("##MenuActive", imgui.ImVec2(350, 280), true, imgui.WindowFlags.NoScrollbar)
        else
			imgui.BeginChild("##MenuActive", imgui.ImVec2(350, 250), true, imgui.WindowFlags.NoScrollbar)
        end

        if type_window.v == 0 then -- ���� �����������
        	imgui.SetCursorPosX((imgui.GetWindowWidth() - 235) / 2)
            imgui.Image(mainPng, imgui.ImVec2(235, 235))
        end
        if type_window.v == 1 then -- �����
        	if #configuration.Binds_Name > 0 then
        		imgui.CenterTextColoredRGB(mc..'���� ���������������� ������ {868686}(?)')
    			imgui.HintHovered(u8'��������� "BB" (��� ���-���), ���\n�������� �������� ����� ����')
    			imgui.Separator()
        		for key_bind, name_bind in pairs(configuration.Binds_Name) do
        			imgui.PushStyleVar(imgui.StyleVar.ItemSpacing, imgui.ImVec2(2, 2))
        			if imgui.Button(name_bind..'##'..key_bind, imgui.ImVec2(269, 30)) then
                        play_bind(key_bind)
                        bank.v = false
                    end	
                    imgui.SameLine()	
                    if imgui.Button(fa.ICON_FA_PEN..'##'..key_bind, imgui.ImVec2(30, 30)) then
                        EditOldBind = true
                        getpos = key_bind
                        binder_delay.v = configuration.Binds_Deleay[key_bind]
                        local returnwrapped = tostring(configuration.Binds_Action[key_bind]):gsub('~', '\n')
                        text_binder.v = returnwrapped
                        binder_name.v = tostring(configuration.Binds_Name[key_bind])
                        imgui.OpenPopup(u8("��������������/�������� �����"))
                        binder_open = true
                    end
                    imgui.SameLine()
                    if imgui.Button(fa.ICON_FA_TRASH..'##'..key_bind, imgui.ImVec2(30, 30)) then
                        sampAddChatMessage(tag..'���� '..mc..'�'..tostring(u8:decode(configuration.Binds_Name[key_bind]))..'�'..wc..' ����� �� ������ ������!', mcx)
                        table.remove(configuration.Binds_Name, key_bind)
                        table.remove(configuration.Binds_Action, key_bind)
                        table.remove(configuration.Binds_Deleay, key_bind)
                        inicfg.save(configuration, 'Bank_Config.ini')
                    end
                    imgui.PopStyleVar()
        		end
        		elsebtn()
        		if imgui.Button(u8('������� ����� ����'), imgui.ImVec2(-1, 30)) then
					imgui.OpenPopup(u8("��������������/�������� �����"))
					binder_delay.v = 2500
				end
				endbtn()
        	else
        		imgui.CenterTextColoredRGB('{393939}(beta)')
				imgui.SetCursorPosX((imgui.GetWindowWidth() - 100) / 2)
            	imgui.Image(bindsPng, imgui.ImVec2(100, 100))
        		imgui.CenterTextColoredRGB(mc..'�������, � ��� ��� ��� ������..')
        		imgui.NewLine()
        		imgui.SetCursorPosX((imgui.GetWindowWidth() - 80) / 2)
        		if imgui.Button(u8('�������!'), imgui.ImVec2(80, 30)) then
					imgui.OpenPopup(u8("��������������/�������� �����"))
					binder_open = true
				end
        	end
        	binder()
        end
        if type_window.v == 2 then -- ������
        	imgui.CenterTextColoredRGB(mc..'���� ���������� ������ ����������� {868686}(?)')
        	imgui.HintHovered(u8'�� ������ ��������� ���� ������!\n��� ����� � ����� moonloader/BHelper/Lections\n�������� ��������� ���� � �������� ���� ������ ������\n\n� ���������� ���������� ����� ��������� ��������\n����� �����������')
        	imgui.NewLine()
        	if doesDirectoryExist("moonloader/BHelper/Lections") then
				for line in lfs.dir(getWorkingDirectory().."\\BHelper\\Lections") do
				  	if line == nil then
				  	elseif line:match(".+%.txt") then
				  		if imgui.Button(u8(line:match("(.+)%.txt")), imgui.ImVec2(-1, 30)) then
							lua_thread.create(function()
								bank.v = false
								local lection_text = io.open("moonloader/BHelper/Lections/"..line:match("(.+)%.txt")..".txt", "r+")
								for line in lection_text:lines() do
									sampSendChat(line)
									wait(LectDelay.v * 1000)
								end
								lection_text:close()
								sampAddChatMessage(tag..'��������������� ������ ��������!', mcx)
							end)
						end
				  	end
				end
			else
				imgui.NewLine(); imgui.NewLine()
				imgui.CenterTextColoredRGB(sc..'� ��� ����������� ����� � ��������\n�� �� ������ ������� �������������\n��� ����� ������� "������� ������"')
				imgui.NewLine()
				imgui.SetCursorPosX((imgui.GetWindowWidth() - 130) / 2)
				if imgui.Button(u8('������� ������ ')..fa.ICON_FA_DOWNLOAD, imgui.ImVec2(130, 30)) then
					downloadLections()
				end
			end
        end
        if type_window.v == 3 then -- �������
        	imgui.CenterTextColoredRGB(mc..'���������� ��� �����������')
        	imgui.NewLine()

        	if imgui.Button(u8('����� ������������ ����� ')..fa.ICON_FA_BOOK_OPEN, imgui.ImVec2(-1, 35)) then
				ustav_window.v = true
			end
			if imgui.Button(u8('������ ������� ��������� ')..fa.ICON_FA_SORT_AMOUNT_UP, imgui.ImVec2(-1, 35)) then
				imgui.OpenPopup(u8("������ ������� ���������"))
			end
			system_uprank()
			if imgui.Button(u8('�������� ������� ')..fa.ICON_FA_CLOCK, imgui.ImVec2(-1, 35)) then
				imgui.OpenPopup(u8("�������� �������"))
			end
			system_cadr()
			if imgui.Button(u8('������� ������������ ')..fa.ICON_FA_CALENDAR_CHECK, imgui.ImVec2(-1, 35)) then
				imgui.OpenPopup(u8("������� ������������"))
			end
			system_credit()
			if imgui.Button(u8('������������ ������ ')..fa.ICON_FA_FLAG, imgui.ImVec2(-1, 35)) then
				imgui.OpenPopup(u8("������������ ������"))
			end
			post()
        end
        if type_window.v == 4 then -- ��. �����
        	if imgui.Button(u8('׸���� ������ ������������� ')..fa.ICON_FA_BAN, imgui.ImVec2(-1, 35)) then
				imgui.OpenPopup(u8("׸���� ������ �������������"))
			end
			blgovpp()
			if imgui.Button(u8('���� ������ ������ ')..fa.ICON_FA_MONEY_CHECK_ALT, imgui.ImVec2(-1, 35)) then
				if configuration.main_settings.rank > 8 then
					bank.v = false
					for i = 1, 9 do
						getmembers[i] = 0
					end
					st_members.v = true
					sampSendChat('/members')
					premium.v = true
				else
					sampAddChatMessage(tag..'�������� ������ � 9-��� �����!', mcx)
				end
			end
			if imgui.Button(u8('��������������� ����� ')..fa.ICON_FA_GLOBE, imgui.ImVec2(-1, 35)) then
				if configuration.main_settings.rank > 8 then
					imgui.OpenPopup(u8("������������ ��������������� �����"))
				else
					sampAddChatMessage(tag..'�������� ������ � 9-��� �����!', mcx)
				end
			end
			gov()
			if imgui.Button(u8('����� /members ')..fa.ICON_FA_BARS, imgui.ImVec2(-1, 35)) then
				type_window.v = 6
			end
        end

        if type_window.v == 5 then -- ���������

        	imgui.CenterTextColoredRGB(mc..'��������� ����������')
        	imgui.NewLine()
        	if imgui.CollapsingHeader(u8("����� ���������")) then
	        	imgui.PushItemWidth(80)
	        	if imgui.InputInt(u8("�������� ��������� � ������� (���)"), LectDelay) then
	        		if LectDelay.v < 1 then
	        			LectDelay.v = 1
	        		elseif LectDelay.v > 30 then
	        			LectDelay.v = 30
	        		end
	        	end
	        	imgui.PopItemWidth()

	        	if imgui.Checkbox(u8'���������� (�� �����)', ki_stat) then
	        		if configuration.main_settings.rank < 5 then 
	        			ki_stat.v = false
	        			sampAddChatMessage(tag..'�������� � 5 �����!', mcx) 
	        		else
		        		sampAddChatMessage(tag..'���������� �� ����� - '..string.format(ki_stat.v and mc..'��������' or mc..'���������'), mcx)
		        		configuration.main_settings.ki_stat = ki_stat.v
		        		inicfg.save(configuration, 'Bank_Config.ini')
		        		if kassa_stat.v then kassa_stat.v = false end
		        	end
	        	end
	        	imgui.TextColored(imgui.ImVec4(0.7, 0.7, 0.7, 1.0), fa.ICON_FA_INFO_CIRCLE..u8(' ������ ����� ����������� �������� /kip'))

	        	fTimeF9()
	        end

        	imgui.Separator()
        	if imgui.CollapsingHeader(u8("������ ��������� � ����")) then
        		if imgui.Checkbox(u8'��������� � /expel', msg_expel) then
        			configuration.Chat.msg_expel = msg_expel.v
        			inicfg.save(configuration, 'Bank_Config.ini')
        		end
        		imgui.SameLine(); imgui.TextDisabled('(?)')
        		imgui.HintHovered(u8'��� ��������:\n> ��������� Jeffy_Cosmo[228] ������(�) �� ����� Sam_Mason[666]\n> �������� �������: �.�.�.')

        		if imgui.Checkbox(u8'������ �������', msg_shtrafs) then
        			configuration.Chat.msg_shtrafs = msg_shtrafs.v
        			inicfg.save(configuration, 'Bank_Config.ini')
        		end
        		imgui.SameLine(); imgui.TextDisabled('(?)')
        		imgui.HintHovered(u8'��� ��������:\n> � ����� ����� ������ 10 000$\n> �������: ������ ������� ������� Jeffy_Cosmo')

        		if imgui.Checkbox(u8'���������� ����� ������������', msg_incazna) then
        			configuration.Chat.msg_incazna = msg_incazna.v
        			inicfg.save(configuration, 'Bank_Config.ini')
        		end
        		imgui.SameLine(); imgui.TextDisabled('(?)')
        		imgui.HintHovered(u8'��� ��������:\n> ��������� Jeffy_Cosmo[228] �������� ����� ����� �� 1000000$')

        		if imgui.Checkbox(u8'�������� �����������', msg_invite) then
        			configuration.Chat.msg_invite = msg_invite.v
        			inicfg.save(configuration, 'Bank_Config.ini')
        		end
        		imgui.SameLine(); imgui.TextDisabled('(?)')
        		imgui.HintHovered(u8'��� ��������:\n> � ����������� �������: Sam_Mason. ��� ������: Jeffy_Cosmo')

        		if imgui.Checkbox(u8'���������� �����������', msg_uval) then
        			configuration.Chat.msg_uval = msg_uval.v
        			inicfg.save(configuration, 'Bank_Config.ini')
        		end
        		imgui.SameLine(); imgui.TextDisabled('(?)')
        		imgui.HintHovered(u8'��� ��������:\n> ��������� Sam_Mason ��� ������ � �������� �.�.�\n> ���������: Jeffy_Cosmo')
        	end

        	imgui.Separator()
    		if imgui.CollapsingHeader(u8("��������� ���������/�������")) then
    			imgui.TextColoredRGB(mc..'��� ���:')
    			if imgui.RadioButton(u8("�������"), sex, 1) then
    				configuration.main_settings.sex = sex.v
	        		if inicfg.save(configuration, 'Bank_Config.ini') then 
	        			sampAddChatMessage(tag..'��� ������� �� '..mc..'�������', mcx)
	        		end
	        	end
		        if imgui.RadioButton(u8("�������"), sex, 2) then 
		        	configuration.main_settings.sex = sex.v
	        		if inicfg.save(configuration, 'Bank_Config.ini') then 
	        			sampAddChatMessage(tag..'��� ������� �� '..mc..'�������', mcx)
	        		end
		        end

		        imgui.TextColoredRGB(mc..'����-����� + /time {565656}(?)')
		        imgui.HintHovered(u8'��� ���� ���������, ����������, ������ ������ � �.�.\n����� ������������� ��������� � /time')
		        if imgui.Checkbox(u8'��������/���������', autoF8) then 
	        		configuration.main_settings.autoF8 = autoF8.v
	        		inicfg.save(configuration, 'Bank_Config.ini')
	        	end
	        	if autoF8.v then
	        		if not doesFileExist(getGameDirectory()..'/Screenshot.asi') then
	        			if imgui.Button(u8'������� Screenshot.asi', imgui.ImVec2(150, 20)) then 
	        				downloadUrlToFile('https://gitlab.com/uploads/-/system/personal_snippet/1978930/0b4025da038173a8b1ce81d5e3848901/Screenshot.asi', getGameDirectory()..'/Screenshot.asi', function (id, status, p1, p2)
								if status == dlstatus.STATUSEX_ENDDOWNLOAD then
									sampAddChatMessage(tag..'������ '..mc..'Screenshot.asi'..wc..' ��������! {FF0000}����������� � ����..', mcx)
								end
							end)
	        			end
	        			imgui.HintHovered(u8'��������� ������ ������������ ��������� ��� ��������� ����\n����� ���������� ����� ��������� � ����\n�����: MISTER_GONWIK')
	        		else
	        			imgui.TextColoredRGB('{30FF30}������������ Screenshot.asi')
	        		end
	        	end

    			imgui.TextColoredRGB(mc..'������')
    			if imgui.Checkbox(u8'��������/��������� ������', accent_status) then 
	        		configuration.main_settings.accent_status = accent_status.v
	        		inicfg.save(configuration, 'Bank_Config.ini')
	        	end
	        	if configuration.main_settings.accent_status then
	        		imgui.PushItemWidth(280)
	        		imgui.InputText(u8"##accent", accent); imgui.SameLine()
	        		imgui.PopItemWidth()
	        		if imgui.Button('Save##accent', imgui.ImVec2(-1, 20)) then 
	        			configuration.main_settings.accent = u8:decode(accent.v)
	        			if inicfg.save(configuration, 'Bank_Config.ini') then 
	        				sampAddChatMessage(tag..'������ ��������!', mcx)
	        			end
	        		end
	        	end
    			imgui.TextColoredRGB(mc..'����-��������� �������')
    			if imgui.Checkbox(u8'��������/��������� ����-�������', rpbat) then 
	        		configuration.main_settings.rpbat = rpbat.v
	        		inicfg.save(configuration, 'Bank_Config.ini')
	        	end
        		if rpbat.v then
	        		imgui.TextColoredRGB(mc..'����� �������')
	        		imgui.PushItemWidth(280)
	        		imgui.InputText(u8"##rpbat_true", rpbat_true); imgui.SameLine()
	        		imgui.PopItemWidth()
	        		if imgui.Button('Save##1', imgui.ImVec2(-1, 20)) then 
	        			configuration.main_settings.rpbat_true = u8:decode(rpbat_true.v)
	        			if inicfg.save(configuration, 'Bank_Config.ini') then 
	        				sampAddChatMessage(tag..'��������� ���������!', mcx)
	        			end
	        		end

	        		imgui.TextColoredRGB(mc..'������ �������')
	        		imgui.PushItemWidth(280)
	        		imgui.InputText(u8"##rpbat_false", rpbat_false); imgui.SameLine()
	        		imgui.PopItemWidth()
	        		if imgui.Button('Save##2', imgui.ImVec2(-1, 20)) then 
	        			configuration.main_settings.rpbat_false = u8:decode(rpbat_false.v)
	        			if inicfg.save(configuration, 'Bank_Config.ini') then 
	        				sampAddChatMessage(tag..'��������� ���������!', mcx)
	        			end
	        		end
        		end
        	end

        	imgui.Separator()
        	if imgui.CollapsingHeader(u8("��������� ������")) then
        		imgui.PushItemWidth(190)
        		if imgui.Combo(u8'�������� �����', style, {u8"Ҹ����", u8"�������", u8"����������", u8"�����"}) then
					configuration.main_settings.style = style.v
					if inicfg.save(configuration, 'Bank_Config.ini') then 
						sampAddChatMessage(tag..'�������� ����� ��������!', mcx)
					end
					set_style(configuration.main_settings.style)
				end

				if configuration.main_settings.style == 1 then
					if imgui.DragFloat(u8'������������ #1', glass_light, 0.005, 0.0, 1.0) then 
						configuration.main_settings.glass_light = glass_light.v
						inicfg.save(configuration, 'Bank_Config.ini')
						style_light()
					end
				end
				if configuration.main_settings.style == 1 then
					if imgui.DragFloat(u8'������������ #2', glass_light_child, 0.005, 0.0, 1.0) then
						configuration.main_settings.glass_light_child = glass_light_child.v
						inicfg.save(configuration, 'Bank_Config.ini')
						style_light()
					end
				end
				imgui.PopItemWidth()

				if imgui.ColorEdit4(u8'���� ���� �����������', colorRchat, imgui.ColorEditFlags.NoInputs + imgui.ColorEditFlags.NoAlpha) then
		            clr = imgui.ImColor.FromFloat4(colorRchat.v[1], colorRchat.v[2], colorRchat.v[3], colorRchat.v[4]):GetU32()
		            configuration.main_settings.colorRchat = clr
		            inicfg.save(configuration, 'Bank_Config.ini')
		        end
		        imgui.SameLine(imgui.GetWindowWidth() - 150)
		       	if imgui.Button(u8("����##RCol"), imgui.ImVec2(50, 20)) then
	                local r, g, b, a = imgui.ImColor(configuration.main_settings.colorRchat):GetRGBA()
					sampAddChatMessage('[R] �������� Jeffy_Cosmo[228]:(( �������� ��������� ))', join_rgb(r, g, b))
	            end
	        	imgui.SameLine(imgui.GetWindowWidth() - 95)
	        	if imgui.Button(u8("C����������##Rcol"), imgui.ImVec2(90, 20)) then
	                configuration.main_settings.colorRchat = 759410733
	                if inicfg.save(configuration, 'Bank_Config.ini') then 
	                	sampAddChatMessage(tag..'����������� ���� ���� ����������� ������������!', mcx)
	                	colorRchat = imgui.ImFloat4(imgui.ImColor(configuration.main_settings.colorRchat):GetFloat4())
	                end
	            end

	            if imgui.ColorEdit4(u8'���� ���� ������������', colorDchat, imgui.ColorEditFlags.NoInputs + imgui.ColorEditFlags.NoAlpha) then
		            clr = imgui.ImColor.FromFloat4(colorDchat.v[1], colorDchat.v[2], colorDchat.v[3], colorDchat.v[4]):GetU32()
		            configuration.main_settings.colorDchat = clr
		            inicfg.save(configuration, 'Bank_Config.ini')
		        end
		        imgui.SameLine(imgui.GetWindowWidth() - 150)
		        if imgui.Button(u8("����##DCol"), imgui.ImVec2(50, 20)) then
	                local r, g, b, a = imgui.ImColor(configuration.main_settings.colorDchat):GetRGBA()
					sampAddChatMessage('[D] ���������� Sam_Mason[666]: [���-��] - [��] �������� ���������', join_rgb(r, g, b))
	            end
	        	imgui.SameLine(imgui.GetWindowWidth() - 95)
	        	if imgui.Button(u8("C����������##DCol"), imgui.ImVec2(90, 20)) then
	                configuration.main_settings.colorDchat = 771725619
	                if inicfg.save(configuration, 'Bank_Config.ini') then 
	                	sampAddChatMessage(tag..'����������� ���� ���� ������������ ������������!', mcx)
	                	colorDchat = imgui.ImFloat4(imgui.ImColor(configuration.main_settings.colorDchat):GetFloat4())
	                end
	            end
            end
            imgui.Separator()
            
        	if imgui.CollapsingHeader(u8("��������� ����")) then
        		imgui.TextColoredRGB('{868686}(���������)')
        		imgui.HintHovered(u8'��� �� �� ������ ������� ������� �����\n���������� � ������ ���� �������� �������� !����\n� ������ ��� �� ��� ������� � ������ ���� ����� �������')
        		imgui.PushItemWidth(280)
        		imgui.TextColoredRGB(mc..'����� ����� '..'{SSSSSS}'..'(!����):')
        		imgui.InputText('##forma_post', forma_post)
        		imgui.PopItemWidth()
        		imgui.SameLine()
        		if imgui.Button('Save##3', imgui.ImVec2(-1, 20)) then 
        			configuration.main_settings.forma_post = u8:decode(forma_post.v)
        			if inicfg.save(configuration, 'Bank_Config.ini') then 
        				sampAddChatMessage(tag..'����� ���������!', mcx)
        			end
        		end
        		imgui.PushItemWidth(280)
        		imgui.TextColoredRGB(mc..'����� ������ ������ '..'{SSSSSS}'..'(!������):')
        		imgui.InputText('##forma_lect', forma_lect)
        		imgui.PopItemWidth()
        		imgui.SameLine()
        		if imgui.Button('Save##4', imgui.ImVec2(-1, 20)) then 
        			configuration.main_settings.forma_lect = u8:decode(forma_lect.v)
        			if inicfg.save(configuration, 'Bank_Config.ini') then 
        				sampAddChatMessage(tag..'����� ���������!', mcx)
        			end
        		end
        		imgui.PushItemWidth(280)
        		imgui.TextColoredRGB(mc..'����� ������ ���������� '..'{SSSSSS}'..'(!�����):')
        		imgui.InputText('##forma_tr', forma_tr)
        		imgui.PopItemWidth()
        		imgui.SameLine()
        		if imgui.Button('Save##5', imgui.ImVec2(-1, 20)) then 
        			configuration.main_settings.forma_tr = u8:decode(forma_tr.v)
        			if inicfg.save(configuration, 'Bank_Config.ini') then 
        				sampAddChatMessage(tag..'����� ���������!', mcx)
        			end
        		end
        	end
        	imgui.Separator()

        	if imgui.CollapsingHeader(u8("� �������")) then
        		imgui.CenterTextColoredRGB('����� �������: '..mc..'Cosmo')
        		imgui.CenterTextColoredRGB('������: '..thisScript().version)
        		imgui.CenterTextColoredRGB('{868686}����� ������? ������ ����:')
	            imgui.SetCursorPosX((imgui.GetWindowWidth() - 190) / 2)
	            imgui.PushStyleVar(imgui.StyleVar.FrameRounding, 5)
	            imgui.PushStyleColor(imgui.Col.Button, imgui.ImVec4(0, 0.45, 0.8, 0.8))
	            imgui.PushStyleColor(imgui.Col.ButtonHovered, imgui.ImVec4(0, 0.45, 0.8, 0.9))
	            imgui.PushStyleColor(imgui.Col.ButtonActive, imgui.ImVec4(0, 0.45, 0.8, 1))
	            imgui.PushStyleColor(imgui.Col.Text, imgui.ImVec4(1, 1, 1, 1))
	            if imgui.Button(fa.ICON_FA_LINK..u8(" ���������"), imgui.ImVec2(90, 25)) then
	                sampAddChatMessage(tag..'������ ����������� � ����� ������', mcx)
	                setClipboardText("https://vk.com/opasanya")
	            end
	            imgui.PopStyleColor(4)
	            imgui.PopStyleVar()
	            imgui.SameLine()
	            imgui.PushStyleVar(imgui.StyleVar.FrameRounding, 5)
	            imgui.PushStyleColor(imgui.Col.Button, imgui.ImVec4(0.41, 0.19, 0.63, 0.8))
	            imgui.PushStyleColor(imgui.Col.ButtonHovered, imgui.ImVec4(0.41, 0.19, 0.63, 0.9))
	            imgui.PushStyleColor(imgui.Col.ButtonActive, imgui.ImVec4(0.41, 0.19, 0.63, 1))
	            if imgui.Button(fa.ICON_FA_LINK..u8(" Discord"), imgui.ImVec2(90, 25)) then
	                sampAddChatMessage(tag..'������ ����������� � ����� ������', mcx)
	                setClipboardText("cosmo#1000")
	            end
	            imgui.PopStyleColor(3)
	            imgui.PopStyleVar()
	            imgui.SetCursorPosX((imgui.GetWindowWidth() - 190) / 2)
	            if imgui.Checkbox(u8'����-�������� ����������', loginupdate) then
	            	configuration.main_settings.loginupdate = loginupdate.v 
	            	if inicfg.save(configuration, 'Bank_Config.ini') then 
        				sampAddChatMessage(tag..'����-�������� ���������� ��� ����� � ���� - '..mc..(configuration.main_settings.loginupdate and '��������' or '���������'), mcx)
        			end
	            end
	            if imgui.Button(u8'��� ������?', imgui.ImVec2(-1, 20)) then
	            	infoupdate.v = true
	            end
	            imgui.NewLine()
        	end
        	imgui.Separator()

        	imgui.SetCursorPosX((imgui.GetWindowWidth() - 170) / 2)
        	if imgui.Button(u8('��������� ���������� ')..fa.ICON_FA_DOWNLOAD, imgui.ImVec2(170, 30)) then
        		autoupdate("https://gitlab.com/snippets/2002160/raw", '['..string.upper(thisScript().name)..']: ', "https://gitlab.com/snippets/2002160/raw?inline=false")
        		checkdata()
        	end
        end

        if type_window.v == 6 then -- ����� /members
    		window6()
        end

        imgui.EndChild()
        imgui.SetCursorPosY(140)

        if imgui.Button(u8('������ ')..fa.ICON_FA_COMMENTS, imgui.ImVec2(180, 28)) then
			type_window.v = 1
		end
		if imgui.Button(u8('������ ')..fa.ICON_FA_UNIVERSITY, imgui.ImVec2(180, 28)) then
			type_window.v = 2
		end
		if imgui.Button(u8('������� ')..fa.ICON_FA_INFO_CIRCLE, imgui.ImVec2(180, 28)) then
			type_window.v = 3
		end
		if configuration.main_settings.rank > 4 then
			if imgui.Button(u8('��. ������ ')..fa.ICON_FA_USERS, imgui.ImVec2(180, 28)) then
				type_window.v = 4
			end
		end

		elsebtn()
	        if imgui.Button(u8'�������', imgui.ImVec2(86, 20)) then
				bank.v = false
			end
			imgui.SameLine()
			if imgui.Button(u8'���������', imgui.ImVec2(86, 20)) then
				type_window.v = 5
			end
		endbtn()

        imgui.End()
	end

	if ustav_window.v then
		local xx, yy = getScreenResolution()
		imgui.SetNextWindowSize(imgui.ImVec2(xx / 1.5, yy / 1.5), imgui.Cond.FirstUseEver)
		imgui.SetNextWindowPos(imgui.ImVec2(xx / 2, yy / 2), imgui.Cond.FirstUseEver, imgui.ImVec2(0.5, 0.5))
        imgui.Begin(u8'����� ������������ �����', ustav_window, imgui.WindowFlags.NoCollapse)
			
        	imgui.PushItemWidth(200)
			imgui.PushAllowKeyboardFocus(false)
			imgui.InputText("##search_ustav", search_ustav, imgui.InputTextFlags.EnterReturnsTrue)
			imgui.PopAllowKeyboardFocus()
			imgui.PopItemWidth()
			if not imgui.IsItemActive() and #search_ustav.v == 0 then
				imgui.SameLine(15)
				imgui.PushStyleColor(imgui.Col.Text, imgui.ImVec4(0.5, 0.5, 0.5, 1))
				imgui.Text(u8'����� �� ������')
				imgui.PopStyleColor()
			end

			imgui.SameLine(imgui.GetWindowWidth() - 100)

			if imgui.Button(u8("���������"), imgui.ImVec2(80, 20)) then
				configuration.main_settings.encode_ustav = not configuration.main_settings.encode_ustav
			end
			imgui.HintHovered(u8'�������, ���� ������ ���� "?"')
			imgui.NewLine()
			local ustav = io.open("moonloader/BHelper/�����_��.txt", "r+")
			for line in ustav:lines() do
				if #search_ustav.v < 1 then
					if configuration.main_settings.encode_ustav then
						imgui.TextWrapped(u8(line))
					else
						imgui.TextWrapped(line)
					end
				elseif configuration.main_settings.encode_ustav then
					if string.rlower(line):find(string.rlower(u8:decode(search_ustav.v))) then
						imgui.TextWrapped(u8(line))
					end
				elseif string.rlower(line):find(string.rlower(search_ustav.v)) then
					imgui.TextWrapped(line)
				end
			end
			ustav:close()

		imgui.End()
	end
	Premium_Window()
	Window_Info_Update()
	GetKassaInfo()
end

function GetKassaInfo()
	if kassa_stat.v then
		imgui.SetNextWindowPos(imgui.ImVec2(configuration.main_settings.KipX, configuration.main_settings.KipY), imgui.ImVec2(0.5, 0.5))
		imgui.SetNextWindowSize(imgui.ImVec2(200, 100), imgui.Cond.FirstUseEver)
	    imgui.Begin(u8("����������"), _, imgui.WindowFlags.NoCollapse + imgui.WindowFlags.NoResize + imgui.WindowFlags.NoMove + imgui.WindowFlags.NoBringToFrontOnFocus + imgui.WindowFlags.NoSavedSettings + imgui.WindowFlags.AlwaysAutoResize + imgui.WindowFlags.NoTitleBar)
	    	local myX, myY, myZ = getCharCoordinates(playerPed)
	    	local distBetweenKassa = getDistanceBetweenCoords3d(kassaX, kassaY, kassaZ, myX, myY, myZ)
	    	if distBetweenKassa < 10 then
		    	imgui.CenterTextColoredRGB(sc..'����: '..kassa_name.v:gsub('\n', ''))
		    	imgui.Separator()
		    	imgui.Text(fa.ICON_FA_CLOCK); imgui.SameLine(35); imgui.TextColoredRGB(mc..'����� �� �����: {SSSSSS}'..kassa_time.v..' ���.')
		    	imgui.Text(fa.ICON_FA_CHECK_CIRCLE); imgui.SameLine(35); imgui.TextColoredRGB(mc..'��������� ���������: {SSSSSS}'..tostring(configuration.timer.kassa_dep))
		    	imgui.Text(fa.ICON_FA_HANDSHAKE); imgui.SameLine(35); imgui.TextColoredRGB(mc..'��������� ��������: {SSSSSS}'..tostring(configuration.timer.kassa_credit))
		    	imgui.Text(fa.ICON_FA_CREDIT_CARD); imgui.SameLine(35); imgui.TextColoredRGB(mc..'������ ����: {SSSSSS}'..tostring(configuration.timer.kassa_card))
		    	imgui.Text(fa.ICON_FA_RECYCLE); imgui.SameLine(35); imgui.TextColoredRGB(mc..'������������� ����: {SSSSSS}'..tostring(configuration.timer.kassa_recard))
		    	imgui.Separator()
		    	imgui.CenterTextColoredRGB(mc..'����������: {SSSSSS}'..(150 * kassa_time.v)..'$')
		    	imgui.Separator()
		    else
		    	imgui.CenterTextColoredRGB(sc..'����: '..kassa_name.v:gsub('\n', ''))
		    	imgui.Separator()
		    	imgui.NewLine()
		    	imgui.CenterTextColoredRGB(mc..'�� ������ ������!\n��������� ����� � �����!\n{SSSSSS}'..math.floor(distBetweenKassa)..'�. �� �����')
		    	imgui.NewLine()
		    end
	    imgui.End()
	end
end

function Premium_Window()
	if premium.v then 
		local xx, yy = getScreenResolution()
		imgui.SetNextWindowSize(imgui.ImVec2(550, 330), imgui.Cond.FirstUseEver)
		imgui.SetNextWindowPos(imgui.ImVec2(xx / 2, yy / 2), imgui.Cond.FirstUseEver, imgui.ImVec2(0.5, 0.5))
	    imgui.Begin(u8'##PremiumMenu', _, imgui.WindowFlags.NoCollapse + imgui.WindowFlags.NoResize + imgui.WindowFlags.NoTitleBar + imgui.WindowFlags.NoScrollbar)
		imgui.CenterTextColoredRGB(mc..'���� ������ ������ �����������'); imgui.NewLine()
		if st_members.v then
			imgui.SetCursorPos(imgui.ImVec2((imgui.GetWindowWidth() - 80) / 2, (imgui.GetWindowHeight() - 80) / 2))
			imgui.Spinner(u8"##members_checking", 40, 5, imgui.GetColorU32(imgui.GetStyle().Colors[imgui.Col.ButtonHovered]))
		else
		    imgui.BeginChild('##SelectRank', imgui.ImVec2(170, -1), true)
		    for i = 1, 9 do
		    	if imgui.RadioButton(u8(i..' ����'), prem_rank[i].v) and getmembers[i] > 0 then 
		    		prem_rank[i].v = not prem_rank[i].v
		    	end
		    	imgui.SameLine(100)
		    	if getmembers[i] > 0 then
		    		imgui.TextColoredRGB('{007000}� ����: '..getmembers[i])
		    		imgui.HintHovered(u8('~ '..sumFormat(premsum.v / getmembers[i])..'$ �� ����������'))
		    	else
		    		imgui.TextColoredRGB('{AAAAAA}� ����: 0')
		    		imgui.HintHovered(u8'��� ����������� � �����')
		    	end
		    end
		    if imgui.Button(u8("���� �����##Premium"), imgui.ImVec2(-1, 25)) then
				rele_rank = not rele_rank
				for i = 1, 9 do 
					if rele_rank and getmembers[i] > 0 then 
						prem_rank[i].v = true
					else 
						prem_rank[i].v = false
					end 
				end
			end
			imgui.Separator()
			imgui.SetCursorPosY(250)
			imgui.RadioButton(u8'##FromMy', prem_bank, 1); imgui.SameLine(65)
			if prem_bank.v == 1 then imgui.CenterTextColoredRGB('<-   C ���   ->') else imgui.CenterTextColoredRGB('<-   C �����   ->') end; imgui.SameLine(143)
		 	imgui.RadioButton(u8'##FromBank', prem_bank, 2)
		    imgui.EndChild()
		    imgui.SameLine()
		    imgui.BeginChild('##SettingsPremium', imgui.ImVec2(-1, -1), true)
		    imgui.CenterTextColoredRGB(mc..'����������� �����������,\n'..mc..'������� ������� ������: '..sc..getCountSelectMembers())
		    imgui.NewLine()
		   	imgui.PushStyleVar(imgui.StyleVar.ItemSpacing, imgui.ImVec2(3, 3))
		    imgui.PushItemWidth(-1)
		    if imgui.DragInt(u8'##premsumDrag', premsum, 1000, 100000, 20000000, u8'������ �� ����: %.0f$') then
		    	if premsum.v < 100000 then premsum.v = 100000 end
		    	if premsum.v > 20000000 then premsum.v = 20000000 end
		    end
		    imgui.PopItemWidth()
		   	imgui.PopStyleVar()
		    imgui.NewLine()
		    imgui.TextColoredRGB(sc..'����� ����� ���������:')
		    imgui.PushFont(fontsize20)
		    if getCountSelectMembers() > 0 then
			    imgui.TextColoredRGB(sumFormat(getMultiplication() * premsum.v)..'$')
			else
				imgui.TextColoredRGB('{565656}������� �����')
			end
			imgui.PopFont()
			imgui.NewLine(); imgui.NewLine()
			if getCountSelectMembers() > 0 then
				if tonumber(getPlayerMoney(playerHandle) - (getMultiplication() * premsum.v)) > 0 and tonumber(bank_cazna.v - (getMultiplication() * premsum.v)) > 0 then
					elsebtn()
					if imgui.Button(u8("������ ������##Premium"), imgui.ImVec2(-1, 40)) then
						for i = 1, 9 do 
							if prem_rank[i].v then
								sampSendChat('/premium '..tostring(prem_bank.v)..' '..tostring(premsum.v)..' '..tostring(i))
							end
						end
						lua_thread.create(function()
							if configuration.main_settings.autoF8 then
								wait(1000)
								sampSendChat('/time')
								wait(500)
								setVirtualKeyDown(VK_F8, true)
								wait(0)
								setVirtualKeyDown(VK_F8, false)
							end
						end)
						premium.v = false
					end
					endbtn()
				else
					imgui.PushDisableButton()
					imgui.Button(u8("������ ������##Premium"), imgui.ImVec2(-1, 40))
					imgui.PopDisableButton()
				end
			else
				imgui.PushDisableButton()
				imgui.Button(u8("������ ������##Premium"), imgui.ImVec2(-1, 40))
				imgui.PopDisableButton()
			end
		    if imgui.Button(u8("�������##Premium"), imgui.ImVec2(-1, -1)) then
				premium.v = false
			end
			imgui.SetCursorPos(imgui.ImVec2(10, 155))
			if getCountSelectMembers() > 0 then
				if prem_bank.v == 1 then
					local my_money = sumFormat(getPlayerMoney(playerHandle) - (getMultiplication() * premsum.v))
					if tonumber(getPlayerMoney(playerHandle) - (getMultiplication() * premsum.v)) < 0 then 
						imgui.TextColoredRGB('{FF0000}'..my_money..'$')
					else
						imgui.TextColoredRGB('{007000}'..my_money..'$')
					end
					imgui.HintHovered(u8'�����, ������� ��������� � ��� �� �����')
				else
					if tonumber(bank_cazna.v - (getMultiplication() * premsum.v)) < 0 then 
						imgui.TextColoredRGB('{FF0000}'..sumFormat(bank_cazna.v)..'$')
					else
						imgui.TextColoredRGB('{007000}'..sumFormat(bank_cazna.v)..'$')
					end
					imgui.HintHovered(u8'�����, ������� ��������� � ����� �����')
				end
			else
				imgui.TextColoredRGB('{565656}��� ������ ����� ������� ��������?)')
			end
			imgui.EndChild()
		end
		imgui.End()
	end
end

function sumFormat(a)
    local b, e = ('%d'):format(a):gsub('^%-', '')
    local c = b:reverse():gsub('%d%d%d', '%1,')
    local d = c:reverse():gsub('^,', '')
    return (e == 1 and '-' or '')..d
end

function getCountSelectMembers()
	local count = 0
	for i = 1, 9 do
		if prem_rank[i].v then 
			count = count + getmembers[i]
		end
	end
	return count
end

function getMultiplication()
	local count = 0
	for i = 1, 9 do
		if prem_rank[i].v then 
			count = count + 1
		end
	end
	return count
end

function window6()
	if imgui.Checkbox(u8'�������� / ���������', members_state) then 
		configuration.main_settings.members_state = members_state.v
		inicfg.save(configuration, 'Bank_Config.ini')
		if members_state.v then
			getMB = true
	        sampSendChat('/members')
	        membersList = {}
		end
	end
	imgui.SameLine(); imgui.TextDisabled('(?)'); imgui.HintHovered(u8'����� �������� �����������, �������� ������, ����� �� ������������')
	imgui.TextDisabled(u8'������� �� ������ �������������������������������������������������')
	if imgui.DragInt(u8'������� �� X', members_posX, 1, 0, ex) then
    	configuration.main_settings.members_posX = members_posX.v
		inicfg.save(configuration, 'Bank_Config.ini')
    end
    imgui.SameLine(); imgui.TextDisabled('(?)'); imgui.HintHovered(u8'������ � �������')
    if imgui.DragInt(u8'������� �� Y', members_posY, 1, 0, ey) then
    	configuration.main_settings.members_posY = members_posY.v
		inicfg.save(configuration, 'Bank_Config.ini')
    end
    imgui.SameLine(); imgui.TextDisabled('(?)'); imgui.HintHovered(u8'������ � �������')
    imgui.TextDisabled(u8'��������� ������ �������������������������������������������������')
    imgui.PushItemWidth(150)
    if imgui.InputInt(u8'����� ���', members_afk) then
    	if members_afk.v < 100 then members_afk.v = 100 end
    	if members_afk.v > 3600 then members_afk.v = 3600 end
    	configuration.main_settings.members_afk = members_afk.v
		inicfg.save(configuration, 'Bank_Config.ini')
    end
    imgui.SameLine(); imgui.TextDisabled('(?)'); imgui.HintHovered(u8'����� ���, ���� ��������� � ��������, �� ����� ������ ������� ������ � ������')
    if imgui.InputInt(u8'������� ���������� (���.)', members_delay) then
    	if members_delay.v < 5 then members_delay.v = 5 end
    	if members_delay.v > 60 then members_delay.v = 60 end
    	configuration.main_settings.members_delay = members_delay.v
		inicfg.save(configuration, 'Bank_Config.ini')
    end
    imgui.SameLine(); imgui.TextDisabled('(?)'); imgui.HintHovered(u8'�������, � ������� ����� ����������� ���������� � ������. ������������� ��������: 10-15 ���')
    imgui.TextDisabled(u8'��������� ������ �������������������������������������������������')
    if imgui.InputText(u8'�����', members_fontName) then 
    	configuration.main_settings.members_fontName = members_fontName.v
    	inicfg.save(configuration, 'Bank_Config.ini')
    	fontChecker = renderCreateFont(configuration.main_settings.members_fontName, configuration.main_settings.members_fontSize, configuration.main_settings.members_fontFlag)
    end
    imgui.SameLine(); imgui.TextDisabled('(?)'); imgui.HintHovered(u8'�������� ������� ����� ������ � ���������\n\nArial\nBook Antiqua\nCourier New\nGeorgia\nTahoma\nTimes New Roman\nTrebuchet MS\nVerdana\n� ������...')
    if imgui.InputInt(u8'������', members_fontSize) then
    	if members_fontSize.v < 5 then members_fontSize.v = 5 end
    	if members_fontSize.v > 20 then members_fontSize.v = 20 end
    	configuration.main_settings.members_fontSize = members_fontSize.v
		inicfg.save(configuration, 'Bank_Config.ini')
		fontChecker = renderCreateFont(configuration.main_settings.members_fontName, configuration.main_settings.members_fontSize, configuration.main_settings.members_fontFlag)
    end
    imgui.SameLine(); imgui.TextDisabled('(?)'); imgui.HintHovered(u8'������ ������')
    if imgui.InputInt(u8'���', members_fontFlag) then
	    configuration.main_settings.members_fontFlag = members_fontFlag.v
		inicfg.save(configuration, 'Bank_Config.ini')
		fontChecker = renderCreateFont(configuration.main_settings.members_fontName, configuration.main_settings.members_fontSize, configuration.main_settings.members_fontFlag)
    end
    imgui.SameLine(); imgui.TextDisabled('(?)'); imgui.HintHovered(u8'���� ������, ���������� �������� �� 1 �� 15')
    if imgui.InputInt(u8'������', members_fontOffset) then
    	if members_fontOffset.v < 5 then members_fontOffset.v = 5 end
    	if members_fontOffset.v > 20 then members_fontOffset.v = 20 end
		configuration.main_settings.members_fontOffset = members_fontOffset.v
		inicfg.save(configuration, 'Bank_Config.ini')
    end
    imgui.SameLine(); imgui.TextDisabled('(?)'); imgui.HintHovered(u8'���������� ����� ��������')
    imgui.PopItemWidth()
end

function go_expel()
	if sampGetPlayerColor(expelId) ~= 2150206647 then
		if not sampIsPlayerPaused(expelId) then
			lua_thread.create(function()
		        sampSendChat("��� ������� ������� ��� �� ������ �����.")
		        wait(2300)
		        sampSendChat("/me {sex:����|�����} �� ���� "..sampGetPlayerNickname(expelId).." � {sex:����|������} � ������")
		        wait(2300)
		        sampSendChat("/todo ��������� ��� ����� ����������*�������� ����� �����..")
		        sampSendChat("/expel "..expelId.." �.�.�.")
		        if configuration.main_settings.autoF8 then
					wait(1000)
					sampSendChat('/time')
					wait(500)
					setVirtualKeyDown(VK_F8, true)
					wait(0)
					setVirtualKeyDown(VK_F8, false)
				end
		    end)
		else
			sampAddChatMessage(tag..'����� � ���!', mcx)
		end
	else
		sampAddChatMessage(tag..'�� ��������� ������� ���������� �����..', mcx)
	end
end

function onWindowMessage(msg, wparam, lparam)
    if wparam == vkeys.VK_ESCAPE then 
    	if int_bank.v then
        	consumeWindowMessage(true, true)
        	int_bank.v = false
        end
        --if bank.v and not binder_open then
        	--consumeWindowMessage(true, true)
        	--bank.v = false
        --end
        if ustav_window.v then
        	consumeWindowMessage(true, true)
        end
    end
end

function imgui.HintHovered(text)
    if imgui.IsItemHovered() then
        imgui.BeginTooltip()
        imgui.PushTextWrapPos(450)
        imgui.TextUnformatted(text)
        imgui.PopTextWrapPos()
        imgui.EndTooltip()
    end
end

function onScriptTerminate(script, quitGame)
    if script == thisScript() then
        if not sampIsDialogActive() then
            showCursor(false, false)
        end
        if inicfg.save(configuration, 'Bank_Config.ini') then 
        	sampfuncsLog(mc..'Bank-Helper:'..wc..' ��� ��������� ���������!')
        end
        if not no_error_msg then
        	sampShowDialog(1313, "{006AC2}[ Bank ] ������ �������� ���� ������", [[
{006AC2}                                                           ��� ������ ���� �� �������� ������?{FFC973}

1. ������ ������ ���� ���������� ������ �� ����������.
 - ���� ���� � ��� ����� ���� ����������� �����-�� ����� �� ������ �� ����������� ��������.

2. ���� �� ���������� � ��������� ������ �������, �� ������ ����� �� ������ ������
 ������ ��� ����������. �� ��� ��� ��������� ������ CTRL + R, � 90 ��������� �������
 ��� ��������

3. ���� � ��� ���������� MVD Helper, �� ��� ����� ��� �������, �� ���������� ������
 ������ MonnLoader 0.25, � ������ ������ �������� �� ��������� ���������� ������ 0.26
 ������� MVD Helper � ��������� ���������� Bank Helper

4. �������� � ��� ����������� ������������� ���������
 - ����-��������
 - ����-������
 - ����� Samp Addon
 - ������ LUA/CLEO/SF/ASI

5. ��� ������ ������� ����� ��������� �����:
 - SAMPFUNCS
 - CLEO 4.1+
 - MoonLoader 0.26
 - ���������� (lib)

6. ���� ������ ������ ������� ����� � ����� ��������� ��� ������, �� ���������� ������� ���������
- � ����� moonloader > ������� ����� CONFIG
- � ����� moonloader > ������� ����� BHelper

7. ���� ������ �������� �� �������, �� ���������� ���������� ������ �� ������ ������

{00FFA5}��� ����� ��� ������� ����� ����� �� ������ {006D86}https://yadi.sk/d/hLZTKsBhFcgzLA

{FFFFFF}���������� ������������ ������ ������� ����� ����������  ������ CTRL + R]], "�����", _, 0)
	    end
    end
end

function sampGetPlayerSkin(id)
    if not id or not sampIsPlayerConnected(tonumber(id)) and not tonumber(id) == select(2, sampGetPlayerIdByCharHandle(playerPed)) then 
        return false 
    end
    local isLocalPlayer = tonumber(id) == select(2, sampGetPlayerIdByCharHandle(playerPed))
    local result, handle = sampGetCharHandleBySampPlayerId(tonumber(id))
    if not result and not isLocalPlayer then return false end
    local skinid = getCharModel(isLocalPlayer and playerPed or handle)
    if skinid < 0 or skinid > 311 then return false end
    return true, skinid
end

function howdress(id)
	if id == 63 or id == 64 or id == 75 or id == 77 or id == 78 or id == 79 or id == 87 or id == 134 or id == 135 or id == 136 or id == 152 or id == 178 or id == 200 or id == 212 or id == 230 or id == 237 or id == 239 or id == 244 or id == 246 or id == 252 or id == 256 or id == 257 then
		return true
	else
		return false
	end
end

function check_forma()
	local _, selfid = sampGetPlayerIdByCharHandle(playerPed) 
	if sampGetPlayerColor(selfid) == 2150206647 then 
		return true
	else
		return false
	end
end

function system_credit()
 	if imgui.BeginPopupModal(u8("������� ������������"), _, imgui.WindowFlags.NoCollapse + imgui.WindowFlags.NoScrollbar + imgui.WindowFlags.AlwaysAutoResize) then
		imgui.BeginChild("##creditlist", imgui.ImVec2(400, 500), false, imgui.WindowFlags.NoScrollbar)

		local credits = io.open("moonloader/BHelper/������������.txt", "r+")
		for line in credits:lines() do
			imgui.CenterTextColoredRGB(line)
		end
		credits:close()
		imgui.Separator()
		imgui.CenterTextColoredRGB(mc..'�� ������ �������� �� ����! {464646}> ������ ��� <')
		imgui.HintHovered(u8(getWorkingDirectory()..'\\BHelper\\������������.txt'))
		imgui.SetCursorPosX((imgui.GetWindowWidth() - 80) / 2)
		if imgui.Button(u8("�������##������������"), imgui.ImVec2(80, 20)) then
			imgui.CloseCurrentPopup()
		end

		imgui.EndChild()
	imgui.EndPopup()
	end
end 

function system_cadr()
 	if imgui.BeginPopupModal(u8("�������� �������"), _, imgui.WindowFlags.NoCollapse + imgui.WindowFlags.NoScrollbar + imgui.WindowFlags.AlwaysAutoResize) then

		local cadrsys = io.open("moonloader/BHelper/�������� �������.txt", "r+")
		for line in cadrsys:lines() do
			imgui.TextColoredRGB(line)
		end
		cadrsys:close()
		imgui.Separator()
		imgui.CenterTextColoredRGB(mc..'�� ������ �������� �� ����! {464646}> ������ ��� <')
		imgui.HintHovered(u8(getWorkingDirectory()..'\\BHelper\\�������� �������.txt'))
		imgui.SetCursorPosX((imgui.GetWindowWidth() - 80) / 2)
		if imgui.Button(u8("�������##��������"), imgui.ImVec2(80, 20)) then
			imgui.CloseCurrentPopup()
		end

	imgui.EndPopup()
	end
end 

function fTimeF9()
	if imgui.Checkbox(u8('/time �� F9'), timeF9) then 
		configuration.main_settings.timeF9 = timeF9.v
		inicfg.save(configuration, 'Bank_Config.ini')
	end
end

function system_uprank()
	if imgui.BeginPopupModal(u8("������ ������� ���������"), _, imgui.WindowFlags.NoCollapse + imgui.WindowFlags.NoScrollbar + imgui.WindowFlags.AlwaysAutoResize) then
		imgui.BeginChild("##SysUpRList", imgui.ImVec2(700, 330), false)

		imgui.CenterTextColoredRGB([[
{0088C2}������� ��������� ��� 1 - 7 ������
C ��� 2020 ���� - ��������. ����� �������� �� ��� ����������, �������� ���� ������
�
{0088C2}�������� [7] -> ���. ��������� [8]
����������� �� ������ ��������� 120 ����� {868686}(5 ����)
1. ��������� �� ������ 2 ���� {868686}(������ ������ 10 ����� � /time)
2. �������� 30 ������ ������� {868686}(�� ����� ��������� 5 ������; ����������� /timestamp)
3. ������ �� 15-� ��������������
4. �������� 5 ����������� ��� �������
5. ��������� 5 ������� {868686}(�� �������; /me /do /todo ������� 10 �� ���������)
�
{0088C2}���.��������� [8] -> �������� ����� [9]
����������� �� ������ ��������� 144 ���� {868686}(6 ����)
��������� �����
������������ � ������ �� ���� ��������� ������������ �����
����������: ����� �� 9-� ���� �������� ������ �� 8-�, �� � ����������� ������ ��������������
�]])
		if imgui.Button(u8("�������"), imgui.ImVec2(-1, 20)) then
			imgui.CloseCurrentPopup()
		end
		imgui.EndChild()
	imgui.EndPopup()
	end
end

function post()
	if imgui.BeginPopupModal(u8("������������ ������"), _, imgui.WindowFlags.NoCollapse + imgui.WindowFlags.NoScrollbar + imgui.WindowFlags.AlwaysAutoResize) then
			imgui.BeginChild("##PostsList", imgui.ImVec2(300, 260), false)

			if imgui.Button(u8("����� - 2"), imgui.ImVec2(-1, 30)) then
				if getCharActiveInterior(playerPed) == 22 then
					setMarker(1, -2693.7869, 797.9387, 1500, 1, 0xFFFF0000)
					sampAddChatMessage(tag..'������������� �� ������������� ������', mcx)
					imgui.CloseCurrentPopup()
					bank.v = false
				else
					sampAddChatMessage(tag..'�� ������ ��������� � ��������� �����!', mcx)
				end
			end
			if imgui.Button(u8("���� - 2"), imgui.ImVec2(-1, 30)) then
				if getCharActiveInterior(playerPed) == 22 then
					setMarker(1, -2693.7881, 794.1193, 1500, 1, 0xFFFF0000)
					sampAddChatMessage(tag..'������������� �� ������������� ������', mcx)
					imgui.CloseCurrentPopup()
					bank.v = false
				else
					sampAddChatMessage(tag..'�� ������ ��������� � ��������� �����!', mcx)
				end
			end
			if imgui.Button(u8("� - 4"), imgui.ImVec2(-1, 30)) then
				if getCharActiveInterior(playerPed) == 22 then
					setMarker(1, -2691.4854, 806.9976, 1500, 1, 0xFFFF0000)
					sampAddChatMessage(tag..'������������� �� ������������� ������', mcx)
					imgui.CloseCurrentPopup()
					bank.v = false
				else
					sampAddChatMessage(tag..'�� ������ ��������� � ��������� �����!', mcx)
				end
			end
			if imgui.Button(u8("���� - 4"), imgui.ImVec2(-1, 30)) then
				if getCharActiveInterior(playerPed) == 22 then
					setMarker(1, -2687.8120, 806.7342, 1500, 1, 0xFFFF0000)
					sampAddChatMessage(tag..'������������� �� ������������� ������', mcx)
					imgui.CloseCurrentPopup()
					bank.v = false
				else
					sampAddChatMessage(tag..'�� ������ ��������� � ��������� �����!', mcx)
				end
			end
			if imgui.Button(u8("��� - 5"), imgui.ImVec2(-1, 30)) then
				if getCharActiveInterior(playerPed) == 22 then
					setMarker(1, -2667.4160, 789.8926, 1500, 1, 0xFFFF0000)
					sampAddChatMessage(tag..'������������� �� ������������� ������', mcx)
					imgui.CloseCurrentPopup()
					bank.v = false
				else
					sampAddChatMessage(tag..'�� ������ ��������� � ��������� �����!', mcx)
				end
			end
			if imgui.Button(u8("�����"), imgui.ImVec2(-1, 30)) then
				if getCharActiveInterior(playerPed) == 22 then
					setMarker(1, -2694.0479, 809.4982, 1500, 1, 0xFFFF0000)
					sampAddChatMessage(tag..'������������� �� ������������� ������', mcx)
					imgui.CloseCurrentPopup()
					bank.v = false
				else
					sampAddChatMessage(tag..'�� ������ ��������� � ��������� �����!', mcx)
				end
			end
			elsebtn()
			if imgui.Button(u8("����� �������"), imgui.ImVec2(-1, 30)) then
				sampSetChatInputEnabled(true)
				sampSetChatInputText("/r �����������: {my_name} | ����:  | ���������: ����������")
				sampSetChatInputCursor(34, 34)
				sampAddChatMessage(tag..'����� ����������� � ����� ������, � ��� �� �������� � ���� ����', mcx)
				sampAddChatMessage(tag..'��� '..mc..'{my_name}'..wc..' ������������� ���������� �� ��� ��� ��� ��������', mcx)
				sampAddChatMessage(tag..'��� �������� ������������� ������ ������ ������ � ��� !���� (�� ���������)', mcx)
				setClipboardText(u8:decode("/r ����������� {my_name} | ����: [��� ����] | ���������: ����������"))
				imgui.CloseCurrentPopup()
				bank.v = false
			end
			imgui.SetCursorPosX((imgui.GetWindowWidth() - 80) / 2)
			if imgui.Button(u8("�������##Docs"), imgui.ImVec2(80, 20)) then
				imgui.CloseCurrentPopup()
			end
			endbtn()

			imgui.EndChild()
		imgui.EndPopup()
	end
end

function gov()
	if imgui.BeginPopupModal(u8("������������ ��������������� �����"), _, imgui.WindowFlags.NoCollapse + imgui.WindowFlags.AlwaysAutoResize) then

		if hGov.v > 23 then hGov.v = 0 end
		if mGov.v > 59 then mGov.v = 0 end
		if hGov.v < 0 then hGov.v = 23 end
		if mGov.v < 0 then mGov.v = 55 end

		imgui.PushStyleVar(imgui.StyleVar.ItemSpacing, imgui.ImVec2(1, 1))
		imgui.SetCursorPosX(30)
		if imgui.Button('+##PlusHour', imgui.ImVec2(40, 20)) then
			status_button_gov = false
			hGov.v = hGov.v + 1
		end
		imgui.SameLine(93)
		if imgui.Button('+##PlusMin', imgui.ImVec2(40, 20)) then
			status_button_gov = false 
			mGov.v = mGov.v + 5
		end

		imgui.PushFont(fontsize20)
		if #tostring(hGov.v) == 1 then zeroh = '0' else zeroh = '' end
		if #tostring(mGov.v) == 1 then zerom = '0' else zerom = '' end
		imgui.SetCursorPosX(30)
		imgui.TextColoredRGB(tostring(zeroh..hGov.v)..' : '..tostring(zerom..mGov.v))
		imgui.PopFont()

		imgui.SetCursorPosX(30)
		if imgui.Button('-##MinusHour', imgui.ImVec2(40, 20)) then
			status_button_gov = false
			hGov.v = hGov.v - 1
		end
		imgui.SameLine(93)
		if imgui.Button('-##MinusMin', imgui.ImVec2(40, 20)) then
			status_button_gov = false
			mGov.v = mGov.v - 5
		end
		imgui.PopStyleVar()

		imgui.CenterTextColoredRGB(mc..'����� ��������������� �����:')
		imgui.PushItemWidth(600)
		imgui.InputText('##govstr1', govstr1)
		imgui.InputText('##govstr2', govstr2)
		imgui.InputText('##govstr3', govstr3)
		imgui.PopItemWidth()
		imgui.SetCursorPosX((imgui.GetWindowWidth() - 160) / 2)
		if imgui.Button(u8'���������', imgui.ImVec2(160, 20)) then 
			imgui.CloseCurrentPopup()
		end

		imgui.SetCursorPos(imgui.ImVec2(150, 30))
		imgui.BeginChild("##SettingsGov", imgui.ImVec2(-1, 75), true)
		imgui.ToggleButton('##gosScreen', gosScreen); imgui.SameLine()
		imgui.TextColoredRGB('����-����� ����� ������')
		imgui.ToggleButton('##gosDep', gosDep); imgui.SameLine()
		imgui.TextColoredRGB('�������� ����� � /d')
		imgui.PushItemWidth(100)
		if imgui.InputInt('##delayGov', delayGov) then 
			if delayGov.v < 0 then delayGov.v = 0 end
			if delayGov.v > 10000 then delayGov.v = 10000 end
		end
		imgui.PopItemWidth()
		imgui.SameLine()
		imgui.TextColoredRGB('�������� {565656}(?)')
		imgui.HintHovered(u8('�������� ����� ����������� � GOV-�����\n����������� � ������������� (1000 �� = 1 ���)'))
		imgui.SetCursorPos(imgui.ImVec2(300, 10))
		if not status_button_gov then 
			if tonumber(os.date("%H", os.time())) == hGov.v and tonumber(os.date("%M", os.time())) == mGov.v then 
				imgui.PushDisableButton()
				imgui.Button(u8'��������##gov', imgui.ImVec2(-1, -1))
				imgui.PopDisableButton()
			else
				greenbtn()
				if imgui.Button(u8'��������##gov', imgui.ImVec2(-1, -1)) then 
					status_button_gov = true
					antiflud = true
				end
				endbtn()
			end
		else
			redbtn()
			if imgui.Button(u8'�� ������: '..getOstTime(), imgui.ImVec2(-1, -1)) then 
				status_button_gov = false
			end
			endbtn()
		end
		imgui.EndChild()
        imgui.EndPopup()
    end
end

function getOstTime()
	local datetime = {
		year  = os.date("%Y", os.time()),
        month = os.date("%m", os.time()),
        day   = os.date("%d", os.time()),
        hour  = hGov.v,
        min   = mGov.v,
        sec   = 00
       	}
       	if os.time(datetime) - os.time() < 0 then
       		datetime.day = os.date("%d", os.time()) + 1
       		exit = tostring(get_timer(os.time(datetime) - os.time()))
       	else
       		exit = tostring(get_timer(os.time(datetime) - os.time()))
       	end
	return exit
end

function blgovpp()
	if imgui.BeginPopupModal(u8("׸���� ������ �������������"), _, imgui.WindowFlags.NoCollapse + imgui.WindowFlags.AlwaysAutoResize) then
		imgui.CenterTextColoredRGB('������� ��������� � ���� �������\n������ �� ���� ��� ������� � �����������\n������ ����� ������� ������ ����� �� ������')
		imgui.SetCursorPosX(80)
		imgui.TextColoredRGB('{FFD900}Ƹ���� ������� {868686}(?)')
		imgui.HintHovered(u8'Ƹ���� �������:\n����� � ������� ������ ��������������\n��� ������ ����� - 1.000.000$')
		imgui.SameLine(330)
		imgui.TextColoredRGB('{FF0000}������� ������� {868686}(?)')
		imgui.HintHovered(u8'������� �������:\n����� � ������� ������ ��������������\n�� ���������� �������� �������������.')
		imgui.PushAllowKeyboardFocus(false)
		imgui.InputTextMultiline("##ylw", ylw, imgui.ImVec2(250, 300))
		imgui.PopAllowKeyboardFocus()
		if not imgui.IsItemActive() and #ylw.v == 0 then
			imgui.SameLine(35)
			imgui.Text(u8'\n\n\n\n\n\n\n\n     ���������� ���� � �������\n���� �� ��� ������, ������� Esc\n (��� ������� ������ ���������)')
		end
		imgui.SameLine(270)
		imgui.PushAllowKeyboardFocus(false)
		imgui.InputTextMultiline("##red", red, imgui.ImVec2(250, 300))
		imgui.PopAllowKeyboardFocus()
		if not imgui.IsItemActive() and #red.v == 0 then
			imgui.SameLine(295)
			imgui.Text(u8'\n\n\n\n\n\n\n\n     ���������� ���� � �������\n���� �� ��� ������, ������� Esc\n (��� ������� ������ ���������)')
		end
		if imgui.Button(u8("��������� � �������##BlackList"), imgui.ImVec2(-1, 25)) then
			if not ylw.v:find('%d+') and not red.v:find('%d+') then
				blg.ylw.y = ylw.v:gsub('\n', '~')
    			blg.red.r = red.v:gsub('\n', '~')
    			if inicfg.save(blg, 'Bank_BlackList.ini') then 
    				sampAddChatMessage(tag..'������ ��������!', mcx) 
    			end
				imgui.CloseCurrentPopup()
			else
				sampAddChatMessage(tag..'������������ ������� (�����)', mcx)
			end
		end
        imgui.EndPopup()
    end
end

function binder()
	if imgui.BeginPopupModal(u8("��������������/�������� �����"), _, imgui.WindowFlags.NoCollapse + imgui.WindowFlags.NoScrollbar + imgui.WindowFlags.AlwaysAutoResize) then
        imgui.BeginChild("##EditBinder", imgui.ImVec2(500, 355), true)
            imgui.PushItemWidth(150)
            imgui.InputInt(u8("�������� ����� �������� � �������������"), binder_delay); imgui.SameLine(); imgui.TextDisabled('(?)')
            imgui.HintHovered(u8('�� ������ 60.000ms!\n(1 sec = 1000 ms)'))
            imgui.PopItemWidth()
            if binder_delay.v <= 0 then
                binder_delay.v = 1
            elseif binder_delay.v >= 60001 then
                binder_delay.v = 60000
            end

            imgui.TextWrapped(u8("�� ������ ������������ ��������� ���� ��� ����� ������!"))
            imgui.SameLine(); imgui.TextDisabled(u8'(���������)')
            if imgui.IsItemClicked() then
				imgui.OpenPopup(u8("��������� ����"))
	        end
	        taginfo()
            imgui.InputTextMultiline("##EditMultiline", text_binder, imgui.ImVec2(-1, 250))
            imgui.Text(u8'�������� ����� (�����������):'); imgui.SameLine()
            imgui.PushItemWidth(150)
            imgui.InputText("##binder_name", binder_name)
            imgui.PopItemWidth()
            if #binder_name.v > 0 and #text_binder.v > 0 then
                elsebtn()
	                imgui.SameLine()
	                if imgui.Button(u8("���������"), imgui.ImVec2(-1, 20)) then
		                if not EditOldBind then
		                    refresh_text = text_binder.v:gsub("\n", "~")
		                    table.insert(configuration.Binds_Name, binder_name.v)
		                    table.insert(configuration.Binds_Action, refresh_text)
		                    table.insert(configuration.Binds_Deleay, binder_delay.v)
		                    if inicfg.save(configuration, 'Bank_Config.ini') then
			                    sampAddChatMessage(tag..'���� '..mc..'�'..u8:decode(binder_name.v)..'�'..wc..' ������� ��������!', mcx)
			                    binder_name.v, text_binder.v = '', ''
		                    end
		                    binder_open = false
		                    imgui.CloseCurrentPopup()
	                    else
	                        refresh_text = text_binder.v:gsub("\n", "~")
	                        table.insert(configuration.Binds_Name, getpos, binder_name.v)
	                        table.insert(configuration.Binds_Action, getpos, refresh_text)
	                        table.insert(configuration.Binds_Deleay, getpos, binder_delay.v)
	                        table.remove(configuration.Binds_Name, getpos + 1)
	                        table.remove(configuration.Binds_Action, getpos + 1)
	                        table.remove(configuration.Binds_Deleay, getpos + 1)
	                        if inicfg.save(configuration, 'Bank_Config.ini') then
			                    sampAddChatMessage(tag..'���� '..mc..'�'..u8:decode(binder_name.v)..'�'..wc..' ������� ��������������!', mcx)
			                    binder_name.v, text_binder.v = '', ''
		                    end
	                        EditOldBind = false
	                        binder_open = false
	                        imgui.CloseCurrentPopup()
	                    end
	                end
                endbtn()
            else
            	imgui.PushDisableButton()
                imgui.SameLine()
                imgui.Button(u8("���������"), imgui.ImVec2(-1, 20))
                imgui.PopDisableButton()
                imgui.HintHovered(u8'��������� �� ��� ������!')
            end
            if imgui.Button(u8("�������"), imgui.ImVec2(-1, 0)) then
                if not EditOldBind then
                    if #text_binder.v == 0 then
                    	binder_open = false
	                    imgui.CloseCurrentPopup()
	                    binder_name.v, text_binder.v = '', ''
	                else
	                    imgui.OpenPopup(u8("�����������##AcceptCloseBinderEdit"))
	                end
                else
                    EditOldBind = false
                    binder_open = false
                    imgui.CloseCurrentPopup()
                    binder_name.v, text_binder.v = '', ''
                end
            end
            elsebtn()
            if imgui.BeginPopupModal(u8("�����������##AcceptCloseBinderEdit"), _, imgui.WindowFlags.NoCollapse + imgui.WindowFlags.NoScrollbar + imgui.WindowFlags.AlwaysAutoResize) then
                imgui.BeginChild("##AcceptCloseBinderEdit", imgui.ImVec2(300, 80), false)
                imgui.CenterTextColoredRGB('�� ��� ������ ���-�� ������ � �����\n��������� ��� ��������?')
                if imgui.Button(u8'���������##accept', imgui.ImVec2(145, 20)) then
                    imgui.CloseCurrentPopup()
                    savedatamlt = true
                end
                imgui.SameLine()
                if imgui.Button(u8'�� ���������##accept', imgui.ImVec2(145, 20)) then
                    imgui.CloseCurrentPopup()
                    nonsavedatamlt = true
                end
                if imgui.Button(u8'��������� �����##accept', imgui.ImVec2(-1, 20)) then
                    imgui.CloseCurrentPopup()
                end

                imgui.EndChild()
                imgui.EndPopup()
            end
            endbtn()
            if savedatamlt then 
                savedatamlt = false
                binder_open = false
                imgui.CloseCurrentPopup()
            elseif nonsavedatamlt then 
                nonsavedatamlt = false
                binder_open = false
                imgui.CloseCurrentPopup()
                binder_name.v, text_binder.v = '', ''
            end
        	imgui.EndChild()
        imgui.EndPopup()
    end
end

function style_dark()
    imgui.SwitchContext()
    local style = imgui.GetStyle()
    local style = imgui.GetStyle()
    local colors = style.Colors
    local clr = imgui.Col
    local ImVec4 = imgui.ImVec4
    local ImVec2 = imgui.ImVec2
    style.WindowRounding = 10.0
    style.FrameRounding = 5.0
    style.ChildWindowRounding = 5.0
    style.WindowTitleAlign = imgui.ImVec2(0.5, 0.5)

    colors[clr.Text]                    = ImVec4(1.00, 1.00, 1.00, 1.00)
    colors[clr.TextDisabled]            = ImVec4(0.30, 0.30, 0.30, 1.00)
    colors[clr.WindowBg]                = ImVec4(0.06, 0.06, 0.06, 0.97)
    colors[clr.ChildWindowBg]           = ImVec4(1.00, 1.00, 1.00, 0.00)
    colors[clr.PopupBg]                 = ImVec4(0.08, 0.08, 0.08, 0.94)
    colors[clr.ComboBg]                 = colors[clr.PopupBg]
    colors[clr.Border]                  = ImVec4(0.43, 0.43, 0.50, 0.30)
    colors[clr.BorderShadow]            = ImVec4(0.00, 0.00, 0.00, 0.00)
    colors[clr.FrameBg]                 = ImVec4(0.12, 0.12, 0.12, 0.94)
    colors[clr.FrameBgHovered]          = ImVec4(0.45, 0.45, 0.45, 0.85)
    colors[clr.FrameBgActive]           = ImVec4(0.63, 0.63, 0.63, 0.63)
    colors[clr.TitleBg]                 = ImVec4(0.13, 0.13, 0.13, 0.99)
    colors[clr.TitleBgActive]           = ImVec4(0.13, 0.13, 0.13, 0.99)
    colors[clr.TitleBgCollapsed]        = ImVec4(0.05, 0.05, 0.05, 0.79)
    colors[clr.MenuBarBg]               = ImVec4(0.14, 0.14, 0.14, 1.00)
    colors[clr.ScrollbarBg]             = ImVec4(0.02, 0.02, 0.02, 0.53)
    colors[clr.ScrollbarGrab]           = ImVec4(0.31, 0.31, 0.31, 1.00)
    colors[clr.ScrollbarGrabHovered]    = ImVec4(0.41, 0.41, 0.41, 1.00)
    colors[clr.ScrollbarGrabActive]     = ImVec4(0.51, 0.51, 0.51, 1.00)
    colors[clr.CheckMark]               = ImVec4(1.00, 1.00, 1.00, 1.00)
    colors[clr.SliderGrab]              = ImVec4(0.28, 0.28, 0.28, 1.00)
    colors[clr.SliderGrabActive]        = ImVec4(0.35, 0.35, 0.35, 1.00)
    colors[clr.Button]                  = ImVec4(0.26, 0.26, 0.26, 1.00)
    colors[clr.ButtonHovered]           = ImVec4(0.00, 0.53, 0.76, 1.00)
    colors[clr.ButtonActive]            = ImVec4(0.00, 0.43, 0.76, 1.00)
    colors[clr.Header]                  = ImVec4(0.12, 0.12, 0.12, 0.94)
    colors[clr.HeaderHovered]           = ImVec4(0.34, 0.34, 0.35, 0.89)
    colors[clr.HeaderActive]            = ImVec4(0.12, 0.12, 0.12, 0.94)
    colors[clr.Separator]               = colors[clr.Border]
    colors[clr.SeparatorHovered]        = ImVec4(0.26, 0.59, 0.98, 0.78)
    colors[clr.SeparatorActive]         = ImVec4(0.26, 0.59, 0.98, 1.00)
    colors[clr.ResizeGrip]             	= ImVec4(0.26, 0.59, 0.98, 0.25)
    colors[clr.ResizeGripHovered]       = ImVec4(0.26, 0.59, 0.98, 0.67)
    colors[clr.ResizeGripActive]        = ImVec4(0.26, 0.59, 0.98, 0.95)
    colors[clr.CloseButton]             = ImVec4(0.41, 0.41, 0.41, 0.50)
    colors[clr.CloseButtonHovered]      = ImVec4(0.98, 0.39, 0.36, 1.00)
    colors[clr.CloseButtonActive]       = ImVec4(0.98, 0.39, 0.36, 1.00)
    colors[clr.PlotLines]               = ImVec4(0.61, 0.61, 0.61, 1.00)
    colors[clr.PlotLinesHovered]        = ImVec4(1.00, 0.43, 0.35, 1.00)
    colors[clr.PlotHistogram]           = ImVec4(0.90, 0.70, 0.00, 1.00)
    colors[clr.PlotHistogramHovered]    = ImVec4(1.00, 0.60, 0.00, 1.00)
    colors[clr.TextSelectedBg]          = ImVec4(0.00, 0.43, 0.76, 1.00)
    colors[clr.ModalWindowDarkening] 	= ImVec4(0.00, 0.00, 0.00, 0.10)
end

function style_light()
	imgui.SwitchContext()
    local style = imgui.GetStyle()
    local style = imgui.GetStyle()
    local colors = style.Colors
    local clr = imgui.Col
    local ImVec4 = imgui.ImVec4
    local ImVec2 = imgui.ImVec2
    style.WindowRounding = 10.0
    style.FrameRounding = 5.0
    style.ChildWindowRounding = 5.0
    style.WindowTitleAlign = imgui.ImVec2(0.5, 0.5)

	colors[clr.Text]   					= ImVec4(0.10, 0.10, 0.10, 0.90)
	colors[clr.TextDisabled]   			= ImVec4(0.60, 0.60, 0.60, 1.00)
	colors[clr.WindowBg]            	= ImVec4(1.00, 1.00, 1.00, glass_light.v)
	colors[clr.ChildWindowBg]         	= ImVec4(0.96, 0.96, 0.96, glass_light_child.v)
	colors[clr.PopupBg]               	= ImVec4(0.92, 0.92, 0.92, 1.00)
	colors[clr.Border]                	= ImVec4(0.76, 0.76, 0.76, 1.00)
	colors[clr.BorderShadow]          	= ImVec4(0.00, 0.00, 0.00, 0.00)
	colors[clr.FrameBg]               	= ImVec4(0.65, 0.65, 0.65, 0.70)
	colors[clr.FrameBgHovered]        	= ImVec4(0.53, 0.53, 0.53, 1.00)
	colors[clr.FrameBgActive]        	= ImVec4(0.50, 0.50, 0.50, 1.00)
	colors[clr.TitleBg]               	= ImVec4(0.00, 0.43, 0.66, 1.00)
	colors[clr.TitleBgCollapsed]      	= ImVec4(0.00, 0.43, 0.66, 1.00)
	colors[clr.TitleBgActive]         	= ImVec4(0.00, 0.43, 0.66, 1.00)
	colors[clr.MenuBarBg]             	= ImVec4(0.00, 0.37, 0.78, 1.00)
	colors[clr.ScrollbarBg]           	= ImVec4(0.00, 0.00, 0.00, 0.00)
	colors[clr.ScrollbarGrab]         	= ImVec4(0.00, 0.35, 1.00, 0.78)
	colors[clr.ScrollbarGrabHovered] 	= ImVec4(0.00, 0.33, 1.00, 0.84)
	colors[clr.ScrollbarGrabActive]   	= ImVec4(0.00, 0.31, 1.00, 0.88)
	colors[clr.ComboBg]               	= ImVec4(0.92, 0.92, 0.92, 1.00)
	colors[clr.CheckMark]             	= ImVec4(0.00, 0.43, 0.66, 1.00)
	colors[clr.SliderGrab]            	= ImVec4(0.00, 0.49, 1.00, 0.59)
	colors[clr.SliderGrabActive]    	= ImVec4(0.00, 0.39, 1.00, 0.71)
	colors[clr.Button]                  = ImVec4(0.77, 0.77, 0.77, 1.00)
    colors[clr.ButtonHovered]           = ImVec4(0.00, 0.43, 0.66, 1.00)
    colors[clr.ButtonActive]            = ImVec4(0.00, 0.33, 0.56, 1.00)
	colors[clr.Header]                	= ImVec4(0.00, 0.43, 0.66, 1.00)
	colors[clr.HeaderHovered]         	= ImVec4(0.00, 0.33, 0.56, 1.00)
	colors[clr.HeaderActive]          	= ImVec4(0.00, 0.23, 0.46, 1.00)
	colors[clr.ResizeGrip]            	= ImVec4(0.00, 0.39, 1.00, 0.59)
	colors[clr.ResizeGripHovered]     	= ImVec4(0.00, 0.27, 1.00, 0.59)
	colors[clr.ResizeGripActive]      	= ImVec4(0.00, 0.25, 1.00, 0.63)
	colors[clr.CloseButton]           	= ImVec4(0.00, 0.35, 0.96, 0.71)
	colors[clr.CloseButtonHovered]    	= ImVec4(0.00, 0.31, 0.88, 0.69)
	colors[clr.CloseButtonActive]     	= ImVec4(0.00, 0.25, 0.88, 0.67)
	colors[clr.PlotLines]             	= ImVec4(0.00, 0.39, 1.00, 0.75)
	colors[clr.PlotLinesHovered]     	= ImVec4(0.00, 0.39, 1.00, 0.75)
	colors[clr.PlotHistogram]         	= ImVec4(0.00, 0.39, 1.00, 0.75)
	colors[clr.PlotHistogramHovered]  	= ImVec4(0.00, 0.35, 0.92, 0.78)
	colors[clr.TextSelectedBg]          = ImVec4(0.00, 0.43, 0.66, 1.00)
	colors[clr.ModalWindowDarkening] 	= ImVec4(0.00, 0.00, 0.00, 0.10)
end

function style_fiolet()
    imgui.SwitchContext()
    local style = imgui.GetStyle()
    local style = imgui.GetStyle()
    local colors = style.Colors
    local clr = imgui.Col
    local ImVec4 = imgui.ImVec4
    local ImVec2 = imgui.ImVec2
    style.WindowRounding = 10.0
    style.FrameRounding = 5.0
    style.ChildWindowRounding = 5.0
    style.WindowTitleAlign = imgui.ImVec2(0.5, 0.5)

    colors[clr.Text]                  	= ImVec4(1.00, 1.00, 1.00, 1.00)
    colors[clr.WindowBg]              	= ImVec4(0.14, 0.12, 0.16, 1.00)
    colors[clr.ChildWindowBg]         	= ImVec4(0.30, 0.20, 0.39, 0.00)
    colors[clr.PopupBg]               	= ImVec4(0.05, 0.05, 0.10, 0.90)
    colors[clr.Border]                	= ImVec4(0.89, 0.85, 0.92, 0.30)
    colors[clr.BorderShadow]          	= ImVec4(0.00, 0.00, 0.00, 0.00)
    colors[clr.FrameBg]               	= ImVec4(0.30, 0.20, 0.39, 1.00)
    colors[clr.FrameBgHovered]        	= ImVec4(0.41, 0.19, 0.63, 0.68)
    colors[clr.FrameBgActive]         	= ImVec4(0.41, 0.19, 0.63, 1.00)
    colors[clr.TitleBg]               	= ImVec4(0.41, 0.19, 0.63, 0.78)
    colors[clr.TitleBgCollapsed]      	= ImVec4(0.41, 0.19, 0.63, 0.78)
    colors[clr.TitleBgActive]         	= ImVec4(0.41, 0.19, 0.63, 0.78)
    colors[clr.MenuBarBg]             	= ImVec4(0.30, 0.20, 0.39, 0.57)
    colors[clr.ScrollbarBg]           	= ImVec4(0.30, 0.20, 0.39, 1.00)
    colors[clr.ScrollbarGrab]         	= ImVec4(0.51, 0.29, 0.73, 1.00)
    colors[clr.ScrollbarGrabHovered]  	= ImVec4(0.51, 0.29, 0.73, 0.80)
    colors[clr.ScrollbarGrabActive]   	= ImVec4(0.51, 0.29, 0.73, 0.60)
    colors[clr.ComboBg]               	= ImVec4(0.30, 0.20, 0.39, 1.00)
    colors[clr.CheckMark]             	= ImVec4(0.56, 0.61, 1.00, 1.00)
    colors[clr.SliderGrab]            	= ImVec4(0.41, 0.19, 0.63, 0.24)
    colors[clr.SliderGrabActive]      	= ImVec4(0.41, 0.19, 0.63, 1.00)
    colors[clr.Button]                	= ImVec4(0.41, 0.19, 0.63, 0.44)
    colors[clr.ButtonHovered]         	= ImVec4(0.41, 0.19, 0.63, 0.86)
    colors[clr.ButtonActive]        	= ImVec4(0.64, 0.33, 0.94, 1.00)
    colors[clr.Header]               	= ImVec4(0.41, 0.19, 0.63, 0.76)
    colors[clr.HeaderHovered]         	= ImVec4(0.41, 0.19, 0.63, 0.86)
    colors[clr.HeaderActive]          	= ImVec4(0.41, 0.19, 0.63, 1.00)
    colors[clr.ResizeGrip]            	= ImVec4(0.41, 0.19, 0.63, 0.20)
    colors[clr.ResizeGripHovered]     	= ImVec4(0.41, 0.19, 0.63, 0.78)
    colors[clr.ResizeGripActive]      	= ImVec4(0.41, 0.19, 0.63, 1.00)
    colors[clr.CloseButton]           	= ImVec4(0.66, 0.00, 1.00, 0.70)
    colors[clr.CloseButtonHovered]    	= ImVec4(0.56, 0.00, 0.90, 0.60)
    colors[clr.CloseButtonActive]     	= ImVec4(0.46, 0.00, 0.80, 0.50)
    colors[clr.PlotLines]             	= ImVec4(0.89, 0.85, 0.92, 0.63)
    colors[clr.PlotLinesHovered]      	= ImVec4(0.41, 0.19, 0.63, 1.00)
    colors[clr.PlotHistogram]         	= ImVec4(0.89, 0.85, 0.92, 0.63)
    colors[clr.PlotHistogramHovered]  	= ImVec4(0.41, 0.19, 0.63, 1.00)
    colors[clr.TextSelectedBg]        	= ImVec4(0.41, 0.19, 0.63, 0.43)
    colors[clr.ModalWindowDarkening] 	= ImVec4(0.00, 0.00, 0.00, 0.10)
end

function style_gray()
    imgui.SwitchContext()
    local style = imgui.GetStyle()
    local style = imgui.GetStyle()
    local colors = style.Colors
    local clr = imgui.Col
    local ImVec4 = imgui.ImVec4
    local ImVec2 = imgui.ImVec2
    style.WindowRounding = 10.0
    style.FrameRounding = 5.0
    style.ChildWindowRounding = 5.0
    style.WindowTitleAlign = imgui.ImVec2(0.5, 0.5)
  

	colors[clr.Text] 					= ImVec4(0.95, 0.96, 0.98, 1.00)
	colors[clr.TextDisabled] 			= ImVec4(0.36, 0.42, 0.47, 1.00)
	colors[clr.WindowBg] 				= ImVec4(0.11, 0.15, 0.17, 1.00)
	colors[clr.ChildWindowBg] 			= ImVec4(0.15, 0.18, 0.22, 1.00)
	colors[clr.PopupBg] 				= ImVec4(0.11, 0.15, 0.17, 1.00)
	colors[clr.Border] 					= ImVec4(0.86, 0.86, 0.86, 1.00)
	colors[clr.BorderShadow] 			= ImVec4(0.00, 0.00, 0.00, 0.00)
	colors[clr.FrameBg] 				= ImVec4(0.20, 0.25, 0.29, 1.00)
	colors[clr.FrameBgHovered] 			= ImVec4(0.12, 0.20, 0.28, 1.00)
	colors[clr.FrameBgActive] 			= ImVec4(0.09, 0.12, 0.14, 1.00)
	colors[clr.TitleBg] 				= ImVec4(0.09, 0.12, 0.14, 0.65)
	colors[clr.TitleBgCollapsed] 		= ImVec4(0.00, 0.00, 0.00, 0.51)
	colors[clr.TitleBgActive] 			= ImVec4(0.08, 0.10, 0.12, 1.00)
	colors[clr.MenuBarBg] 				= ImVec4(0.15, 0.18, 0.22, 1.00)
	colors[clr.ScrollbarBg] 			= ImVec4(0.02, 0.02, 0.02, 0.39)
	colors[clr.ScrollbarGrab] 			= ImVec4(0.20, 0.25, 0.29, 1.00)
	colors[clr.ScrollbarGrabHovered] 	= ImVec4(0.18, 0.22, 0.25, 1.00)
	colors[clr.ScrollbarGrabActive] 	= ImVec4(0.09, 0.21, 0.31, 1.00)
	colors[clr.ComboBg] 				= ImVec4(0.20, 0.25, 0.29, 1.00)
	colors[clr.CheckMark] 				= ImVec4(0.28, 0.56, 1.00, 1.00)
	colors[clr.SliderGrab] 				= ImVec4(0.28, 0.56, 1.00, 1.00)
	colors[clr.SliderGrabActive] 		= ImVec4(0.37, 0.61, 1.00, 1.00)
	colors[clr.Button] 					= ImVec4(0.20, 0.25, 0.29, 1.00)
	colors[clr.ButtonHovered] 			= ImVec4(0.28, 0.56, 1.00, 1.00)
	colors[clr.ButtonActive]			= ImVec4(0.06, 0.53, 0.98, 1.00)
	colors[clr.Header] 					= ImVec4(0.20, 0.25, 0.29, 0.55)
	colors[clr.HeaderHovered] 			= ImVec4(0.26, 0.59, 0.98, 0.80)
	colors[clr.HeaderActive] 			= ImVec4(0.26, 0.59, 0.98, 1.00)
	colors[clr.ResizeGrip] 				= ImVec4(0.26, 0.59, 0.98, 0.25)
	colors[clr.ResizeGripHovered] 		= ImVec4(0.26, 0.59, 0.98, 0.67)
	colors[clr.ResizeGripActive] 		= ImVec4(0.06, 0.05, 0.07, 1.00)
	colors[clr.CloseButton] 			= ImVec4(0.40, 0.39, 0.38, 0.16)
	colors[clr.CloseButtonHovered] 		= ImVec4(0.40, 0.39, 0.38, 0.39)
	colors[clr.CloseButtonActive] 		= ImVec4(0.40, 0.39, 0.38, 1.00)
	colors[clr.PlotLines] 				= ImVec4(0.61, 0.61, 0.61, 1.00)
	colors[clr.PlotLinesHovered] 		= ImVec4(1.00, 0.43, 0.35, 1.00)
	colors[clr.PlotHistogram] 			= ImVec4(0.90, 0.70, 0.00, 1.00)
	colors[clr.PlotHistogramHovered] 	= ImVec4(1.00, 0.60, 0.00, 1.00)
	colors[clr.TextSelectedBg] 			= ImVec4(0.25, 1.00, 0.00, 0.43)
	colors[clr.ModalWindowDarkening] 	= ImVec4(0.00, 0.00, 0.00, 0.10)
end

function set_style(type)
	if type == 0 then 
		style_dark()
	elseif type == 1 then
		style_light()
	elseif type == 2 then
		style_fiolet()
	elseif type == 3 then 
		style_gray()
	end
end
set_style(configuration.main_settings.style)

function taginfo()
	if imgui.BeginPopupModal(u8("��������� ����"), _, imgui.WindowFlags.NoCollapse + imgui.WindowFlags.NoScrollbar + imgui.WindowFlags.AlwaysAutoResize) then
			imgui.CenterTextColoredRGB(sc..'����� �� ������ ���, ��� �� ����������� ���')
			imgui.Separator()
            if imgui.Button('{select_id}', imgui.ImVec2(120, 25)) then setClipboardText('{select_id}'); sampAddChatMessage(tag..'��� ����������!', mcx) end
            imgui.SameLine(); imgui.TextColoredRGB(mc..'�������� ID ������ � ������� ����������������')
            if imgui.Button('{select_name}', imgui.ImVec2(120, 25)) then setClipboardText('{select_name}'); sampAddChatMessage(tag..'��� ����������!', mcx) end
            imgui.SameLine(); imgui.TextColoredRGB(mc..'�������� NickName ������ � ������� ����������������')
            if imgui.Button('{my_id}', imgui.ImVec2(120, 25)) then setClipboardText('{my_id}'); sampAddChatMessage(tag..'��� ����������!', mcx) end
            imgui.HintHovered(u8'{my_id}\n - '..select(2, sampGetPlayerIdByCharHandle(playerPed)))
            imgui.SameLine(); imgui.TextColoredRGB(mc..'�������� ��� ID')
            if imgui.Button('{my_name}', imgui.ImVec2(120, 25)) then setClipboardText('{my_name}'); sampAddChatMessage(tag..'��� ����������!') end
            imgui.HintHovered(u8'{my_name}\n - '..sampGetPlayerNickname(select(2, sampGetPlayerIdByCharHandle(playerPed))))
            imgui.SameLine(); imgui.TextColoredRGB(mc..'�������� ��� NickName')
            if imgui.Button('{closest_id}', imgui.ImVec2(120, 25)) then setClipboardText('{closest_id}'); sampAddChatMessage(tag..'��� ����������!', mcx) end
            imgui.HintHovered(u8'����� �� ���� ����� ������� � ��������� "{closest_id}"\n - ����� �� ���� ����� ������� � ��������� "228"')
            imgui.SameLine(); imgui.TextColoredRGB(mc..'�������� ID ���������� � ��� ������')
            if imgui.Button('{closest_name}', imgui.ImVec2(120, 25)) then setClipboardText('{closest_name}'); sampAddChatMessage(tag..'��� ����������!', mcx) end
           	imgui.HintHovered(u8'����� �� ���� ����� {closest_name}\n - ����� �� ���� ����� Jeffy_Cosmo')
            imgui.SameLine(); imgui.TextColoredRGB(mc..'�������� NickName ���������� � ��� ������')
            if imgui.Button('{time}', imgui.ImVec2(120, 25)) then setClipboardText('{time}'); sampAddChatMessage(tag..'��� ����������!', mcx) end
            imgui.HintHovered(u8'/do �� ����� {time_s}\n - /do �� ����� '..os.date("%H:%M", os.time()))
            imgui.SameLine(); imgui.TextColoredRGB(mc..'������� ����� ( ��:�� )')
            if imgui.Button('{time_s}', imgui.ImVec2(120, 25)) then setClipboardText('{time_s}'); sampAddChatMessage(tag..'��� ����������!', mcx) end
            imgui.HintHovered(u8'/do �� ����� {time_s}\n - /do �� ����� '..os.date("%H:%M:%S", os.time()))
            imgui.SameLine(); imgui.TextColoredRGB(mc..'������� ����� ( ��:��:�� )')
            if imgui.Button('{rank}', imgui.ImVec2(120, 25)) then setClipboardText('{rank}'); sampAddChatMessage(tag..'��� ����������!', mcx) end
            imgui.HintHovered(u8('��� ��������� - {rank}\n - ��� ��������� - '..configuration.nameRank[configuration.main_settings.rank]))
            imgui.SameLine(); imgui.TextColoredRGB(mc..'�������� ������ �������� �����')
            if imgui.Button('{score}', imgui.ImVec2(120, 25)) then setClipboardText('{rank}'); sampAddChatMessage(tag..'��� ����������!', mcx) end
            imgui.HintHovered(u8('� �������� � ����� ��� {score} ���\n - � �������� � ����� ��� '..sampGetPlayerScore(select(2, sampGetPlayerIdByCharHandle(playerPed)))..' ���'))
            imgui.SameLine(); imgui.TextColoredRGB(mc..'��� ������� �������')
            if imgui.Button('{screen}', imgui.ImVec2(120, 25)) then setClipboardText('{screen}'); sampAddChatMessage(tag..'��� ����������!', mcx) end
            imgui.HintHovered(u8('/giverank 123 5 {screen}\n - �������� �������: /giverank 123 5, � ����� ������� ��������'))
            imgui.SameLine(); imgui.TextColoredRGB(mc..'������ ��������')
            if imgui.Button('{sex:text1|text2}', imgui.ImVec2(120, 25)) then setClipboardText('{sex:text1|text2}'); sampAddChatMessage(tag..'��� ����������!', mcx) end
            imgui.HintHovered(u8'/me {sex:����|�����} ����� �� �����\n - ���� ������� ���: /me ����..\n - ���� ������� ���: /me �����')
            imgui.SameLine(); imgui.TextColoredRGB(mc..'������ ����� � ����������� �� ������ ����')
            if imgui.Button(u8'@[ID ������]', imgui.ImVec2(120, 25)) then setClipboardText('@[ID]'); sampAddChatMessage(tag..'��� ����������!', mcx) end
            imgui.HintHovered(u8'/fam @228 �������� ������?\n - /fam Jeffy Cosmo, �������� ������?')
            imgui.SameLine(); imgui.TextColoredRGB(mc..'�������� NickName ������ ���������� ID �� �������')
            imgui.Separator()
            if imgui.Button(u8'��������� �����##tag', imgui.ImVec2(-1, 30)) then
                imgui.CloseCurrentPopup()
            end
        imgui.EndPopup()
    end
end

function play_bind(num)
	lua_thread.create(function()
		if num ~= -1 then
	        for bp in configuration.Binds_Action[num]:gmatch('[^~]+') do
	            sampSendChat(u8:decode(tostring(bp)))
	            wait(configuration.Binds_Deleay[num])
	        end
	        num = -1
	    end
	end)
end

function frpbat()
	local weapon = getCurrentCharWeapon(playerPed)
	if weapon == 3 and not rp_check then 
		sampSendChat(configuration.main_settings.rpbat_true)
		rp_check = true
	elseif weapon ~= 3 and rp_check then
		sampSendChat(configuration.main_settings.rpbat_false)
		rp_check = false
	end
end

function imgui.CenterTextColoredRGB(text)
    local width = imgui.GetWindowWidth()
    local style = imgui.GetStyle()
    local colors = style.Colors
    local ImVec4 = imgui.ImVec4

    local explode_argb = function(argb)
        local a = bit.band(bit.rshift(argb, 24), 0xFF)
        local r = bit.band(bit.rshift(argb, 16), 0xFF)
        local g = bit.band(bit.rshift(argb, 8), 0xFF)
        local b = bit.band(argb, 0xFF)
        return a, r, g, b
    end

    local getcolor = function(color)
        if color:sub(1, 6):upper() == 'SSSSSS' then
            local r, g, b = colors[1].x, colors[1].y, colors[1].z
            local a = tonumber(color:sub(7, 8), 16) or colors[1].w * 255
            return ImVec4(r, g, b, a / 255)
        end
        local color = type(color) == 'string' and tonumber(color, 16) or color
        if type(color) ~= 'number' then return end
        local r, g, b, a = explode_argb(color)
        return imgui.ImColor(r, g, b, a):GetVec4()
    end

    local render_text = function(text_)
        for w in text_:gmatch('[^\r\n]+') do
            local textsize = w:gsub('{.-}', '')
            local text_width = imgui.CalcTextSize(u8(textsize))
            imgui.SetCursorPosX( width / 2 - text_width .x / 2 )
            local text, colors_, m = {}, {}, 1
            w = w:gsub('{(......)}', '{%1FF}')
            while w:find('{........}') do
                local n, k = w:find('{........}')
                local color = getcolor(w:sub(n + 1, k - 1))
                if color then
                    text[#text], text[#text + 1] = w:sub(m, n - 1), w:sub(k + 1, #w)
                    colors_[#colors_ + 1] = color
                    m = n
                end
                w = w:sub(1, n - 1) .. w:sub(k + 1, #w)
            end
            if text[0] then
                for i = 0, #text do
                    imgui.TextColored(colors_[i] or colors[1], u8(text[i]))
                    imgui.SameLine(nil, 0)
                end
                imgui.NewLine()
            else
                imgui.Text(u8(w))
            end
        end
    end
    render_text(text)
end

function sampev.onSendChat(msg)
    if msg:find('{select_id}') then
        if actionId ~= nil then
            local input_with_select_id = msg:gsub('{select_id}', actionId)
            sampSendChat(input_with_select_id)
            return false
        else
            sampAddChatMessage(tag..'�� ��� �� �������� ������ ��� ���� '..mc..'{select_id}', mcx)
            return false
        end
    end

    if msg:find('{select_name}') then
        if actionId ~= nil then
        	local name_without_space = sampGetPlayerNickname(actionId):gsub('_', ' ')
            local input_with_select_name = msg:gsub('{select_name}', name_without_space)
            sampSendChat(input_with_select_name)
            return false
        else
            sampAddChatMessage(tag..'�� ��� �� �������� ������ ��� ���� '..mc..'{select_name}', mcx)
            return false
        end
    end

    if msg:find('{my_id}') then
        local input_with_id = msg:gsub('{my_id}', select(2, sampGetPlayerIdByCharHandle(playerPed)))
        sampSendChat(input_with_id)
        return false
    end

    if msg:find('{my_name}') then
        local _, myid = sampGetPlayerIdByCharHandle(playerPed)
      	local name_without_space = sampGetPlayerNickname(myid):gsub('_', ' ')
        local input_with_name = msg:gsub('{my_name}', name_without_space)
        sampSendChat(input_with_name)
        return false
    end

    if msg:find('{closest_id}') then
        if sampGetPlayerCount(true) <= 1 then 
            sampAddChatMessage(tag..'� ����� ������� ��� ������� ��� ���������� ���� '..mc..'{closest_id}', mcx)
            return false
        else
            local input_with_rid = msg:gsub('{closest_id}', getClosestPlayerId())
            sampSendChat(input_with_rid)
            return false
        end
    end

    if msg:find('{closest_name}') then
        if sampGetPlayerCount(true) <= 1 then 
            sampAddChatMessage(tag..'� ����� ������� ��� ������� ��� ���������� ���� '..mc..'{closest_name}', mcx)
            return false
        else
        	local name_without_space = sampGetPlayerNickname(getClosestPlayerId()):gsub('_', ' ')
            local input_with_rname = msg:gsub('{closest_name}', name_without_space)
            sampSendChat(input_with_rname)
            return false
        end
    end

    if msg:find('@%d+') then
        local get_id = msg:match('@(%d+)')
        if sampIsPlayerConnected(get_id) then
        	local get_name = sampGetPlayerNickname(get_id)
        	if get_name:find('%_') then
	        	local get_name = get_name:match('(%a+)%_')
	        	local get_name = msg:gsub('@%d+', get_name)
	            sampSendChat(get_name)
	        else
	        	local get_name = msg:gsub('@%d+', get_name)
	        	sampSendChat(get_name)
	        end
        else
            sampAddChatMessage(tag..'������ � ����� ID �� ������� ���!', mcx)
        end
        return false
    end

    if msg:find('{time}') then
        local get_time = msg:gsub('{time}', os.date("%H:%M", os.time()))
        sampSendChat(get_time)
        return false
    end
    if msg:find('{time_s}') then
        local get_time = msg:gsub('{time_s}', os.date("%H:%M:%S", os.time()))
        sampSendChat(get_time)
        return false
    end

    if msg:find('{rank}') then
        local get_rank = msg:gsub('{rank}', configuration.nameRank[configuration.main_settings.rank])
        sampSendChat(get_rank)
        return false
    end

    if msg:find('{score}') then
        local get_score = msg:gsub('{score}', sampGetPlayerScore(select(2, sampGetPlayerIdByCharHandle(playerPed))))
        sampSendChat(get_score)
        return false
    end

    if msg:find('{sex:%A+|%A+}') then
        local male, female = msg:match('{sex:(%A+)|(%A+)}')
        if configuration.main_settings.sex == 1 then
        	local returnMsg = msg:gsub('{sex:%A+|%A+}', male, 1)
        	sampSendChat(tostring(returnMsg))
        	return false
        else
        	local returnMsg = msg:gsub('{sex:%A+|%A+}', female, 1)
        	sampSendChat(tostring(returnMsg))
        	return false
        end
    end

    if msg:find('{screen}') then
        local screenMsg = msg:gsub('{screen}', '')
        if #screenMsg > 0 then sampSendChat(screenMsg) end
        lua_thread.create(function()
	        wait(300)
			sampSendChat('/time')
			wait(500)
			setVirtualKeyDown(VK_F8, true)
			wait(0)
			setVirtualKeyDown(VK_F8, false)
		end)
        return false
    end

    if configuration.main_settings.accent_status then
    	if #msg > 0 then
    		if msg ~= ')' and msg ~= '(' and msg ~= '))' and msg ~= '((' and msg ~= 'xD' and msg ~= ':D' then
	    		return {configuration.main_settings.accent..' '..msg}
	    	end
	    end
    end
end

function sampev.onSendCommand(cmd)
    if cmd:find('{select_id}') then
        if actionId ~= nil then
            local input_with_select_id = cmd:gsub('{select_id}', actionId)
            sampSendChat(input_with_select_id)
            return false
        else
            sampAddChatMessage(tag..'�� ��� �� �������� ������ ��� ���� '..mc..'{select_id}', mcx)
            return false
        end
    end

    if cmd:find('{select_name}') then
        if actionId ~= nil then
        	local name_without_space = sampGetPlayerNickname(actionId):gsub('_', ' ')
            local input_with_select_name = cmd:gsub('{select_name}', name_without_space)
            sampSendChat(input_with_select_name)
            return false
        else
            sampAddChatMessage(tag..'�� ��� �� �������� ������ ��� ���� '..mc..'{select_name}', mcx)
            return false
        end
    end

    if cmd:find('{my_id}') then
        local input_with_id = cmd:gsub('{my_id}', select(2, sampGetPlayerIdByCharHandle(playerPed)))
        sampSendChat(input_with_id)
        return false
    end

    if cmd:find('{my_name}') then
        local _, myid = sampGetPlayerIdByCharHandle(playerPed)
      	local name_without_space = sampGetPlayerNickname(myid):gsub('_', ' ')
        local input_with_name = cmd:gsub('{my_name}', name_without_space)
        sampSendChat(input_with_name)
        return false
    end

    if cmd:find('{closest_id}') then
        if sampGetPlayerCount(true) <= 1 then 
            sampAddChatMessage(tag..'� ����� ������� ��� ������� ��� ���������� ���� '..mc..'{closest_id}', mcx)
            return false
        else
            local input_with_rid = cmd:gsub('{closest_id}', getClosestPlayerId())
            sampSendChat(input_with_rid)
            return false
        end
    end

    if cmd:find('{closest_name}') then
        if sampGetPlayerCount(true) <= 1 then 
            sampAddChatMessage(tag..'� ����� ������� ��� ������� ��� ���������� ���� '..mc..'{closest_name}', mcx)
            return false
        else
        	local name_without_space = sampGetPlayerNickname(getClosestPlayerId()):gsub('_', ' ')
            local input_with_rname = cmd:gsub('{closest_name}', name_without_space)
            sampSendChat(input_with_rname)
            return false
        end
    end

    if cmd:find('{time}') then
        local get_time = cmd:gsub('{time}', os.date("%H:%M", os.time()))
        sampSendChat(get_time)
        return false
    end
    if cmd:find('{time_s}') then
        local get_time = cmd:gsub('{time_s}', os.date("%H:%M:%S", os.time()))
        sampSendChat(get_time)
        return false
    end

    if cmd:find('{rank}') then
        local get_rank = cmd:gsub('{rank}', configuration.nameRank[configuration.main_settings.rank])
        sampSendChat(get_rank)
        return false
    end

    if cmd:find('{score}') then
        local get_score = cmd:gsub('{score}', sampGetPlayerScore(select(2, sampGetPlayerIdByCharHandle(playerPed))))
        sampSendChat(get_score)
        return false
    end

    if cmd:find('{sex:%A+|%A+}') then
        local male, female = cmd:match('{sex:(%A+)|(%A+)}')
        if configuration.main_settings.sex == 1 then
        	local returnMsg = cmd:gsub('{sex:%A+|%A+}', male, 1)
        	sampSendChat(tostring(returnMsg))
        	return false
        else
        	local returnMsg = cmd:gsub('{sex:%A+|%A+}', female, 1)
        	sampSendChat(tostring(returnMsg))
        	return false
        end
    end

    if cmd:find('{screen}') then
        local screenMsg = cmd:gsub('{screen}', '')
        sampSendChat(screenMsg)
        lua_thread.create(function()
	        wait(300)
			sampSendChat('/time')
			wait(500)
			setVirtualKeyDown(VK_F8, true)
			wait(0)
			setVirtualKeyDown(VK_F8, false)
		end)
        return false
    end

    if cmd:find('@%d+') then
        local get_id = cmd:match('@(%d+)')
        if sampIsPlayerConnected(get_id) then
        	local get_name = sampGetPlayerNickname(get_id)
        	if get_name:find('%_') then
	        	local get_name = get_name:match('(%a+)%_')
	        	local get_name = cmd:gsub('@%d+', get_name)
	            sampSendChat(get_name)
	        else
	        	local get_name = cmd:gsub('@%d+', get_name)
	        	sampSendChat(get_name)
	        end
        else
            sampAddChatMessage(tag..'������ � ����� ID �� ������� ���!', mcx)
        end
        return false
    end

    if configuration.main_settings.accent_status then
	    if cmd:find('%/c') then
	    	local command = cmd:match('%/c (.+)')
	    	if command then
	    		if command:find('%S+') then
	    			return {'/c '..configuration.main_settings.accent..' '..command}
	    		end
	    	end
	    end
	    if cmd:find('%/s') then
	    	local command = cmd:match('%/s (.+)')
	    	if command then
	    		if command:find('%S+') then
	    			return {'/s '..configuration.main_settings.accent..' '..command}
	    		end
	    	end
	    end
	end

	if configuration.main_settings.rank > 8 then
		if cmd:find('%/invite %d+') then
	    	local id_log_invite = cmd:match('%/invite (%d+)')
	    	if sampIsPlayerConnected(id_log_invite) then
	    		if not doesDirectoryExist(getWorkingDirectory()..'/BHelper/�����������') then 
	    			if createDirectory(getWorkingDirectory()..'/BHelper/�����������') then 
	    				sampfuncsLog(mc..'Bank-Helper:'..wc..' ������� ����������� /BHelper/�����������')
	    			end
	    		end
	    		log_invite = io.open(getWorkingDirectory()..'/BHelper/�����������/�������.txt', "a")
				log_invite:write("["..os.date("%H:%M:%S", os.time()).."] ������ �� �������: "..sampGetPlayerNickname(tonumber(id_log_invite)).."("..tostring(id_log_invite)..")\n")
				log_invite:close()
	    	end
	    end
	    if cmd:find('%/uninvite %d+ .+') then
	    	local id_log_uninvite, reason_log_uninvite = cmd:match('%/uninvite (%d+) (.+)')
	    	if sampIsPlayerConnected(id_log_uninvite) then
	    		if not doesDirectoryExist(getWorkingDirectory()..'/BHelper/�����������') then 
	    			if createDirectory(getWorkingDirectory()..'/BHelper/�����������') then 
	    				sampfuncsLog(mc..'Bank-Helper:'..wc..' ������� ����������� /BHelper/�����������')
	    			end
	    		end
	    		log_uninvite = io.open(getWorkingDirectory()..'/BHelper/�����������/����������.txt', "a")
				log_uninvite:write("["..os.date("%H:%M:%S", os.time()).."] ������ �� �������: "..sampGetPlayerNickname(tonumber(id_log_uninvite)).."("..tostring(id_log_uninvite)..") | �������: "..reason_log_uninvite.."\n")
				log_uninvite:close()
	    	end
	    end
	    if cmd:find('%/fwarn %d+ .+') then
	    	local id_log_fwarn, reason_log_fwarn = cmd:match('%/fwarn (%d+) (.+)')
	    	if sampIsPlayerConnected(id_log_fwarn) then
	    		if not doesDirectoryExist(getWorkingDirectory()..'/BHelper/�����������') then 
	    			if createDirectory(getWorkingDirectory()..'/BHelper/�����������') then 
	    				sampfuncsLog(mc..'Bank-Helper:'..wc..' ������� ����������� /BHelper/�����������')
	    			end
	    		end
	    		log_fwarn = io.open(getWorkingDirectory()..'/BHelper/�����������/��������.txt', "a")
				log_fwarn:write("["..os.date("%H:%M:%S", os.time()).."] ����� ������� ����������: "..sampGetPlayerNickname(tonumber(id_log_fwarn)).."("..tostring(id_log_fwarn)..") | �������: "..reason_log_fwarn.."\n")
				log_fwarn:close()
	    	end
	    end
	    if cmd:find('%/fmute %d+ %d+ .+') then
	    	local id_log_fmute, time_log_fmute, reason_log_fmute = cmd:match('%/fmute (%d+) (%d+) (.+)')
	    	if sampIsPlayerConnected(id_log_fmute) then
	    		if not doesDirectoryExist(getWorkingDirectory()..'/BHelper/�����������') then 
	    			if createDirectory(getWorkingDirectory()..'/BHelper/�����������') then 
	    				sampfuncsLog(mc..'Bank-Helper:'..wc..' ������� ����������� /BHelper/�����������')
	    			end
	    		end
	    		log_fmute = io.open(getWorkingDirectory()..'/BHelper/�����������/����.txt', "a")
				log_fmute:write("["..os.date("%H:%M:%S", os.time()).."] ����� ��� ����������: "..sampGetPlayerNickname(tonumber(id_log_fmute)).."("..tostring(id_log_fmute)..") �� "..tostring(time_log_fmute).." ����� | �������: "..reason_log_fmute.."\n")
				log_fmute:close()
	    	end
	    end
	    if cmd:find('%/blacklist %d+ .+') then
	    	local id_log_bl, reason_log_bl = cmd:match('%/blacklist (%d+) (.+)')
	    	if sampIsPlayerConnected(id_log_bl) then
	    		if not doesDirectoryExist(getWorkingDirectory()..'/BHelper/�����������') then 
	    			if createDirectory(getWorkingDirectory()..'/BHelper/�����������') then 
	    				sampfuncsLog(mc..'Bank-Helper:'..wc..' ������� ����������� /BHelper/�����������')
	    			end
	    		end
	    		log_bl = io.open(getWorkingDirectory()..'/BHelper/�����������/��.txt', "a")
				log_bl:write("["..os.date("%H:%M:%S", os.time()).."] ������ � ��: "..sampGetPlayerNickname(tonumber(id_log_bl)).."("..tostring(id_log_bl)..") | �������: "..reason_log_bl.."\n")
				log_bl:close()
	    	end
	    end
	end
end

function getClosestPlayerId()
    local minDist = 9999
    local closestId = -1
    local x, y, z = getCharCoordinates(playerPed)
    for i = 0, 999 do
        local streamed, pedID = sampGetCharHandleBySampPlayerId(i)
        if streamed then
            local xi, yi, zi = getCharCoordinates(pedID)
            local dist = math.sqrt( (xi - x) ^ 2 + (yi - y) ^ 2 + (zi - z) ^ 2 )
            if dist < minDist then
                minDist = dist
                closestId = i
            end
        end
    end
    return closestId
end

function get_timer(time)
	return string.format("%s:%s:%s", string.format("%s%s", (tonumber(os.date("%H", time)) < tonumber(os.date("%H", 0)) and 24 + tonumber(os.date("%H", time)) - tonumber(os.date("%H", 0)) or tonumber(os.date("%H", time)) - tonumber(os.date("%H", 0))) < 10 and 0 or "", tonumber(os.date("%H", time)) < tonumber(os.date("%H", 0)) and 24 + tonumber(os.date("%H", time)) - tonumber(os.date("%H", 0)) or tonumber(os.date("%H", time)) - tonumber(os.date("%H", 0))), os.date("%M", time), os.date("%S", time))
end

function imgui.VerticalSeparator()
    local p = imgui.GetCursorScreenPos()
    imgui.GetWindowDrawList():AddLine(imgui.ImVec2(p.x, p.y), imgui.ImVec2(p.x, p.y + imgui.GetContentRegionMax().y), imgui.GetColorU32(imgui.GetStyle().Colors[imgui.Col.Separator]))
end

function getDistBetweenPlayers(btwid)
	if sampIsPlayerConnected(btwid) then
		local res, charbid = sampGetCharHandleBySampPlayerId(btwid)
		if res then
			local kx, ky, kz = getCharCoordinates(playerPed)
			local vx, vy, vz = getCharCoordinates(charbid)
			local distbtw = getDistanceBetweenCoords3d(kx, ky, kz, vx, vy, vz)
			return math.floor(distbtw)
		end
	end
	return 10000
end

function getDistanceBetween3dText(Id3dtext)
    if sampIs3dTextDefined(Id3dtext) then
        local _, _, X3d, Y3d, Z3d, _, _, _, _ = sampGet3dTextInfoById(Id3dtext)
        local Xme, Yme, Zme = getCharCoordinates(playerPed)
        return math.floor(getDistanceBetweenCoords3d(Xme, Yme, Zme, X3d, Y3d, Z3d))
    end
    return nil
end

function timer()
    while true do
        wait(1000)
        configuration.timer.online = configuration.timer.online + 1
    end
end

function members()
	while sampIsDialogActive() do wait(100) end
	while true do wait(0)
		if configuration.main_settings.members_state and sampGetPlayerColor(select(2, sampGetPlayerIdByCharHandle(playerPed))) == 2150206647 then
			while true do
		   		if not sampIsDialogActive() then 
			   		getMB = true
			        sampSendChat('/members')
			        membersList = {}
			    end
		        wait(members_delay.v * 1000)
			end
	    end
	end
end

function setMarker(type, x, y, z, radius, color)
    deleteCheckpoint(marker)
    removeBlip(checkpoint)
    checkpoint = addBlipForCoord(x, y, z)
    marker = createCheckpoint(type, x, y, z, 1, 1, 1, radius)
    changeBlipColour(checkpoint, color)
    lua_thread.create(function()
    repeat
        wait(0)
        local x1, y1, z1 = getCharCoordinates(PLAYER_PED)
        until getDistanceBetweenCoords3d(x, y, z, x1, y1, z1) < radius or not doesBlipExist(checkpoint)
        deleteCheckpoint(marker)
        removeBlip(checkpoint)
        addOneOffSound(0, 0, 0, 1149)
    end)
end

function imgui.TextColoredRGB(text)
    local style = imgui.GetStyle()
    local colors = style.Colors
    local ImVec4 = imgui.ImVec4

    local explode_argb = function(argb)
        local a = bit.band(bit.rshift(argb, 24), 0xFF)
        local r = bit.band(bit.rshift(argb, 16), 0xFF)
        local g = bit.band(bit.rshift(argb, 8), 0xFF)
        local b = bit.band(argb, 0xFF)
        return a, r, g, b
    end

    local getcolor = function(color)
        if color:sub(1, 6):upper() == 'SSSSSS' then
            local r, g, b = colors[1].x, colors[1].y, colors[1].z
            local a = tonumber(color:sub(7, 8), 16) or colors[1].w * 255
            return ImVec4(r, g, b, a / 255)
        end
        local color = type(color) == 'string' and tonumber(color, 16) or color
        if type(color) ~= 'number' then return end
        local r, g, b, a = explode_argb(color)
        return imgui.ImColor(r, g, b, a):GetVec4()
    end

    local render_text = function(text_)
        for w in text_:gmatch('[^\r\n]+') do
            local text, colors_, m = {}, {}, 1
            w = w:gsub('{(......)}', '{%1FF}')
            while w:find('{........}') do
                local n, k = w:find('{........}')
                local color = getcolor(w:sub(n + 1, k - 1))
                if color then
                    text[#text], text[#text + 1] = w:sub(m, n - 1), w:sub(k + 1, #w)
                    colors_[#colors_ + 1] = color
                    m = n
                end
                w = w:sub(1, n - 1) .. w:sub(k + 1, #w)
            end
            if text[0] then
                for i = 0, #text do
                    imgui.TextColored(colors_[i] or colors[1], u8(text[i]))
                    imgui.SameLine(nil, 0)
                end
                imgui.NewLine()
            else imgui.Text(u8(w)) end
        end
    end

    render_text(text)
end

function elsebtn()
	if configuration.main_settings.style == 2 then
	    imgui.PushStyleColor(imgui.Col.Button, imgui.ImVec4(0.66, 0.00, 1.0, 0.70))
	    imgui.PushStyleColor(imgui.Col.ButtonHovered, imgui.ImVec4(0.56, 0.00, 0.90, 0.60))
	    imgui.PushStyleColor(imgui.Col.ButtonActive, imgui.ImVec4(0.46, 0.00, 0.80, 0.50))
	else
		imgui.PushStyleColor(imgui.Col.Button, imgui.ImVec4(0.00, 0.43, 0.66, 1.00))
	    imgui.PushStyleColor(imgui.Col.ButtonHovered, imgui.ImVec4(0.00, 0.33, 0.56, 1.00))
	    imgui.PushStyleColor(imgui.Col.ButtonActive, imgui.ImVec4(0.00, 0.23, 0.46, 1.00))
	end
end

function redbtn()
    imgui.PushStyleColor(imgui.Col.Button, imgui.ImVec4(0.50, 0.00, 0.00, 1.00))
    imgui.PushStyleColor(imgui.Col.ButtonHovered, imgui.ImVec4(0.40, 0.00, 0.00, 1.00))
    imgui.PushStyleColor(imgui.Col.ButtonActive, imgui.ImVec4(0.30, 0.00, 0.00, 1.00))
end

function greenbtn()
    imgui.PushStyleColor(imgui.Col.Button, imgui.ImVec4(0.00, 0.50, 0.00, 1.00))
    imgui.PushStyleColor(imgui.Col.ButtonHovered, imgui.ImVec4(0.00, 0.40, 0.00, 1.00))
    imgui.PushStyleColor(imgui.Col.ButtonActive, imgui.ImVec4(0.00, 0.30, 0.00, 1.00))
end

function endbtn()
    imgui.PopStyleColor(3)
end

function imgui.PushDisableButton()
	imgui.PushStyleColor(imgui.Col.Button, imgui.ImVec4(0.0, 0.0, 0.0, 0.2))
    imgui.PushStyleColor(imgui.Col.ButtonHovered, imgui.ImVec4(0.0, 0.0, 0.0, 0.2))
    imgui.PushStyleColor(imgui.Col.ButtonActive, imgui.ImVec4(0.0, 0.0, 0.0, 0.2))
end
function imgui.PopDisableButton()
	imgui.PopStyleColor(3)
end

local russian_characters = {
    [168] = '�', [184] = '�', [192] = '�', [193] = '�', [194] = '�', [195] = '�', [196] = '�', [197] = '�', [198] = '�', [199] = '�', [200] = '�', [201] = '�', [202] = '�', [203] = '�', [204] = '�', [205] = '�', [206] = '�', [207] = '�', [208] = '�', [209] = '�', [210] = '�', [211] = '�', [212] = '�', [213] = '�', [214] = '�', [215] = '�', [216] = '�', [217] = '�', [218] = '�', [219] = '�', [220] = '�', [221] = '�', [222] = '�', [223] = '�', [224] = '�', [225] = '�', [226] = '�', [227] = '�', [228] = '�', [229] = '�', [230] = '�', [231] = '�', [232] = '�', [233] = '�', [234] = '�', [235] = '�', [236] = '�', [237] = '�', [238] = '�', [239] = '�', [240] = '�', [241] = '�', [242] = '�', [243] = '�', [244] = '�', [245] = '�', [246] = '�', [247] = '�', [248] = '�', [249] = '�', [250] = '�', [251] = '�', [252] = '�', [253] = '�', [254] = '�', [255] = '�',
}
function string.rlower(s)
    s = s:lower()
    local strlen = s:len()
    if strlen == 0 then return s end
    s = s:lower()
    local output = ''
    for i = 1, strlen do
        local ch = s:byte(i)
        if ch >= 192 and ch <= 223 then -- upper russian characters
            output = output .. russian_characters[ch + 32]
        elseif ch == 168 then -- �
            output = output .. russian_characters[184]
        else
            output = output .. string.char(ch)
        end
    end
    return output
end
function string.rupper(s)
    s = s:upper()
    local strlen = s:len()
    if strlen == 0 then return s end
    s = s:upper()
    local output = ''
    for i = 1, strlen do
        local ch = s:byte(i)
        if ch >= 224 and ch <= 255 then -- lower russian characters
            output = output .. russian_characters[ch - 32]
        elseif ch == 184 then -- �
            output = output .. russian_characters[168]
        else
            output = output .. string.char(ch)
        end
    end
    return output
end

function join_rgb(rr, gg, bb)
	return bit.bor(bit.bor(bb, bit.lshift(gg, 8)), bit.lshift(rr, 16))
end

function sampSetChatInputCursor(start, finish)
    local finish = finish or start
    local start, finish = tonumber(start), tonumber(finish)
    local mem = require 'memory'
    local chatInfoPtr = sampGetInputInfoPtr()
    local chatBoxInfo = getStructElement(chatInfoPtr, 0x8, 4)
    mem.setint8(chatBoxInfo + 0x11E, start)
    mem.setint8(chatBoxInfo + 0x119, finish)
    return true
end

function autoupdate(json_url, prefix, url)
  local dlstatus = require('moonloader').download_status
  local json = getWorkingDirectory() .. '\\'..thisScript().name..'-version.json'
  if doesFileExist(json) then os.remove(json) end
  downloadUrlToFile(json_url, json,
    function(id, status, p1, p2)
      if status == dlstatus.STATUSEX_ENDDOWNLOAD then
        if doesFileExist(json) then
          local f = io.open(json, 'r')
          if f then
            local info = decodeJson(f:read('*a'))
            updatelink = info.updateurl
            updateversion = info.latest
            f:close()
            os.remove(json)
            if updateversion ~= thisScript().version then
              lua_thread.create(function(prefix)
                local dlstatus = require('moonloader').download_status
                local color = -1
				sampfuncsLog(mc..'Bank-Helper:'..wc..' ������� ���������� '..thisScript().version..' -> '..updateversion..'! ��������..', mcx)
                wait(250)
                downloadUrlToFile(updatelink, thisScript().path,
                  function(id3, status1, p13, p23)
                    if status1 == dlstatus.STATUS_DOWNLOADINGDATA then
					-- ? --
                    elseif status1 == dlstatus.STATUS_ENDDOWNLOADDATA then
						sampfuncsLog(mc..'Bank-Helper:'..wc..' ������ �������� �� ������ '..mc..updateversion, mcx)
						goupdatestatus = true
						configuration.main_settings.infoupdate = true

						lua_thread.create(function() 
							no_error_msg = true
							wait(500)
							thisScript():reload() 
						end)
                    end
                    if status1 == dlstatus.STATUSEX_ENDDOWNLOAD then
                      if goupdatestatus == nil then
                        sampfuncsLog(mc..'Bank-Helper:'..wc..' ������ �� ���� ��������� �� ������ '..updateversion..'. ���������� ��� ���', mcx)
                        update = false
                      end
                    end
                  end
                )
                end, prefix
              )
            else
              update = false
          	sampfuncsLog(mc..'Bank-Helper:'..wc..' ���������� �� �������', mcx)
            end
          end
        else 
          sampfuncsLog(mc..'Bank-Helper:'..wc..' ����������� ������ ��� �������� ����������. ���������� � ������������', mcx)
          update = false
          end
      end
    end
  )
  while update ~= false do wait(100) end
end

function Window_Info_Update()
	if infoupdate.v then 
		local xx, yy = getScreenResolution()
		imgui.SetNextWindowSize(imgui.ImVec2(xx / 1.5, yy / 1.5), imgui.Cond.FirstUseEver)
		imgui.SetNextWindowPos(imgui.ImVec2(xx / 2, yy / 2), imgui.Cond.FirstUseEver, imgui.ImVec2(0.5, 0.5))
        imgui.Begin(u8'���������� � �����������', infoupdate, imgui.WindowFlags.NoCollapse + imgui.WindowFlags.NoResize + imgui.WindowFlags.NoMove)
			
        imgui.CenterTextColoredRGB(mc..'��������� ������ �������: '..thisScript().version..'!')
        imgui.SameLine(imgui.GetWindowWidth() - 160)
        if imgui.Button(u8'��� ������� �������', imgui.ImVec2(150, 20)) then 
        	imgui.OpenPopup(u8("��� ������� �������"))
        end
        helpCommands()
        imgui.NewLine()

        imgui.PushStyleColor(imgui.Col.ScrollbarBg, imgui.GetStyle().Colors[imgui.Col.FrameBg])
        	imgui.InputTextMultiline("##changelog", changelog, imgui.ImVec2(-1, -1), imgui.InputTextFlags.ReadOnly)
       	imgui.PopStyleColor()

		imgui.End()
	end
end

function helpCommands()
	if imgui.BeginPopupModal(u8("��� ������� �������"), _, imgui.WindowFlags.NoCollapse + imgui.WindowFlags.NoScrollbar + imgui.WindowFlags.AlwaysAutoResize) then

		imgui.CenterTextColoredRGB(mc..'� ���� ���� ��������� ��� ��������� ������� � ��������� ������,\n'..mc..'������� ����� ������������ � ���� �������')
		imgui.NewLine()
		imgui.TextColoredRGB(mc..'/bank{SSSSSS} - �������� ���� ������� (���� "BB" ��� ���-���)')
		imgui.TextColoredRGB(mc..'/ustav{SSSSSS} - ����� �� � ��������� ����')
		imgui.TextColoredRGB(mc..'/ro [text]{SSSSSS} - �� ��� ���������� (9+ ����)')
		imgui.TextColoredRGB(mc..'/rbo [text]{SSSSSS} - ����� ��� ���������� (9+ ����)')
		imgui.TextColoredRGB(mc..'/premium{SSSSSS} - ������� ���� ������ ������ �����������')
		imgui.TextColoredRGB(mc..'/uninvite [id] [�������]{SSSSSS} - ���������� ���������� � �� ���������� (9+ ����)')
		imgui.TextColoredRGB(mc..'/giverank [id] [����]{SSSSSS} - ������ ����� ���������� � �� ���������� (9+ ����)')
		imgui.TextColoredRGB(mc..'/fwarn [id] [�������]{SSSSSS} - ������ �������� ���������� � �� ���������� (9+ ����)')
		imgui.TextColoredRGB(mc..'/blacklist [id] [�������]{SSSSSS} - ��������� � �� ����� � �� ���������� (9+ ����)')
		imgui.TextColoredRGB(mc..'/unfwarn [id]{SSSSSS} - ������ �������� ���������� � �� ���������� (9+ ����)')
		imgui.TextColoredRGB(mc..'/uval [id] [�������] [������� ��]{SSSSSS} - ������� ���������� ������ � �� � �����������')
		imgui.TextColoredRGB(mc..'/uval{SSSSSS} - ���� ���������� ����������� (9+ ����)')
		imgui.TextColoredRGB(mc..'/kip{SSSSSS} - ������� ������� ������ ���������� �� �����')
		imgui.Separator()
		imgui.TextColoredRGB(mc..'��� + Q{SSSSSS} - ���� �������������� � ��������')
		imgui.TextColoredRGB(mc..'��� + G{SSSSSS} - ������� �� ����� � �������� �.�.�')
		imgui.TextColoredRGB(mc..'��� + U{SSSSSS} - ���� ���������� (9+)')
		imgui.TextColoredRGB(mc..'��� + R{SSSSSS} - �������� ���� ��� ���� {select_id}/{select_name}')
		imgui.NewLine()
		imgui.SameLine((imgui.GetWindowWidth() - 150) / 2)
		if imgui.Button(u8'�������##�������', imgui.ImVec2(150, 20)) then 
        	imgui.CloseCurrentPopup()
		end	
		imgui.EndPopup()
	end
end

function checkdata()
	if not doesDirectoryExist(getWorkingDirectory()..'/BHelper') then
		no_error_msg = true
		if createDirectory(getWorkingDirectory()..'/BHelper') then 
			sampfuncsLog(mc..'Bank-Helper:'..wc..' �������� ����������� �������!')
			sampfuncsLog(mc..'Bank-Helper:'..wc..' ������� ��������� ��� �����..')
			
			downloadUrlToFile('https://gitlab.com/uploads/-/system/personal_snippet/1978930/6b234161b85b07fdee8306e9e4b0fd11/%D0%A3%D1%81%D1%82%D0%B0%D0%B2_%D0%A6%D0%91.txt', getWorkingDirectory()..'/BHelper/�����_��.txt', function (id, status, p1, p2)
				if status == dlstatus.STATUSEX_ENDDOWNLOAD then
					sampfuncsLog(mc.."Bank-Helper:"..wc.." ���� �����_��.txt ��������!")
				end
			end)
	    	downloadUrlToFile('https://gitlab.com/uploads/-/system/personal_snippet/1978930/889dbc2cc55e5c10b1f723c856e81800/%D0%9A%D1%80%D0%B5%D0%B4%D0%B8%D1%82%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5.txt', getWorkingDirectory()..'/BHelper/������������.txt', function (id, status, p1, p2)
				if status == dlstatus.STATUSEX_ENDDOWNLOAD then
					sampfuncsLog(mc.."Bank-Helper:"..wc.." ���� ������������.txt ��������!")
				end
			end)
	    	downloadUrlToFile('https://gitlab.com/uploads/-/system/personal_snippet/1978930/be6cc2e15f5ef2b34399c5d7a82aa3b6/binds.png', getWorkingDirectory()..'/BHelper/binds.png', function (id, status, p1, p2)
				if status == dlstatus.STATUSEX_ENDDOWNLOAD then
					sampfuncsLog(mc.."Bank-Helper:"..wc.." ���� binds.png ��������!")
				end
			end)
			downloadUrlToFile('https://gitlab.com/uploads/-/system/personal_snippet/1978930/6274f2bc275e2c6f7ca9d3f1a3f47053/%D0%9A%D0%B0%D0%B4%D1%80%D0%BE%D0%B2%D0%B0%D1%8F_%D1%81%D0%B8%D1%81%D1%82%D0%B5%D0%BC%D0%B0.txt', getWorkingDirectory()..'/BHelper/�������� �������.txt', function (id, status, p1, p2)
				if status == dlstatus.STATUSEX_ENDDOWNLOAD then
					sampfuncsLog(mc.."Bank-Helper:"..wc.." ���� �������� �������.txt ��������!")
				end
			end)
	    	downloadUrlToFile('https://gitlab.com/uploads/-/system/personal_snippet/1978930/edd6b17cdd809bc687a7ab1482f2d13b/main.png', getWorkingDirectory()..'/BHelper/main.png', function (id, status, p1, p2)
				if status == dlstatus.STATUSEX_ENDDOWNLOAD then
					sampfuncsLog(mc.."Bank-Helper:"..wc.." ���� main.png ��������!")
					if doesFileExist(getWorkingDirectory()..'/resource/fonts/fa-solid-900.ttf') then 
						sampfuncsLog(mc..'Bank-Helper:'..wc..' �������� ���� ������ ���������!')
						thisScript():reload()
					end
				end
			end)
		end
	else
		if not doesFileExist(getWorkingDirectory()..'/BHelper/�����_��.txt') then 
			no_error_msg = true
			downloadUrlToFile('https://gitlab.com/uploads/-/system/personal_snippet/1978930/6b234161b85b07fdee8306e9e4b0fd11/%D0%A3%D1%81%D1%82%D0%B0%D0%B2_%D0%A6%D0%91.txt', getWorkingDirectory()..'/BHelper/�����_��.txt', function (id, status, p1, p2)
				if status == dlstatus.STATUSEX_ENDDOWNLOAD then
					sampfuncsLog(mc.."Bank-Helper:"..wc.." ���� �����_��.txt ��������!")
				end
			end)
		end
		if not doesFileExist(getWorkingDirectory()..'/BHelper/������������.txt') then 
			downloadUrlToFile('https://gitlab.com/uploads/-/system/personal_snippet/1978930/6d29b697dc2c87a300c143506c1a68d1/%D0%9A%D1%80%D0%B5%D0%B4%D0%B8%D1%82%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5.txt', getWorkingDirectory()..'/BHelper/������������.txt', function (id, status, p1, p2)
				if status == dlstatus.STATUSEX_ENDDOWNLOAD then
					sampfuncsLog(mc.."Bank-Helper:"..wc.." ���� ������������.txt ��������!")
				end
			end)
		end
		if not doesFileExist(getWorkingDirectory()..'/BHelper/binds.png') then 
			downloadUrlToFile('https://gitlab.com/uploads/-/system/personal_snippet/1978930/be6cc2e15f5ef2b34399c5d7a82aa3b6/binds.png', getWorkingDirectory()..'/BHelper/binds.png', function (id, status, p1, p2)
				if status == dlstatus.STATUSEX_ENDDOWNLOAD then
					sampfuncsLog(mc.."Bank-Helper:"..wc.." ���� binds.png ��������!")
				end
			end)
		end
		if not doesFileExist(getWorkingDirectory()..'/BHelper/main.png') then 
			downloadUrlToFile('https://gitlab.com/uploads/-/system/personal_snippet/1978930/edd6b17cdd809bc687a7ab1482f2d13b/main.png', getWorkingDirectory()..'/BHelper/main.png', function (id, status, p1, p2)
				if status == dlstatus.STATUSEX_ENDDOWNLOAD then
					sampfuncsLog(mc.."Bank-Helper:"..wc.." ���� main.png ��������!")
				end
			end)
		end
		if not doesFileExist(getWorkingDirectory()..'/BHelper/�������� �������.txt') then 
			downloadUrlToFile('https://gitlab.com/uploads/-/system/personal_snippet/1978930/6274f2bc275e2c6f7ca9d3f1a3f47053/%D0%9A%D0%B0%D0%B4%D1%80%D0%BE%D0%B2%D0%B0%D1%8F_%D1%81%D0%B8%D1%81%D1%82%D0%B5%D0%BC%D0%B0.txt', getWorkingDirectory()..'/BHelper/�������� �������.txt', function (id, status, p1, p2)
				if status == dlstatus.STATUSEX_ENDDOWNLOAD then
					sampfuncsLog(mc.."Bank-Helper:"..wc.." ���� �������� �������.txt ��������!")
				end
			end)
		end
	end
	if not doesDirectoryExist(getWorkingDirectory()..'/resource') then
		no_error_msg = true
		if createDirectory(getWorkingDirectory()..'/resource') then 
			if createDirectory(getWorkingDirectory()..'/resource/fonts') then 
				downloadUrlToFile('https://gitlab.com/uploads/-/system/personal_snippet/1978930/95884378224857885f0917483e9f73c2/fa-solid-900.ttf', getWorkingDirectory()..'/resource/fonts/fa-solid-900.ttf', function (id, status, p1, p2)
					if status == dlstatus.STATUSEX_ENDDOWNLOAD then
						sampfuncsLog(mc.."Bank-Helper:"..wc.." ���� fa-solid-900.ttf ��������!")
						sampfuncsLog(mc..'Bank-Helper:'..wc..' �������� ���� ������ ���������!')
						thisScript():reload()
					end
				end)
			end
		end
	elseif not doesDirectoryExist(getWorkingDirectory()..'/resource/fonts') then
		no_error_msg = true
		if createDirectory(getWorkingDirectory()..'/resource/fonts') then 
			downloadUrlToFile('https://gitlab.com/uploads/-/system/personal_snippet/1978930/95884378224857885f0917483e9f73c2/fa-solid-900.ttf', getWorkingDirectory()..'/resource/fonts/fa-solid-900.ttf', function (id, status, p1, p2)
				if status == dlstatus.STATUSEX_ENDDOWNLOAD then
					sampfuncsLog(mc.."Bank-Helper:"..wc.." ���� fa-solid-900.ttf ��������!")
					sampfuncsLog(mc..'Bank-Helper:'..wc..' �������� ���� ������ ���������!')
					thisScript():reload()
				end
			end)
		end
	elseif not doesFileExist(getWorkingDirectory()..'/resource/fonts/fa-solid-900.ttf') then 
		no_error_msg = true
		downloadUrlToFile('https://gitlab.com/uploads/-/system/personal_snippet/1978930/95884378224857885f0917483e9f73c2/fa-solid-900.ttf', getWorkingDirectory()..'/resource/fonts/fa-solid-900.ttf', function (id, status, p1, p2)
			if status == dlstatus.STATUSEX_ENDDOWNLOAD then
				sampfuncsLog(mc.."Bank-Helper:"..wc.." ���� fa-solid-900.ttf ��������!")
				sampfuncsLog(mc..'Bank-Helper:'..wc..' �������� ���� ������ ���������!')
				thisScript():reload()
			end
		end)
	end	
end

function downloadLections()
	createDirectory(getWorkingDirectory()..'/BHelper/Lections')
	downloadUrlToFile('https://gitlab.com/uploads/-/system/personal_snippet/1978930/679b2d6ec98190b68596a4f022d5e384/%D0%92%D0%BD%D0%B5%D1%88%D0%BD%D0%B8%D0%B9_%D0%B2%D0%B8%D0%B4_%D1%81%D0%BE%D1%82%D1%80%D1%83%D0%B4%D0%BD%D0%B8%D0%BA%D0%BE%D0%B2.txt', getWorkingDirectory()..'/BHelper/Lections/������� ��� �����������.txt')
	downloadUrlToFile('https://gitlab.com/uploads/-/system/personal_snippet/1978930/393ea47ec1ff4d0b72988685ee9cb23f/%D0%92%D1%80%D0%B5%D0%BC%D0%B5%D0%BD%D0%BD%D0%BE%D0%B5_%D0%BE%D1%82%D0%BB%D1%83%D1%87%D0%B5%D0%BD%D0%B8%D0%B5_%D0%BE%D1%82_%D0%B4%D0%B5%D0%BB.txt', getWorkingDirectory()..'/BHelper/Lections/��������� ��������� �� ���.txt')
	downloadUrlToFile('https://gitlab.com/uploads/-/system/personal_snippet/1978930/f40ceaba851c01b6beb1dad8dcd74b56/%D0%9E%D0%B1%D1%80%D0%B0%D1%89%D0%B5%D0%BD%D0%B8%D0%B5_%D1%81%D0%BE_%D1%81%D1%82%D0%B0%D1%80%D1%88%D0%B8%D0%BC%D0%B8.txt', getWorkingDirectory()..'/BHelper/Lections/��������� �� ��������.txt')
	downloadUrlToFile('https://gitlab.com/uploads/-/system/personal_snippet/1978930/067f3be8503724074fcd0738ae52a7ec/%D0%9F%D1%80%D0%B0%D0%B2%D0%B8%D0%BB%D0%B0_%D0%BD%D0%B0%D1%88%D0%B5%D0%B3%D0%BE_%D0%B1%D0%B0%D0%BD%D0%BA%D0%B0.txt', getWorkingDirectory()..'/BHelper/Lections/������� ������ �����.txt')
	downloadUrlToFile('https://gitlab.com/uploads/-/system/personal_snippet/1978930/60b709983d9d30ae203a6d5c84b1029e/%D0%9F%D1%80%D0%B0%D0%B2%D0%B8%D0%BB%D0%B0_%D0%BF%D0%BE%D0%B2%D0%B5%D0%B4%D0%B5%D0%BD%D0%B8%D1%8F_%D0%BF%D1%80%D0%B8_%D1%82%D0%B5%D1%80%D0%B0%D0%BA%D1%82%D0%B5.txt', getWorkingDirectory()..'/BHelper/Lections/������� ��������� ��� �������.txt')
	downloadUrlToFile('https://gitlab.com/uploads/-/system/personal_snippet/1978930/a5b674e13218e9e58e02a09bccd51dc1/%D0%9F%D1%80%D0%B8%D1%87%D0%B0%D1%81%D1%82%D0%BD%D0%BE%D1%81%D1%82%D1%8C_%D0%BA_%D0%BA%D1%80%D0%B8%D0%BC%D0%B8%D0%BD%D0%B0%D0%BB%D1%83.txt', getWorkingDirectory()..'/BHelper/Lections/������������ � ���������.txt', function (id, status, p1, p2)
		if status == dlstatus.STATUSEX_ENDDOWNLOAD then
			sampAddChatMessage(tag..'��� ������ ���������!', mcx)
		end
	end)
end